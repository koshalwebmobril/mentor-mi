//
//  Constant.swift
//  HotelBooking
//
//  Created by Manish on 08/11/19
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation

import UIKit

//MARK:- String Constants
enum StringConstants {
    
    enum Response {
        static var Status : String { return "status" }
        static var Message : String { return "message" }
        static var Data : String { return "data" }
        static var Success : String { return "success" }
        static var Error : String { return "error"}
    }
    
    enum Alert {
        
        enum Titles {
            static var Error : String { return "Mentor Mi" }
            static var Success : String { return "Success" }
            static var AppName : String { return "Mentor Mi" }
        }
        
        enum Messages {
            static var NoInternet : String { return "No Internet" }
            static var SomeErrorOccurred : String { return "Some Error Occurred" }
            static var ErrorEmail : String { return "Enter Email" }
            static var ErrorEmailFormat : String { return "Enter Invalid Email" }
            static var ErrorPassword : String { return "Enter Password" }
            static var ErrorPasswordMatch : String { return "Password does not Match" }
            static var ErrorTC : String { return "Accept Terms & Conditions"}
            static var ErrorPhone : String { return "Error Phone" }
            static var ErrorEmailOrPhone : String { return "Enter Email Or Phone" }
            static var InvalidOTP : String { return "Invalid OTP" }
            static var ErrorFirstName : String { return "Enter First Name"}
            static var ErrorLasttName : String { return "Enter Last Name"}
            static var ErrorConfirmPassword : String { return "Enter Confirm Password"}
            
            static var ErrorSkills : String { return "Enter Skill"}
            static var ErrorOccupation : String { return "Enter Occupation"}
            static var ErrorQualification : String { return "Enter Qualification"}
            static var ErrorExperience : String { return "Enter Experience"}
            
            static var ErrorTrainingType : String { return "Enter Training Type"}
            static var ErrorUniversity : String { return "Enter University"}
            static var ErrorCollege : String { return "Enter College"}
            static var ErrorTraining : String { return "Enter Formal Training"}
            static var ErrorHobby : String { return "Enter Hobbies"}
            static var ErrorMusic : String { return "Enter Music"}
            static var ErrorFilm : String { return "Enter Films"}
            static var ErrorLikes : String { return "Enter Likes"}
            static var ErrorDisLikes : String { return "Enter Dislikes"}
            static var ErrorProfilePic : String { return "Upload Profile Picture"}
            static var ErrorTutorialTitle : String { return "Enter Tutorial Title"}
            static var ErrorDificultyLevel : String { return "Enter Difficulty Level"}
            static var ErrorTag : String { return "Enter Tag"}
            static var ErrorDescription : String { return "Enter Description"}
            static var ErrorClassName : String { return "Enter Class Name"}
            static var ErrorNoClass : String{ return "No Class Found"}
            static var ErrorNoTextMsgs: String{ return "Please Enter Messages"}
            static var ErrorNoCamera: String{ return "You don't have camera"}
        }
        
        enum Actions {
            static var Okay : String { return "Ok" }
            static var Cancel : String { return "Cancel" }
        }
    }
}

enum URLConstants {
    
    static var BaseEndpointUrl = URL(string: "https://www.webmobril.org/dev/enigma/api/v1")!
    
    static let Login = BaseEndpointUrl.appendingPathComponent("auth/login")
    static let Register = BaseEndpointUrl.appendingPathComponent("auth/register")
    static let VerifyOTP = BaseEndpointUrl.appendingPathComponent("auth/verify_otp")
    static let ResetPassword = BaseEndpointUrl.appendingPathComponent("auth/reset_password")
    static let SendOTP = BaseEndpointUrl.appendingPathComponent("auth/send_otp")
    
}
