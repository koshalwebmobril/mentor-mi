//
//  StoryBoard.swift
//  HotelBooking
//
//  Created by Manish on 08/11/19
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    private class var main : UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class var login : LoginVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
    }
    
    class var registration : RegistrationVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: RegistrationVC.self)) as! RegistrationVC
    }
    
    class var forget : ForgetVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: ForgetVC.self)) as! ForgetVC
    }
    
    class var skills : SkillInfoVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SkillInfoVC.self)) as! SkillInfoVC
        
    }
    
    class var education : EducationInfoVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: EducationInfoVC.self)) as! EducationInfoVC
    }
    
    class var personal : PersonalInfoVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: PersonalInfoVC.self)) as! PersonalInfoVC
    }
    
    class var profilePic : ProfilePicVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: ProfilePicVC.self)) as! ProfilePicVC
    }
    
    class var welcome : WelcomeVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: WelcomeVC.self)) as! WelcomeVC
    }
    
    class var playerThumbnail : PlayerThumbnailVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: PlayerThumbnailVC.self)) as! PlayerThumbnailVC
    }
    
    class var premium : PremiumVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: PremiumVC.self)) as! PremiumVC
    }
    
    class var notification : NotificationVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: NotificationVC.self)) as! NotificationVC
    }
    
    class var setting : SettingVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SettingVC.self)) as! SettingVC
    }
    
    class var profile : ProfileVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: ProfileVC.self)) as! ProfileVC
    }
    
    class var editProfile : EditProfileVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: EditProfileVC.self)) as! EditProfileVC
    }
    
    class var mentormenteesList : MentorMenteeListVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: MentorMenteeListVC.self)) as! MentorMenteeListVC
    }
    
    class var otherProfile : OtherUserProfileVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: OtherUserProfileVC.self)) as! OtherUserProfileVC
    }
    
    class var otherProfileDetail : OtherUserProfileDetailVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: OtherUserProfileDetailVC.self)) as! OtherUserProfileDetailVC
    }
    
    class var otherTutorial : OtherTutorialVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: OtherTutorialVC.self)) as! OtherTutorialVC
    }
    
    class var uploadVideo : UploadVideoVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: UploadVideoVC.self)) as! UploadVideoVC
    }
    
    class var createTutorial : CreateTutorialVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: CreateTutorialVC.self)) as! CreateTutorialVC
    }
    
    class var subscription : SubscribeVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SubscribeVC.self)) as! SubscribeVC
    }
    
    class var createClass : CreateClassVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: CreateClassVC.self)) as! CreateClassVC
    }
    
    class var singleChat : SingleChatVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SingleChatVC.self)) as! SingleChatVC
    }
    
    class var singleChatList : SingleChatListVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SingleChatListVC.self)) as! SingleChatListVC
    }
    
    class var searchProfile : ProfilebySearchVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: ProfilebySearchVC.self)) as! ProfilebySearchVC
    }
    
    class var tutorial : TutorialVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: TutorialVC.self)) as! TutorialVC
    }
    
    class var resetPassword :  ResetPasswordViewController {
        return self.main.instantiateViewController(withIdentifier: String(describing: ResetPasswordViewController.self)) as! ResetPasswordViewController
    }

    class var help : helpVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: helpVC.self)) as! helpVC
    }
    class var contactUS : ContactUsVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: ContactUsVC.self)) as! ContactUsVC
    }

    class var privacy : privacyVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: privacyVC.self)) as! privacyVC
    }
    
    class var tutorialByGroup : CreateTutorialByGroupVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: CreateTutorialByGroupVC.self)) as! CreateTutorialByGroupVC
    }
    
    class var faqVC : FAQViewController {
        return self.main.instantiateViewController(withIdentifier: String(describing: FAQViewController.self)) as! FAQViewController
    }

//    class var tutorial : TutorialVC {
//        return self.main.instantiateViewController(withIdentifier: String(describing: TutorialVC.self)) as! TutorialVC
//    }
    
    
    
    
    
    
    
}
