//
//  UserDefaults.swift
//  Mentor Mi
//
//  Created by Manish on 08/11/19
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class UserDefault: NSObject {
    
    static let standard = UserDefault()
    let defaults = UserDefaults.standard

    func clearAll(){
        if let appDomain = Bundle.main.bundleIdentifier {
            defaults.removePersistentDomain(forName: appDomain)
            defaults.synchronize()
        }
    }
    
    func setCurrentDeviceToken(token: String) {
        defaults.set(token, forKey: "deviceToken")
    }
    
    func getCurrentDeviceToken() -> String{
        if let token = defaults.value(forKey: "deviceToken") as? String{
            return token
        }
        return "abc"
    }
    
    func setCurrentUserId(userId: Int) {
        defaults.set(userId, forKey: "userId")
    }
    
    func getCurrentUserId() -> Int?{
        if let userId = defaults.value(forKey: "userId") as? Int{
            return userId
        }
        return nil
    }
    
    func setCurrentUserName(userName: String) {
        defaults.set(userName, forKey: "userName")
    }
    
    func getCurrentUserName() -> String?{
        if let name = defaults.value(forKey: "userName") as? String{
            return name
        }
        return nil
    }
    
    func setCurrentUserImage(userImage: String) {
        defaults.set(userImage, forKey: "userImage")
    }
    
    func getCurrentUserImage() -> String?{
        if let image = defaults.value(forKey: "userImage") as? String{
            return image
        }
        return nil
    }
    
    func removeUserId() {
        defaults.removeObject(forKey: "userId")
    }
    
    func setCurrentSupscriptionType(type: String) {
        defaults.set(type, forKey: "subscription_Type")
    }
    
    func getCurrentSupscriptionType() -> String{
        if let token = defaults.value(forKey: "subscription_Type") as? String{
            return token
        }
        return "abc"
    }
    
    func setThoughtofDay(type: String) {
        defaults.set(type, forKey: "thought_Day")
    }
    
    func getThoughtofDay() -> String{
        if let token = defaults.value(forKey: "thought_Day") as? String{
            return token
        }
        return "abc"
    }
    
    func setStatus(type: String) {
        defaults.set(type, forKey: "status")
    }
    
    func getStatus() -> String{
        if let token = defaults.value(forKey: "status") as? String{
            return token
        }
        return "abc"
    }
//    status
    
}
