//
//  Font.swift
//  Mentor_Mi
//
//  Created by Manish on 18/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation

import UIKit

extension UIFont {
    static func appFontRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "SYSTEM", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func appFontBold(size: CGFloat) -> UIFont {
        return UIFont(name: "SYSTEM-BOLD", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
