//
//  AppDelegate.swift
//  Mentor_Mi
//
//  Created by Manish on 17/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManager
import AVFoundation
import UserNotifications
import GoogleSignIn
//import TwitterKit
import FacebookLogin
import FacebookCore
import Firebase
import Messages
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, GIDSignInDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var token = String()
    var menuController: DDMenuController?
    let appConstant:AppConstants = AppConstants()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        Fabric.with([Crashlytics.self])
        
        Thread.sleep(forTimeInterval: 5)
        
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }
        }
        
        
        registerForPushNotifications()
        IQKeyboardManager.shared().isEnabled = true
        //        GADMobileAds.configure(withApplicationID: "Mentor Mica-app-pub-2773807744970684~4833195565")
        GIDSignIn.sharedInstance().clientID = "969391961401-aq6lci2jovd4sh14539gc38odlkht9bh.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
//        TWTRTwitter.sharedInstance().start(withConsumerKey:"7IRqjiIYXFXGt2I2It158TNTF", consumerSecret:"rBxbUujbqc7aRVSWt27CVlunLs8OjyRuKMGmtPujecQzumTaeh")
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        UINavigationBar.appearance().barTintColor = appConstant.navigationColor
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        if UserDefault.standard.getCurrentUserId() == nil {
            
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let navigationController = UINavigationController(rootViewController: initialViewController)
            navigationController.isNavigationBarHidden = true
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            
        }
        else {
            goToHome()
        }
        
        return true
    }
    
    
    
    // MARK: - Push Notifications
    func registerForPushNotifications() {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString: \(deviceTokenString)")
        
        //set apns token in messaging
        Messaging.messaging().apnsToken = deviceToken
        
        //get FCM token
        if let tokenn = Messaging.messaging().fcmToken {
            print("FCM token: \(tokenn)")
            token = tokenn
            UserDefault.standard.setCurrentDeviceToken(token: token)

        }
        
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Mentor_Mi")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //    Mark :- CallbackURL
   /*
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if(url.scheme!.isEqual("twitterkit-7IRqjiIYXFXGt2I2It158TNTF")) {
            return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
            
        }

        else {
            
           // return TWTRTwitter.sharedInstance().application(app, open: url, options: options) || GIDSignIn.sharedInstance()?.handle(url as URL?, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplication.OpenURLOptionsKey.annotation])
           
            
            return TWTRTwitter.sharedInstance().application(app, open: url, options: options) || (GIDSignIn.sharedInstance()?.handle(url as URL?) ?? false || ApplicationDelegate.shared.application(app,open: url,options: options))
           
        }
    }

 */

    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if LISDKCallbackHandler.shouldHandle(url) {
            return LISDKCallbackHandler.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return true
    }
    
    
    //    Mark :- GoogleSignIn
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
            // ...
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    //    Mark :- goTo Methods
    
    func goToHome() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        mainController.selectedIndex = 2
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        leftnavController.isNavigationBarHidden = true
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToMentorFeed() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        mainController.selectedIndex = 0
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        leftnavController.isNavigationBarHidden = true
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToTutorial() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        mainController.selectedIndex = 3
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        leftnavController.isNavigationBarHidden = true
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToProfile() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        mainController.selectedIndex = 4
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        //        leftnavController.isNavigationBarHidden = true
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToSoapBox() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        mainController.selectedIndex = 2
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        //        leftnavController.isNavigationBarHidden = true
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToTutorials() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        mainController.selectedIndex = 3
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        //        leftnavController.isNavigationBarHidden = true
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToClasses () {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "CreateClassVC") as! CreateClassVC
        mainController.fromWhere = "menuBar"
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        leftnavController.isNavigationBarHidden = false
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
    }
    
    func goToGroup() {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "GroupsListsVC") as! GroupsListsVC
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        leftnavController.isNavigationBarHidden = false
        rootController?.leftViewController = leftnavController;
        menuController = rootController
        menuController?.leftViewController = leftnavController;
        window?.rootViewController = menuController
        
        
    }
    

}

extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}



