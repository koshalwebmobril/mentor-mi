//
//  AppConstant.swift
//  Mentor_Mi
//
//  Created by Manish on 17/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation

import UIKit

class AppConstants {
    
    let appBlueColor = UIColor(red: 33/255, green: 157/255, blue: 211/255, alpha: 1.0)
    let navigationColor = UIColor(red: 244/255, green: 244/255, blue: 252/255, alpha: 1.0)
    
    let buttonTextColor = UIColor(red: 134/255, green: 59/255, blue: 253/255, alpha: 1.0)
    
    let bottomTitleColor = UIColor(red: 69/255, green: 164/255, blue: 132/255, alpha: 1.0)
    let blueBorderColor = UIColor(red: 62/255, green: 123/255, blue: 206/255, alpha: 1.0)
    
    let textBorderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
    func randomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    
    let iPhone8:CGFloat=736.0
    
    
}
