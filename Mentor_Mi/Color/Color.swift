//
//  Color.swift
//  Mentor_Mi
//
//  Created by Manish on 18/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class var appDefaultColor: UIColor {
        let defaultColor = 0xed1859
        return UIColor.init(red: 0/255, green: 166/255, blue: 130/255, alpha: 1.0)
//        return UIColor.rgb(fromHex: defaultColor)
    }
    
    class func rgb(fromHex: Int) -> UIColor {
        
        let red =   CGFloat((fromHex & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((fromHex & 0x00FF00) >> 8) / 0xFF
        let blue =  CGFloat(fromHex & 0x0000FF) / 0xFF
        let alpha = CGFloat(1.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
