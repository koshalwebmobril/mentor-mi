//
//  URLClass.swift
//  HotelBooking
//
//  Created by Manish on 08/11/19
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation


struct GlobalUrls{
    
//    static let BASE_URL = "http://dev.webmobrilmedia.com/mentor_me/api/"
    static let BASE_URL = "http://mutantfrog.co.uk/api/"
    
    static let contactUS = "contact_admin.php"
    static let login = "login.php"
    static let phoneLogin = "phone_login.php"
    static let otpVerify = "otp_verification.php"
    static let resendOTP = "resend_otp.php"
    static let signUP = "signup.php"
    static let resetPassword = "reset_password.php"
    static let loginApple = "apple_login.php"
    
    static let countryLists = "get_all_country.php"
    static let stateLists = "get_all_state.php"
    static let cityLists = "get_all_city.php"
    
    static let basicInfo = "basic_info.php"
    static let educattionalInfo = "educational_info.php"
    static let forgotPassword = "forget_password.php"
    static let personalInfo = "personal_info.php"
    static let getProfile = "get_profile.php"
    static let updateProfile = "profile_update.php"
    static let picProfile = "upload_profile_pic.php"
    static let getUserProfile = "get_user_profile.php"
    
    static let search = "search.php"
    static let skillInfo = "skill_set.php"
    static let timelineDatewiseTutorial = "timeline.php"
    static let trending_Tutorial = "trending_tutorials.php"
    static let recommend_Tutorial = "other_tutorials.php"
    
    static let createClass = "create_class.php"
    static let createGroup = "create_group.php"
    static let createPost = "create_post.php"
    static let deletePost = "delete_post.php"
    static let followUser = "follow_user.php"
    static let othersTutorial = "get_other_post.php"
    static let likeProfile = "like_profile.php"
    static let listMentees = "list_mentees.php"
    static let listMentors = "list_mentors.php"
    
    static let unfollowUser = "unfollow_user.php"
    static let unlikeUser = "unlike_profile.php"
    static let viewComments = "view_comments.php"
    static let viewMenteesPost = "view_mentees_post.php"
    static let viewTutorialbyClass = "view_post_by_classes.php"
    static let viewPostbyUser = "view_post_by_user_id.php"
    static let viewMentorsPost = "view_post_of_followid.php"
    static let myPost = ""
    
    static let faqDetails = "faq.php"
    static let groupsByUsers = "view_groups_by_user_id.php"
    static let myClassLists = "get_all_class_by_userid.php"
    static let postTutorial = "create_post.php"
    static let scheduleSend = "schedule.php"
    static let shareNow = "post_sharing.php"
    static let comments = "view_comments.php"
    static let addComment = "add_post_comments.php"
    
    static let rate = "add_post_rating.php"
    static let subscribePlan = "get_subscription_list.php"
    
    static let GmailLogin = "gmail_login.php"
    static let TwitterLogin = "twitter_login.php"
    static let facebookLogin = "fb_login.php"
    
    static let chatList = "get_chat_list.php"
    static let sendMsg = "do_chat.php"
    static let getAllUser = "get_all_users.php"
    static let addTuteInGroup = "create_group_post.php"
    
    static let groupMembers = "view_users_by_groupid"
    static let exitGroup = "leave_group.php"
    static let addMemberOnGroup = "add_user_by_group_id.php"
    
    static let myChatLists = "get_recent_chat.php"
    static let myParticularChat = "get_user_chat.php"
    static let doChat = "do_user_chat.php"
    
    static let removeMemberFromGroup = "remove_group_member.php"
    static let addStatus = "add_status.php"
    static let ratingVideo = "view_post_rating.php"
    
    static let inApp = "make_payment.php"
    static let payment = "make_payment.php"
    static let notification = "get_all_notifications.php"
    static let deletePersonalMsg = "delete_msg.php"
    
}
