
//
//  ApiManager.swift
//  HotelBooking
//
//  Created by Manish on 08/11/19
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation
import Alamofire
import SystemConfiguration
import SwiftyJSON


class APIManager {
    
    typealias ApiCompletionHandler = (_ success: Bool, _ response:[String : Any]?) -> Void
    
    static let sharedInstance = APIManager()
    private init() {}
    let defaultHeader = ["Content-Type" : "application/json"]
    
    
//  MARK:- Detect Network
    
    var isNetworkConnected: Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
//  MARK:- Hit Request Method

    
    func startRequest(url:URL, method:HTTPMethod, parameterrs:Parameters?, completion: @escaping ApiCompletionHandler) {
        Alamofire.request(url, method: method, parameters: parameterrs, encoding: URLEncoding.default).responseJSON { response in
            guard let responseData = response.data, let responseDictionary = try? JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? [String : Any] else {
                
                completion(false, nil)
                return
            }
            if let responseCode = response.response?.statusCode, 200...203 ~= responseCode{
                completion(true, responseDictionary)
            } else {
                return completion(false, responseDictionary)
            }
        }
    }
   
    
    
    private func startRequest(url:URL, method:HTTPMethod, parameterrs:Parameters?, encoding: JSONEncoding = .default, headers: HTTPHeaders? = nil, completion: @escaping ApiCompletionHandler) {
        
        if self.isNetworkConnected {
            Alamofire.request(url, method: method, parameters: parameterrs, encoding: encoding, headers: headers).responseData {(response) in
                
                guard let responseData = response.data, let responseDictionary = try? JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? [String : Any] else {
                    
                    completion(false, nil)
                    return
                }
                if let responseCode = response.response?.statusCode, 200...203 ~= responseCode{
                    completion(true, responseDictionary)
                } else {
                    return completion(false, responseDictionary)
                }
            }
        } else {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.NoInternet, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            completion(false, nil)
        }
    }
}

extension APIManager {
    func contactUS(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.contactUS)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    func login(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.login)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
        
    func registration(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.signUP)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    
    func forget(parameterss: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.forgotPassword)"
        print(parameterss)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameterss) { (success, response) in completion(success, response) }
    }
    
    func googleLogin(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.GmailLogin)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func twitterLogin(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.TwitterLogin)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func facebookLogin(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.facebookLogin)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func skills(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.skillInfo)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func education(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.educattionalInfo)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func personal(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.personalInfo)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func photoUpload(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.picProfile)"
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func getUserDetails(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func getUserProfile(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getUserProfile)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    
    
    func search(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.search)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func getTrending(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.trending_Tutorial)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func getRecomended(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.recommend_Tutorial)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    
    func getTimeLine(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func uploadStatus(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    
    func unFollowUser(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.unfollowUser)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func followUser(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.followUser)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func mentorLists(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.listMentors)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func menteesLists(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.listMentees)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func mentorPost(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewMentorsPost)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func myClassList(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.myClassLists)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func otherTutorial(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewMentorsPost)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func myTutorial(parameters: [String: Any], completion: @escaping ApiCompletionHandler)  {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewPostbyUser)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL! , method: .post, parameterrs: parameters) { (success, response) in completion(success, response) }
    }
    
    func notificationList(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.notification)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    func postByUser(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewPostbyUser)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    func viewTutorialbyClass(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewTutorialbyClass)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    func unLikeProfile(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.unlikeUser)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    func likeProfile(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.likeProfile)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
//    groupsByUsers
   
    func groupByUsers(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.groupsByUsers)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
//    myParticularChat
    
    func particularUserChat(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.myParticularChat)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    func chatStart(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.doChat)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    func viewComment(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewComments)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
   
    
    func shareTutorials(parameters: [String: Any], completion: @escaping ApiCompletionHandler) {
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.shareNow)"
        print(url)
        let tempURL = URL(string: url)
        self.startRequest(url:tempURL!, method: .post, parameterrs: parameters) { (success, response) in completion(success, response)}
    }
    
    
    
}


