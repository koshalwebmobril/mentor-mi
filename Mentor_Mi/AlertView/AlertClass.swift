//
//  AlertClass.swift
//  HotelBooking
//
//  Created by Manish on 08/11/19
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation
import UIKit

public struct AlertAction {
    
    var text : String
    var style : UIAlertAction.Style = .default
    var actionHandler : (() -> ())?
    
    init(text : String, style : UIAlertAction.Style = .default, actionHandler : (() -> ())? = nil) {
        self.text = text
        self.style = style
        self.actionHandler = actionHandler
    }
    
}

struct Type {
    static var login = ""
}

extension UIAlertController {
    
    class func showAlert(withTitle title : String, message : String, style : UIAlertController.Style, buttons : [AlertAction]) {
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: style)
            
            for button in buttons {
                alert.addAction(UIAlertAction(title: button.text, style: button.style, handler: { (action) in
                    button.actionHandler?()
                }))
            }
            
            if #available(iOS 13.0, *) {
                if var topController = UIApplication.shared.keyWindow?.rootViewController  {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                        
                    }
                    topController.present(alert, animated: true, completion: nil)
                }
            }else{
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindow.Level.alert + 1
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                
            }
            
        }
    }
}
