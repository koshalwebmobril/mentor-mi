//
//  ResetPasswordViewController.swift
//  Mentor Mi
//
//  Created by WebMobril on 04/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import Alamofire
import SVProgressHUD
import SwiftyJSON
import SkyFloatingLabelTextField

class ResetPasswordViewController: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let appConstant:AppConstants = AppConstants()
     var isOn = true
    
    @IBOutlet weak var txtConfirm: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnReset: UIButton!
    
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var statusTxt: UITextField!
    
    @IBOutlet weak var newPasswordBtn: UIButton!
    @IBOutlet weak var confrmpwrdBtn: UIButton!
    @IBOutlet weak var oldPasswordBtn: UIButton!
    
    
    var arrTimeLine = NSMutableArray()
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefaults.standard.object(forKey: "user_id") as! String
        let newTodo: [String: Any] = ["user_id": userID]
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    
                    
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefaults.standard.object(forKey: "user_id") as! String
        let newTodo: [String: Any] = ["user_id": userID]
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = NSMutableArray(array: responseJson.arrayObject!)
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func uploadStatus() {
        
        if UserDefault.standard.getStatus() == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefaults.standard.object(forKey: "status") as! String
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefaults.standard.object(forKey: "user_id") as! String
            let newTodo: [String: Any] = ["user_id": userID,"status":statusAbtMe]
            
            SVProgressHUD.show(withStatus: "Loading...")
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    SVProgressHUD.dismiss()
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        
                        self.getProfileData()
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    @IBOutlet weak var threedotsinfo: UIView!
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
        
    }
    
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
    }
   
    @IBOutlet weak var content: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.content.layer.borderWidth = 1
        self.content.layer.borderColor = #colorLiteral(red: 0, green: 0.6509803922, blue: 0.5098039216, alpha: 1)
        btnReset.layer.cornerRadius = 5.0
        btnReset.clipsToBounds = true
        
        self.navigationController?.isNavigationBarHidden=true
        tabBarController?.navigationController?.isNavigationBarHidden=true

        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    @objc func notification() {
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }
    
    @objc func threedots() {
       
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.tabBarController?.title = "Reset Password"
//        tabBarController?.navigationController?.isNavigationBarHidden = false
//        tabBarController?.tabBar.isHidden = false
//        navigationController?.navigationBar.barTintColor = appConstant.navigationColor
//        isOn = true
//        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
//        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
//        let rightBarButtons = [threeDots,notify]
//        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
//        notify.tintColor = UIColor.black
//        threeDots.tintColor = UIColor.black
//        threedotsinfo.isHidden = true
    }
    
    @IBAction func oldPasswordShoHideButton(_ sender: Any) {
        
           switch txtOldPassword.isSecureTextEntry{
              case true:
                 txtOldPassword.isSecureTextEntry = false
               oldPasswordBtn.setImage(UIImage(named: "eye"), for: .normal)

              case false:
                 txtOldPassword.isSecureTextEntry = true
                 oldPasswordBtn.setImage(UIImage(named: "eye-hide"), for: .normal)
           }
        
       }
    
     @IBAction func newPasswordShoHideButton(_ sender: Any) {
        switch txtNewPassword.isSecureTextEntry{
           case true:
              txtNewPassword.isSecureTextEntry = false
            newPasswordBtn.setImage(UIImage(named: "eye"), for: .normal)

           case false:
              txtNewPassword.isSecureTextEntry = true
              newPasswordBtn.setImage(UIImage(named: "eye-hide"), for: .normal)
        }
    }
     @IBAction func confirmPasswordShowhideButton(_ sender: Any) {
      switch txtConfirm.isSecureTextEntry{
           case true:
              txtConfirm.isSecureTextEntry = false
            confrmpwrdBtn.setImage(UIImage(named: "eye"), for: .normal)

           case false:
              txtConfirm.isSecureTextEntry = true
              confrmpwrdBtn.setImage(UIImage(named: "eye-hide"), for: .normal)
        }
    }
    
    
    
    public func isValidPassword(pass: String) -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: pass)
    }
    
    @IBAction func actionResetButton(_ sender: Any) {
        
        let finalPass = txtNewPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if txtOldPassword.text == "" {
            let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter old password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if (txtOldPassword.text != ""){
            if txtNewPassword.text == "" {
                let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter new password", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            else if (txtNewPassword.text != ""){
                if (isValidPassword(pass: finalPass!)){
                    if txtConfirm.text == "" {
                        let alert  = UIAlertController(title: "Mentor Mi" , message: "Confirm your password", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if(txtNewPassword.text != txtConfirm.text){
                        let alert  = UIAlertController(title: "Mentor Mi" , message: "New password and Confirm password not matched.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    else {
                        if txtNewPassword.text == txtConfirm.text {
                            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.resetPassword)"
                                        print("\(apiURL)")
                            
                            let userID = UserDefaults.standard.object(forKey: "UserID") as! String
                           // if UserDefaults.standard.object(forKey: "user_id") as? String != nil{
                               // userID = UserDefaults.standard.object(forKey: "user_id") as! String
                           // }
                                        let newTodo: [String: Any] = ["user_id":userID,"old_pass":"\(txtOldPassword.text ?? "")","new_pass":"\(txtNewPassword.text ?? "")"]
                            
                                        print(newTodo)
                            
                                        SVProgressHUD.show(withStatus: "Loading...")
                                        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                                            if((response.result.value) != nil) {
                                                SVProgressHUD.dismiss()
                                                let swiftyJsonVar = JSON(response.result.value!)
                                                print(swiftyJsonVar)
                                                if swiftyJsonVar["status"].string == "success" {
                                                    let responseJson = swiftyJsonVar["data"]
                                                    // we're OK to parse!
                                                    print(responseJson)
                            
                                                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                                                        UserDefaults.standard.removeObject(forKey: "user_id")
                                                        UserDefaults.standard.removeObject(forKey: "user_first_name")
                                                        UserDefaults.standard.removeObject(forKey: "user_last_name")
                                                        UserDefaults.standard.removeObject(forKey: "user_username")
                            
                                                        GIDSignIn.sharedInstance().signOut()
                                                        UserDefaults.standard.removeObject(forKey: "user_id")
                                                        
                                                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                                        let navigationController = UINavigationController(rootViewController: controller!)
                                                        navigationController.isNavigationBarHidden = true
                                                        self.appDelegate.window?.rootViewController = navigationController
                                                        self.appDelegate.window?.makeKeyAndVisible()
//                                                        let stng = self.storyboard?.instantiateViewController(withIdentifier: "settingVC") as! settingVC
//                                                        self.navigationController?.pushViewController(stng, animated: true)
                                                    }))
                                                    self.present(alert, animated: true, completion: nil)
                                                }
                                                else {
                                                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                                    self.present(alert, animated: true, completion: nil)
                                                }
                                            }
                                            else {
                            
                                                SVProgressHUD.dismiss()
                                            }
                                        }
                        }
                    }
                }
                
                else {
                    let alert  = UIAlertController(title: "Mentor Mi" , message: "Your password must contain at least one number,one lowercase & one uppercase letter, one special characters and 8 characters long.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
       
        
    }
    
}
