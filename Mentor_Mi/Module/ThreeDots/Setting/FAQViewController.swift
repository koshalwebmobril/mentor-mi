//
//  FAQViewController.swift
//  Mentor_Mi
//
//  Created by deepkohli on 15/09/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class FAQViewController: UIViewController {
    
     @IBOutlet weak var faqtableView: UITableView!
       
    var arrForFAQList = [JSON]()
    var selectedIndexPath = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
       selectedIndexPath = 500
        getFAQListAPI()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    func getFAQListAPI() {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.faqDetails)"
        print("\(apiURL)")
        
        
        let newTodo: [String: Any] = [:]
        
        Alamofire.request(apiURL, method: .get, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrForFAQList.removeAll()
                    self.arrForFAQList = swiftyJsonVar["data"].arrayValue
                    if self.arrForFAQList.count > 0 {
                        self.faqtableView.reloadData()
                    }
                    
                }
                else {
                    
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
            }
        }
    }
    
}
extension FAQViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int{
           return 1
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrForFAQList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell", for: indexPath as IndexPath) as! FAQTableViewCell
        
            cell.lblTitle.text = arrForFAQList[indexPath.row]["question"].stringValue
            
            if  selectedIndexPath == indexPath.row && cell.dropImageview.image == UIImage(named: "drop") {
                
                cell.lblDescription.text = arrForFAQList[indexPath.row]["answer"].stringValue
                
                cell.dropImageview.image = UIImage(named: "dropdown-1")
            }
            else{
                 cell.lblDescription.text = ""
                 cell.dropImageview.image = UIImage(named: "drop")
            }
        

        return cell
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath.row
        faqtableView.reloadData()
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
         if selectedIndexPath == indexPath.row{
            return UITableView.automaticDimension
        }
        else{
           return 50
        }
            
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       //return UITableView.automaticDimension
      
           return 50
       }
    
     
}
