//
//  helpVC.swift
//  Mentor Mi
//
//  Created by Webmobril on 04/07/19.
//  Copyright © 2019 WebMobril. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage
class helpVC: UIViewController {

    var isOn = true
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var statusView: UIView!
    var arrTimeLine = NSMutableArray()
    @IBOutlet weak var statusTxt: UITextField!
    
    @IBAction func cancel(_ sender: Any) {
          statusView.isHidden = true
    }
    
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefaults.standard.object(forKey: "user_id") as! String
        let newTodo: [String: Any] = ["user_id": userID]
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    self.getTimeLineAPI()
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
                SVProgressHUD.dismiss()
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefaults.standard.object(forKey: "user_id") as! String
        let newTodo: [String: Any] = ["user_id": userID]
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = NSMutableArray(array: responseJson.arrayObject!)
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func uploadStatus() {
        
        if UserDefault.standard.getStatus() == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefaults.standard.object(forKey: "status") as! String
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefaults.standard.object(forKey: "user_id") as! String
            let newTodo: [String: Any] = ["user_id": userID,"status":statusAbtMe]
            
            SVProgressHUD.show(withStatus: "Loading...")
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    SVProgressHUD.dismiss()
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        self.getProfileData()
                    }
                    else {
                        
                    }
                }
                else {
                    
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    @IBAction func faq(_ sender: Any) {
    self.navigationController?.pushViewController(UIStoryboard.faqVC, animated: true)
    }
    @IBAction func contactAdmin(_ sender: Any) {
         self.navigationController?.pushViewController(UIStoryboard.contactUS, animated: true)
       // UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Coming Soon...", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
    }
    
    @IBOutlet weak var threedotsinfo: UIView!
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden=true
        tabBarController?.navigationController?.isNavigationBarHidden=true
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }

       
    }
    
    @objc func notification() {
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }

    @objc func threedots() {
        
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.tabBarController?.title = "Help Settings"
//        tabBarController?.navigationController?.isNavigationBarHidden = false
//        tabBarController?.tabBar.isHidden = false
//        navigationController?.navigationBar.barTintColor = appDelegate.appConstant.navigationColor
//        
//        isOn = true
//        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
//        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
//        let rightBarButtons = [threeDots,notify]
//        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
//        notify.tintColor = UIColor.black
//        threeDots.tintColor = UIColor.black
//        threedotsinfo.isHidden = true
        
    }
    

   
}
