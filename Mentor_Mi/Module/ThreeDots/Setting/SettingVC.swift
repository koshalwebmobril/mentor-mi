//
//  SettingVC.swift
//  Mentor_Mi
//
//  Created by Manish on 10/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnPrivacy: UIButton!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var btnHelp: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden=true
        tabBarController?.navigationController?.isNavigationBarHidden=true
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func actionReset(_ sender: Any) {
        self.navigationController?.pushViewController(UIStoryboard.resetPassword, animated: true)
    }
    
    @IBAction func actionPrivacy(_ sender: Any) {
        self.navigationController?.pushViewController(UIStoryboard.privacy, animated: true)

    }
    
    @IBAction func actionInvite(_ sender: Any) {

        if let urlStr = NSURL(string: "https://itunes.apple.com/us/app/myapp/idxxxxxxxx?ls=1&mt=8") {
            let objectsToShare = [urlStr]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

            if UI_USER_INTERFACE_IDIOM() == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }

            self.present(activityVC, animated: true, completion: nil)
        }

    }
    
    @IBAction func actionHelp(_ sender: Any) {
        self.navigationController?.pushViewController(UIStoryboard.help, animated: true)

    }
}
