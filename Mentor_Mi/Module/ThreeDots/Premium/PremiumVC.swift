//
//  PremiumVC.swift
//  Mentor_Mi
//
//  Created by Manish on 10/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class PremiumVC: UIViewController {

    @IBOutlet weak var monthlyPayButton: UIButton!
    @IBOutlet weak var yearPayButton: UIButton!
    
    //var productID :[String] = ["com.mentormi.MonthlySubscription","com.mentormi.YearlySubscription"]
    var productID :[String] = ["com.mentormi.MonthlynonSubscription","com.mentormi.YearlynonSubscription"]
    
    var productsArray = [SKProduct]()
    var isfromProfile = String()
    
    var type = String()
    var productsData = [SKProduct]()
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        if isfromProfile.count>0{
            self.dismiss(animated: true, completion: nil)
            
        }else{ self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
        self.navigationController?.isNavigationBarHidden=true
        tabBarController?.navigationController?.isNavigationBarHidden=true
        setupUI()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    

    func viewSetup() {
         UtilityMethods.showIndicator()
        IAPManager.shared.getProducts { (result) in
     
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                UtilityMethods.hideIndicator()
                switch result {
                case .success(let products):
                    print(products)
                    self.productsData = products
                    
                    guard let price = IAPManager.shared.getPriceFormatted(for: self.productsData[0]) else { return }
                        self.monthlyPayButton.setTitle(String(format: "%@/Monthly", price), for: .normal)
                    
                    guard let yearlyprice = IAPManager.shared.getPriceFormatted(for: self.productsData[1]) else { return }
                    self.yearPayButton.setTitle(String(format: "%@/Yearly", yearlyprice), for: .normal)
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func purchase(product: SKProduct) -> Bool {
        if !IAPManager.shared.canMakePayments() {
            return false
        } else {
            
               if UIScreen.main.bounds.width > 375{
                 SVProgressHUD.show(withStatus: "Loading...")
                 }
                else{
                 UtilityMethods.showIndicator()
                 }
            IAPManager.shared.buy(product: product) { (result) in
                
                SVProgressHUD.dismiss()
                UtilityMethods.hideIndicator()
               
                DispatchQueue.main.async {

     
                    switch result {
                    case .success(_):
                        print(product)
                        
                        let id = product.productIdentifier
                       let price = product.price
                        self.makePayment(price: "\(price)", transactionId: id)
                    case .failure(let error):
                        print(error)
                        let alertController = UIAlertController(title: "Mentor-MI",
                                                                message: error.localizedDescription,
                                                                   preferredStyle: .alert)
                        
                           alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                           self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            return true
        }
    }
    
    
    
    
    func showAlert(for product: SKProduct) {
        guard let price = IAPManager.shared.getPriceFormatted(for: product) else { return }
        let alertController = UIAlertController(title: product.localizedTitle,
                                                message: product.localizedDescription,
                                                preferredStyle: .alert)
     
        alertController.addAction(UIAlertAction(title: "Buy now for \(price)", style: .default, handler: { (_) in
            if !self.purchase(product: product){
                print("In-App Purchases are not allowed in this device.")
            }
        }))
     
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    
    func setupUI(){
        
        monthlyPayButton.layer.cornerRadius=5
        monthlyPayButton.layer.masksToBounds=true
        
        yearPayButton.layer.cornerRadius=5
        yearPayButton.layer.masksToBounds=true
    }
    
    
    @IBAction func monthlyPayBtnAction(_ sender: Any) {
        

        type = "1"
        if productsData.count > 0{
        showAlert(for: productsData[0])
        }
        
    }
    
    
    @IBAction func yearlyPayBtnAction(_ sender: Any) {
        

        type = "2"
        
        if productsData.count > 0{
        showAlert(for: productsData[1])
        }
        
    }
        
//       http://(DomainURL)/api/make_payment.php?user_id=1&subscription_type=1&txn_status=approved&txn_id=232324&amt=19&txn_date=2018-9-1
    
    
    func makePayment(price: String,transactionId : String){
         if UIScreen.main.bounds.width > 375{
        SVProgressHUD.show(withStatus: "Loading...")
          }
         else{
          UtilityMethods.showIndicator()
          }
        let url = "\(GlobalUrls.BASE_URL)\(GlobalUrls.inApp)"
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let transactionDate = formatter.string(from: date)
        
        let userID = UserDefault.standard.getCurrentUserId()
        let params :[String:Any] = ["user_id":"\(userID!)","subscription_type":type,"txn_status":"approved","txn_id":"\(transactionId)","amt":"\(price)","txn_date":"\(transactionDate)"]
       
        Alamofire.request(url, method: .post, parameters: params,  headers: nil).responseJSON { response in
           SVProgressHUD.dismiss()
           UtilityMethods.hideIndicator()
            
            
            if  response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "success"{
                    let alertController = UIAlertController(title: "Mentor-MI",
                                                            message: "Payment successful.",
                                                               preferredStyle: .alert)
                    
                       alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                       self.present(alertController, animated: true, completion: nil)
                }else{
                    let alertController = UIAlertController(title: "Mentor-MI",
                                                                               message: "Payment failed.",
                                                                                  preferredStyle: .alert)
                                       
                                          alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                                          self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
  
}
