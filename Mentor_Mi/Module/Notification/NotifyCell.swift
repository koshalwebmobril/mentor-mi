//
//  NotifyCell.swift
//  Mentor_Mi
//
//  Created by Manish on 15/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class NotifyCell: UITableViewCell {
    
    @IBOutlet weak var lblNotify: UILabel!
    @IBOutlet weak var imgNotify: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
