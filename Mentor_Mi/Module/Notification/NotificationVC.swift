//
//  NotificationVC.swift
//  Mentor_Mi
//
//  Created by Manish on 10/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btbBack: UIButton!
    @IBOutlet weak var tableNotifications: UITableView!
    
    var arrNotify = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableNotifications.estimatedRowHeight = 49
        tableNotifications.rowHeight = UITableView.automaticDimension
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.isNavigationBarHidden = true

        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        myNotifications(id : UserDefault.standard.getCurrentUserId()!)
    }
    
    func myNotifications(id : Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.notificationList(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleNotificationResponse(response: response)
        }
    }
    
    func handleNotificationResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrNotify = tempResponse["data"].arrayValue
            self.tableNotifications.reloadData()
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotify.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.tableFooterView = UIView()
        
        let cell = tableNotifications.dequeueReusableCell(withIdentifier: "cell") as! NotifyCell
        cell.lblNotify.text = String(format:"%@", arrNotify[indexPath.row]["title"].stringValue)
        cell.imgNotify.layer.cornerRadius = cell.imgNotify.frame.height/2
        cell.imgNotify.layer.borderWidth = 5.0
        cell.imgNotify.layer.borderColor = UIColor.white.cgColor
        cell.imgNotify.clipsToBounds = true
        
        let strURL = String(format:"%@", arrNotify[indexPath.row]["from_profile_pic"].stringValue)
        cell.imgNotify.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileDetail = UIStoryboard.otherProfile
        profileDetail.dictFromData = arrNotify[indexPath.row]
        profileDetail.fromWhere = "Notify"
        profileDetail.strTitle = "Profile"
        self.navigationController?.pushViewController(profileDetail, animated: true)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
