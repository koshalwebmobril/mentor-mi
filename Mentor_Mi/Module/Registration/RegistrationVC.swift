//
//  RegistrationVC.swift
//  Mentor_Mi
//
//  Created by Manish on 17/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class RegistrationVC: UIViewController {
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEMail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnEyePassword: UIButton!
    @IBOutlet weak var btnEyeConfirmPassword: UIButton!
    @IBOutlet weak var btnAcceptTC: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var viewPasswordValidation: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var isAccept :Bool = false
    
    @IBOutlet weak var privacyView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }
    
    func setUI() {
        viewPasswordValidation.isHidden = true
        privacyView.isHidden = true
        btnJoin.layer.cornerRadius = 5.0
        btnJoin.clipsToBounds = true
        
        privacyView.layer.cornerRadius = 10.0
        privacyView.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        if UIScreen.main.bounds.height == 667{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
        }
    }

//    Mark :- actionButton
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionEyePassword(_ sender: UIButton) {
        if txtPassword.isSecureTextEntry == true {
            txtPassword.isSecureTextEntry = false
            sender.setImage(UIImage(named: "eye"), for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            sender.setImage(UIImage(named: "eye-hide"), for: .normal)
        }
    }
    
    @IBAction func actionEyeConfirmPassword(_ sender: UIButton) {
        if txtConfirmPassword.isSecureTextEntry == true {
            txtConfirmPassword.isSecureTextEntry = false
            sender.setImage(UIImage(named: "eye"), for: .normal)
        } else {
            txtConfirmPassword.isSecureTextEntry = true
            sender.setImage(UIImage(named: "eye-hide"), for: .normal)
        }
    }
    
    @IBAction func actionAcceptTC(_ sender: UIButton) {
        if sender.imageView?.image == UIImage(named: "checkBox") {
            sender.setImage(UIImage(named: "checked"), for: UIControl.State.normal)
            isAccept = true
        }
        else {
            sender.setImage(UIImage(named: "checkBox"), for: UIControl.State.normal)
            isAccept = false
        }
    }
    
    @IBAction func actionTCandPrivacyPolicy(_ sender: Any) {
        
        privacyView.isHidden = false
//        let alert  = UIAlertController(title: "Welcome" , message: "we are working on it, Update soon.", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func privcyokAction(_ sender: Any) {
        privacyView.isHidden = true
    }
    
    @IBAction func actionJoin(_ sender: Any) {
        if validationCheck() {
            
            viewPasswordValidation.isHidden = true
            privacyView.isHidden = true
            
//             if (isValidEmail(email: txtEMail.text!)){
//                if (isValidPassword(pass: txtPassword.text!)){
                    self.registration(firstName: txtFirstName.text!, lastName: txtLastName.text!, mail: txtEMail.text!, password: txtPassword.text!, phone: txtPhone.text!)
//                }
//                else {
//                    viewPasswordValidation.isHidden = false
//
//
//                }
//            }
//             else {
//                let alert  = UIAlertController(title: "Welcome" , message: "Please enter valid email.", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
        }
    }
    
    @IBAction func actionSignIn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
//         self.present(UIStoryboard.login, animated: true, completion: nil)
    }
    
    func validationCheck() -> Bool {
        if txtFirstName.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorFirstName, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtLastName.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorLasttName, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtEMail.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmail, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if isValidEmail(email: txtEMail.text!) == false  {
            
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Please enter valid email.", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
            
        }
        else if txtPassword.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorPassword, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if isValidPassword(pass: txtPassword.text!) == false {
            viewPasswordValidation.isHidden = false
            
            return false
        }
        else if txtConfirmPassword.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorConfirmPassword, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if btnAcceptTC.imageView?.image == UIImage(named: "checkBox") {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorTC, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtPassword.text != txtConfirmPassword.text {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorPasswordMatch, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }else if isAccept == false{
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Please accept terms and privacy policy.", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        
        return true
        
    }
    
    func isValidEmail(email: String)-> Bool{
        let emailRegEx = "[A-Za-z0-9.%+-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: email)
    }
    
    func isValidPassword(pass: String) -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: pass)
    }
    
    func isValidName(Name:String) -> Bool {
        let nameRegEx = "\\A\\w{3,30}\\z"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: Name)
    }
    
    
    func registration(firstName: String, lastName: String, mail: String, password: String, phone: String) {
        let parameters = ["firstname": firstName, "lastname": lastName, "email" : mail, "phone" : phone, "password": password, "device_tokken":"1234","device_type":"2" ] as [String:Any]
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.registration(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleRegistrationResponse(response: response)
        }
    }
    
 
    func handleRegistrationResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            Type.login = "1"
             UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
            UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)
            
           let VC = UIStoryboard.skills
            VC.modalPresentationStyle = .fullScreen
            self.present(VC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func actionPasswordOK(_ sender: Any) {
        viewPasswordValidation.isHidden = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
