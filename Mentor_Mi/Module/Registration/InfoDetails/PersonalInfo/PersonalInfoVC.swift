//
//  PersonalInfoVC.swift
//  Mentor_Mi
//
//  Created by Manish on 20/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class PersonalInfoVC: UIViewController {
    @IBOutlet weak var txtHobbies: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMusic: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFilm: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLikes: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDisLikes: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        setUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func setUI() {
        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if checkValidation() {
            self.personal(hobby: txtHobbies.text!, music: txtMusic.text!, film: txtFilm.text!, like: txtLikes.text!, disLike: txtDisLikes.text!)
        }
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        Type.login = "1"
        let VC = UIStoryboard.profilePic
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
    }
    
    
    func checkValidation() -> Bool {
        if txtHobbies.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorHobby, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtMusic.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorMusic, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtFilm.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorFilm, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtLikes.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorLikes, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtDisLikes.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorDisLikes, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        return true
    }
    
    func personal(hobby: String, music: String, film:String, like: String, disLike: String) {
        let temp = UserDefault.standard.getCurrentUserId()
        let parameters = ["user_id": temp!, "hobbies": hobby, "music" : music, "film" : film, "likes" : like, "dislikes" : disLike] as [String:Any]
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.personal(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handlePersonalResponse(response: response)
        }
    }
    
    func handlePersonalResponse(response:  [String : Any]) {
        let result = JSON(response)
        if result["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: result["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            
            let vc = UIStoryboard.profilePic
            vc.modalPresentationStyle = .fullScreen
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
