//
//  SkillInfoVC.swift
//  Mentor_Mi
//
//  Created by Manish on 19/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class SkillInfoVC: UIViewController {
    
    @IBOutlet weak var txtSkill: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOccupation: SkyFloatingLabelTextField!
    @IBOutlet weak var txtQualification: SkyFloatingLabelTextField!
    @IBOutlet weak var txtExperience: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTrainingType: SkyFloatingLabelTextField!
    @IBOutlet weak var btnNext: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        
       if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        btnNext.layer.cornerRadius = 5.0
        btnNext.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if checkValidation() {
            self.skills(skill: txtSkill.text!, occupation: txtOccupation.text!, qualification: txtQualification.text!, experience: txtExperience.text!, trainingType: txtTrainingType.text!)
        }
    }
    
    func checkValidation() -> Bool {
        if txtSkill.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorSkills, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtOccupation.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorOccupation, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtQualification.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorQualification, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtExperience.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorExperience, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtTrainingType.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorTrainingType, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        return true
    }
    
    func skills(skill: String, occupation: String, qualification:String, experience: String, trainingType: String) {
        let temp = UserDefault.standard.getCurrentUserId()
        let parameters = ["user_id": temp!, "occupation": occupation, "qualification" : qualification, "experience" : experience, "skills": skill, "training":trainingType] as [String:Any]
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.skills(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleSkillsResponse(response: response)
        }
    }
    
    func handleSkillsResponse(response : [String : Any]) {
        let result = JSON(response)
        if result["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: result["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            
            let VC = UIStoryboard.education
            VC.modalPresentationStyle = .fullScreen
            self.present(VC, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
