//
//  ProfilePicVC.swift
//  Mentor_Mi
//
//  Created by Manish on 20/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ProfilePicVC: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var btnNext: UIButton!

    var imgData = Data()
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        imagePicker.delegate = self
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(ProfilePicVC.actionMediaOpen))
        imgProfilePic.isUserInteractionEnabled = true
        imgProfilePic.addGestureRecognizer(tap1)
        
        imgProfilePic.layer.cornerRadius = imgProfilePic.bounds.height/2
        imgProfilePic.clipsToBounds = true
        btnNext.layer.cornerRadius = 5.0
        btnNext.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
   
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if checkValidation() {
            uploadProfilePicture()
        }
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        Type.login = "1"
        let VC = UIStoryboard.welcome
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
    }
    
    func checkValidation() -> Bool {
        if imgProfilePic.image == UIImage(named: "camera") {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorProfilePic, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        return true
    }
    
    func uploadProfilePicture() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.picProfile)"
        print("\(apiURL)")
        
        let image = imgProfilePic.image
        imgData = image!.pngData()!
        
        let userID: String = String(format: "%d", UserDefault.standard.getCurrentUserId()!)
        let newTodo: [String: Any] = ["user_id": "\(userID)"]
        
        UtilityMethods.showIndicator()
        
        Alamofire.upload(multipartFormData: { MultipartFormData in
            
            MultipartFormData.append(self.imgData, withName: "uploadedfile",fileName: "image1.jpg", mimeType: "image/jpg")
            
            for (key, value) in newTodo {
                MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: apiURL, encodingCompletion: {
            (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                     UtilityMethods.hideIndicator()
                    do {
                        Type.login = "1"
                        
                        let vc = UIStoryboard.welcome
                        vc.modalPresentationStyle = .fullScreen
                        
                         self.present(vc, animated: true, completion: nil)
                        
                    } catch {
                        print("Failed saving to local")
                    }
                }
            case .failure(let encodingError):
                UtilityMethods.hideIndicator()
                print(encodingError)
            }
        })
    }
    
    // Mark :- MediaOpen
    
    @objc func actionMediaOpen(sender : UITapGestureRecognizer) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // Mark :- ImagePickerDelegate
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //        if imgDP.image == nil {
        var pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        pickedImage = pickedImage!.squareImage(with: CGSize(width: 300, height: 300))
        imgProfilePic.image = pickedImage

        dismiss(animated:true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
