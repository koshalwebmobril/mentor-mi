//
//  EducationInfoVC.swift
//  Mentor_Mi
//
//  Created by Manish on 20/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class EducationInfoVC: UIViewController {
    @IBOutlet weak var txtuniversity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCollege: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFormalTraining: SkyFloatingLabelTextField!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    func setUI() {
        
        btnNext.layer.cornerRadius = 5.0
        btnNext.clipsToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if checkValidation() {
            self.educationInfo(university: txtuniversity.text!, college: txtCollege.text!, training: txtFormalTraining.text!)
        }
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        Type.login = "1"
        let VC = UIStoryboard.personal
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func checkValidation() -> Bool {
        if txtuniversity.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorUniversity, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtCollege.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorCollege, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtFormalTraining.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorTraining, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        return true
    }
    
    
    func educationInfo(university: String, college: String, training: String) {
        let temp = UserDefault.standard.getCurrentUserId()
        let parameters = ["user_id": temp!, "formal_training": training, "college_name" : college, "university_name" : university] as [String:Any]
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.education(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleEducationResponse(response: response)
        }
    }
    
    func handleEducationResponse(response:  [String : Any]) {
        let result = JSON(response)
        if result["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: result["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            
            let VC = UIStoryboard.personal
            VC.modalPresentationStyle = .fullScreen
            self.present(VC, animated: true, completion: nil)
        }
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
