//
//  ViewController.swift
//  Mentor_Mi
//
//  Created by Manish on 17/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnCreateAccount: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
   
    }

    func setUI() {
        btnLogin.layer.cornerRadius = 5.0
        btnLogin.clipsToBounds = true
        btnCreateAccount.layer.cornerRadius = 5.0
        btnCreateAccount.clipsToBounds = true
        
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
        
//        self.present(UIStoryboard.login, animated: true, completion: nil)
    }
    
    @IBAction func actionCreateAccount(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
//        self.present(UIStoryboard.registration, animated: true, completion: nil)
//          self.navigationController?.pushViewController(UIStoryboard.registration, animated: true)
    }
    
}

