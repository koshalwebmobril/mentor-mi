//
//  SendTutorialVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 10/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import SDWebImage

class SendTutorialVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var btnMentor: UIButton!
    @IBOutlet weak var btnMentes: UIButton!
    @IBOutlet weak var btngroup: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var tableLists: UITableView!
    
    var arrMentor = [JSON]()
    var arrMentees = [JSON]()
    var arrGroup = [JSON]()
    var isMentor = false
    var isMentees = false
    var isGroup = false
    var isShare = false
    var strTitle = String()
    var strContent = String()
    var strAccess = String()
    var strLevel = String()
    var strTag = String()
    var strClassId = String()
    var pathVideos : URL?
    var imgData = Data()
    var dataTutorial = Data()
    var imgView = UIImageView()
    var thumbnail = String()
    var strShare = String()
    var strMentorID = String()
    var strMenteesID = String()
    var strGroupID = String()
    var timing = String()
    var date = String()
    var arrMentorSelect = NSMutableArray()
    var arrmenteesSelect = NSMutableArray()
    var arrGroupSelect = NSMutableArray()
    
    //    arrMentorSelect.count = 0
    //    arrmenteesSelect.count = 0
    //    arrGroupSelect.count = 0
    //
    var fromWhere = String()
    var strPostId = String()
    let appConstant:AppConstants = AppConstants()
    var isOn = true
    @IBOutlet weak var btnSkip: UIButton!
    var isCheckTutorialCreated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sending to"
        navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        tabBarController?.tabBar.isHidden = true
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
//        tableLists.tableFooterView = UIView()
        tableLists.layer.masksToBounds = true
        tableLists.layer.cornerRadius = 10
        
        if fromWhere == "Calender" {
            btnSkip.isHidden = true
            
        }
        
    }
    
    @IBAction func actionMentors(_ sender: Any) {
        getMentorsLists(id: UserDefault.standard.getCurrentUserId()!)
        
    }
    
    @IBAction func actionMentees(_ sender: Any) {
        getMenteesLists(id: UserDefault.standard.getCurrentUserId()!)
        
    }
    
    @IBAction func actionGroup(_ sender: Any) {
        getGroupLists(id: UserDefault.standard.getCurrentUserId()!)
        
    }
    
    func getMentorsLists(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.mentorLists(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleMentorListsResponse(response: response)
        }
    }
    
    func handleMentorListsResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrMentor = tempResponse["data"].arrayValue
            isMentor = true
            isMentees = false
            isGroup = false
            tableLists.isHidden = false
            tableLists.reloadData()
            tableLists.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    func getMenteesLists(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.menteesLists(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleMenteesListsResponse(response: response)
        }
    }
    
    func handleMenteesListsResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrMentees = tempResponse["data"].arrayValue
            isMentor = false
            isMentees = true
            isGroup = false
            tableLists.isHidden = false
            tableLists.reloadData()
            tableLists.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    func getGroupLists(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.groupByUsers(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleGroupListsResponse(response: response)
        }
    }
    
    func handleGroupListsResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrGroup = tempResponse["data"].arrayValue
            isMentor = false
            isMentees = false
            isGroup = true
            tableLists.isHidden = false
            tableLists.reloadData()
            tableLists.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    
    
    
    @IBAction func actionShare(_ sender: Any) {
        
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        appDelegate.goToHome()
    }
    @IBAction func actionSend(_ sender: Any) {
        
                if fromWhere == "Calender" {
                    sendScheduleVideo()
        }
        
        
       else if fromWhere == "Now" || fromWhere == "" {
            
            if arrMentorSelect.count == 0 && arrmenteesSelect.count == 0 && arrGroupSelect.count == 0 {
                let alert = UIAlertController(title: "Mentor-Mi", message: "Please select anyone to send, or click on SKIP button.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

            else {
                strGroupID = ""
                strMentorID = ""
                strMenteesID = ""
                strShare = ""
                
                if arrGroup.count > 0 {
                    strGroupID = arrGroupSelect.componentsJoined(by: ",")
                }
                if arrMentor.count > 0 {
                    strMentorID = arrMentorSelect.componentsJoined(by: ",")
                }
                if arrMentees.count > 0 {
                    strMenteesID = arrmenteesSelect.componentsJoined(by: ",")
                }
                
                shareNow(id: UserDefault.standard.getCurrentUserId()!,shareTo: strShare, mentorId: strMentorID, menteesId: strMenteesID,groupId: strGroupID, postId: strPostId)
            }
        }
        else {
            self.appDelegate.goToHome()
        }
    }
    
    func shareNow(id: Int,shareTo: String, mentorId: String, menteesId: String,groupId: String, postId: String) {
        
        let parameters = ["user_id": id, "share_to":shareTo, "mentors_id":mentorId, "mentees_id":menteesId, "group_id":groupId, "post_id":postId] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.shareTutorials(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleShareResponse(response: response)
        }
    }
    
    func handleShareResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.appDelegate.goToHome()
        }
    }
    
    @IBAction func actionReset(_ sender: Any) {
        
    }
    
    @IBAction func actionDelete(_ sender: Any) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0 {
            if isMentor == true {
                return arrMentor.count
            }
            else if isMentees == true {
                return arrMentees.count
            }
            else if isGroup == true {
                return arrGroup.count
            }
            else {
                return 0
            }
//        }
//        else  {
//            return 1
//        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.section == 0 {
//            let cell = tableLists.dequeueReusableCell(withIdentifier: "cell1") as! SendTutorialCell
            var finalcell = UITableViewCell()
            if isMentor == true {
                
                let cell = tableLists.dequeueReusableCell(withIdentifier: "mentorCell") as! SendTutorialCell
                
                cell.btnUncheck.tag = indexPath.row
                cell.btnUncheck.addTarget(self, action: #selector(mentorSelection(_:)), for: .touchUpInside)
                cell.lblName.text = String(format:"%@ %@",arrMentor[indexPath.row]["first_name"].stringValue,arrMentor[indexPath.row]["last_name"].stringValue)
                cell.lblDesignation.text = String(format:"%@",arrMentor[indexPath.row]["occupation"].stringValue)
                
                cell.imgDP.layer.masksToBounds = true
                cell.imgDP.layer.cornerRadius = cell.imgDP.frame.height/2
                
                let tempProfile = String(format:"%@", arrMentor[indexPath.row]["profile_pic"].stringValue)
                cell.imgDP.sd_setImage(with: URL(string: tempProfile), placeholderImage: UIImage(named: "trending3"))
                
                finalcell = cell
                
            }
            else if isMentees == true {
                let cell = tableLists.dequeueReusableCell(withIdentifier: "menteesCell") as! SendTutorialCell
                cell.btnUncheck.tag = indexPath.row
                cell.btnUncheck.addTarget(self, action: #selector(menteesSelection(_:)), for: .touchUpInside)
                
                cell.lblName.text = String(format:"%@ %@",arrMentees[indexPath.row]["first_name"].stringValue, arrMentees[indexPath.row]["last_name"].stringValue)
                cell.lblDesignation.text = String(format:"%@", arrMentees[indexPath.row]["occupation"].stringValue)
                
                cell.imgDP.layer.masksToBounds = true
                cell.imgDP.layer.cornerRadius = cell.imgDP.frame.height/2
                let tempProfile = String(format:"%@", arrMentees[indexPath.row]["profile_pic"].stringValue)
                cell.imgDP.sd_setImage(with: URL(string: tempProfile), placeholderImage: UIImage(named: "trending3"))
                
                finalcell = cell
            }
            else if isGroup == true {
                
                let cell = tableLists.dequeueReusableCell(withIdentifier: "groupCell") as! SendTutorialCell

                
                cell.btnUncheck.tag = indexPath.row
                cell.btnUncheck.addTarget(self, action: #selector(groupsSelection(_:)), for: .touchUpInside)
                
                cell.lblName.text = String(format:"%@", arrGroup[indexPath.row]["group_name"].stringValue)
                cell.lblDesignation.text = arrGroup[indexPath.row]["occupation"].stringValue
                cell.imgDP.layer.masksToBounds = true
                cell.imgDP.layer.cornerRadius = cell.imgDP.frame.height/2
                let tempProfile = arrGroup[indexPath.row]["profile_pic"].stringValue
                cell.imgDP.sd_setImage(with: URL(string: tempProfile), placeholderImage: UIImage(named: "trending3"))
                
                finalcell = cell
            }
            else {
                
            }
            return finalcell
//        }
//        else {
//            let cell = tableLists.dequeueReusableCell(withIdentifier: "cell2") as! SendTutorialCell
//            cell.btnOK.addTarget(self, action: #selector(Done), for: .touchUpInside)
//            return cell
//        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 80
        }
        else  {
            return 60
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 60))
        view.backgroundColor = .white
        let button = UIButton(frame: CGRect(x: view.frame.width/2 - 60, y: 10, width: 120, height: 40))
        button.setTitle("Ok", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(Done), for: .touchUpInside)
        button.backgroundColor = .appDefaultColor
        view.addSubview(button)
        
        return view
    }
    
    @objc func mentorSelection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableLists)
        let indexPath = self.tableLists.indexPathForRow(at:buttonPosition)!
        let cell = self.tableLists.cellForRow(at: indexPath) as! SendTutorialCell
        
        
        if sender.isSelected{
            
            cell.btnUncheck.setImage(UIImage(named: "checked"), for: .normal)
            
//            sender.setImage(UIImage(named: "checked"), for: .normal)
            let tempId = String(format:"%@",arrMentor[indexPath.row]["m_id"].stringValue)
            arrMentorSelect.add(tempId)
            print("selected....")
        }else{
            cell.btnUncheck.setImage(UIImage(named: "checkBox"), for: .normal)
//            sender.setImage(UIImage(named: "checkBox"), for: .normal)
            let tempId = String(format:"%@", arrMentor[indexPath.row]["m_id"].stringValue)
            
            if arrMentorSelect.contains(tempId) {
                for i in arrMentorSelect {
                    
                    arrMentorSelect.remove(i)
                }
            }
             print("Deselected....")
        }
     
    }
    
    @objc func menteesSelection(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableLists)
        let indexPath = self.tableLists.indexPathForRow(at:buttonPosition)!
        let cell = self.tableLists.cellForRow(at: indexPath) as! SendTutorialCell
        
        if sender.isSelected{
            cell.btnUncheck.setImage(UIImage(named: "checked"), for: .normal)
            
            //            sender.setImage(UIImage(named: "checked"), for: .normal)
            let tempId = String(format:"%@",arrMentees[indexPath.row]["m_id"].stringValue)
            arrmenteesSelect.add(tempId)
            print("selected....")
        }else{
            cell.btnUncheck.setImage(UIImage(named: "checkBox"), for: .normal)
            //            sender.setImage(UIImage(named: "checkBox"), for: .normal)
            let tempId = String(format:"%@", arrMentees[indexPath.row]["m_id"].stringValue)
            
            if arrmenteesSelect.contains(tempId) {
                for i in arrmenteesSelect {
                    
                    arrmenteesSelect.remove(i)
                }
            }
            print("Deselected....")
        }
        
        
    }
    
    @objc func groupsSelection(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableLists)
        let indexPath = self.tableLists.indexPathForRow(at:buttonPosition)!
        let cell = self.tableLists.cellForRow(at: indexPath) as! SendTutorialCell
        if sender.isSelected{
            
            cell.btnUncheck.setImage(UIImage(named: "checked"), for: .normal)
            
            //                   sender.setImage(UIImage(named: "checked"), for: .normal)
            let tempId = String(format:"%@",arrGroup[indexPath.row]["group_id"].stringValue)
            arrGroupSelect.add(tempId)
            print("selected....")
        }else{
            cell.btnUncheck.setImage(UIImage(named: "checkBox"), for: .normal)
            //                   sender.setImage(UIImage(named: "checkBox"), for: .normal)
            let tempId = String(format:"%@", arrGroup[indexPath.row]["group_id"].stringValue)
            
            if arrGroupSelect.contains(tempId) {
                for i in arrGroupSelect {
                    
                    arrGroupSelect.remove(i)
                }
            }
            print("Deselected....")
        }
        
        
    }
    
    
    
    @objc func Done(_ sender: UIButton) {
        tableLists.isHidden = true
        if isMentor == true {
            if arrMentorSelect.count > 0 {
                btnMentor.setImage(UIImage(named: "checked"), for: .normal)
            }
            else {
                btnMentor.setImage(UIImage(named: "checkBox"), for: .normal)
            }
        }
        else if isMentees == true {
            if arrmenteesSelect.count > 0 {
                btnMentes.setImage(UIImage(named: "checked"), for: .normal)
            }
            else {
                btnMentes.setImage(UIImage(named: "checkBox"), for: .normal)
            }
        }
        else if isGroup == true {
            if arrGroupSelect.count > 0 {
                btngroup.setImage(UIImage(named: "checked"), for: .normal)
            }
            else {
                btngroup.setImage(UIImage(named: "checkBox"), for: .normal)
            }
        }
        else {
            
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func sendScheduleVideo(){
        if isCheckTutorialCreated == false{
            isCheckTutorialCreated = true
        strGroupID = ""
        strMentorID = ""
        strMenteesID = ""
        strShare = ""
        
        if arrGroupSelect.count > 0 {
            strGroupID = arrGroupSelect.componentsJoined(by: ",")
            //                strGroupID = arrGroupSelect.
        }
        if arrMentorSelect.count > 0 {
            strMentorID = arrMentorSelect.componentsJoined(by: ",")
        }
        if arrmenteesSelect.count > 0 {
            strMenteesID = arrmenteesSelect.componentsJoined(by: ",")
        }
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.scheduleSend)"
        print("\(apiURL)")
        
        var movieData:Data?
        //do{
        movieData = UserDefaults.standard.object(forKey: "videoPath") as? Data
        if let data=UserDefaults.standard.object(forKey: "videoPathURLLOCALThumbnail") as? Data{
        
        imgData = data
        }
        let userID = UserDefault.standard.getCurrentUserId()!
        
        let newTodo: [String: Any] = ["user_id":"\(userID)","class_id":strClassId,"post_title":"\(strTitle)","post_content":"\(strContent)","post_accessibility": self.strAccess,"difficulty_level":"\(strLevel)","tag":"\(strTag)","schedule_date":date,"schedule_time":"\(timing)","share_to":"\(strShare)","mentors_id":"\(strMentorID)","mentees_id":"\(strMenteesID)","group_id":"\(strGroupID)"]
        print("parameters are \(newTodo)")
        
//                    SVProgressHUD.show(withStatus: "Loading...")
        UtilityMethods.showIndicator()
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.imgData, withName: "thumbimg",fileName: "profile.jpg", mimeType: "image/jpg")
            multipartFormData.append(movieData!, withName: "uploadedvediofile", fileName: "file.mp4", mimeType: "video/mp4")
            
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:apiURL)
        {
            (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
                upload.responseJSON { response in
                    
//                                            SVProgressHUD.dismiss()
                    UtilityMethods.hideIndicator()
                     if  response.result.value != nil{
                    print(response.result.value!)
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    
                    if swiftyJsonVar["status"].string == "success" {
                        self.isCheckTutorialCreated = false
                        let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            self.appDelegate.goToHome()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                     
                    else {
                        self.isCheckTutorialCreated = false
                        let alert = UIAlertController(title: "Mentor-Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    }
                }
            case .failure(let encodingError):
                self.isCheckTutorialCreated = false
                let alert = UIAlertController(title: "Mentor Mi", message: "The request timed out.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                }))
                self.present(alert, animated: true, completion: nil)
                print(encodingError)
            }
        }
    }
    }
    
    
}
