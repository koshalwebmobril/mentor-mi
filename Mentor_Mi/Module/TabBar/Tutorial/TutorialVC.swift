//
//  TutorialVC.swift
//  Mentor_Mi
//
//  Created by Manish on 11/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TutorialVC: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    let appConstant:AppConstants = AppConstants()
    var controllerArray : [UIViewController] = []
    var isOn = true
    var dictTutorial = JSON()

    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var btbAdd: UIButton!
    @IBOutlet weak var threedotsinfo: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
       if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)
        notificationCenterData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func notificationCenterData() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(MoveTo), name: NSNotification.Name(rawValue: "moveto"), object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(playFromTutorial), name: NSNotification.Name(rawValue: "playFromTutorial"), object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(playFromTutorialbyMe), name: NSNotification.Name(rawValue: "playFromTutorialbyMe"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        isOn = true

        setPageMenu()
        setNavigationUI()
        
    }
    
    func setNavigationUI() {
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
//        navigationController?.view.addSubview(threedotsinfo)
//        navigationController?.view.addSubview(statusView)
        view.addSubview(threedotsinfo)
        view.addSubview(statusView)
        self.tabBarController?.title = "Tutorial"

        self.tabBarController?.tabBar.isHidden = false
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        tabBarController?.navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        
        threedotsinfo.isHidden = true
        isOn = true
    }
    
    func setPageMenu() {
        controllerArray.removeAll()
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "OtherTutorialVC") as? OtherTutorialVC
        controller?.title="MENTORS"
        controllerArray.append(controller!)
        let controller1 = self.storyboard?.instantiateViewController(withIdentifier: "MyTutorialVC") as? MyTutorialVC
        controller1?.title="MI"
        controllerArray.append(controller1!)
        let parameters: [CAPSPageMenuOption] = [.selectionIndicatorColor(appConstant.bottomTitleColor), .menuItemSeparatorWidth(0.1), .useMenuLikeSegmentedControl(true), .menuItemSeparatorPercentageHeight(0.1),.selectedMenuItemLabelColor(UIColor.darkGray),.unselectedMenuItemLabelColor(UIColor.gray),.scrollMenuBackgroundColor(appConstant.navigationColor)]
        
        let screenSize:CGFloat=view.frame.height
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0, y:(screenSize>appConstant.iPhone8 ? 90 : 67), width:self.view.frame.width, height:self.view.frame.height - 84), pageMenuOptions: parameters)
        
        threedotsinfo.isHidden = true
        
        self.view.addSubview(pageMenu!.view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        btbAdd.layer.cornerRadius = btbAdd.frame.height/2
//        btbAdd.layer.borderColor = UIColor.white.cgColor
//        btbAdd.layer.borderWidth = 5.0
        btbAdd.clipsToBounds = true
        
        self.view.addSubview(btbAdd)
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    @objc func MoveTo(notication: NSNotification){
        
        
    
        self.navigationController?.pushViewController(UIStoryboard.createClass, animated: true)
    }
    
    @objc func playFromTutorial(notification: NSNotification) {
        let controller = UIStoryboard.playerThumbnail
        controller.dictTutorial = dictVideoData
        controller.fromWhere = "otherTutorial"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func playFromTutorialbyMe(notification: NSNotification) {
        let controller = UIStoryboard.playerThumbnail
        controller.dictTutorial = dictVideoData
        controller.fromWhere = "myTutorial"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    
    @objc func openCreateClass() {
        self.navigationController?.pushViewController(UIStoryboard.uploadVideo, animated: true)
    }
    
    @IBAction func actionAddClass(_ sender: Any) {
        self.navigationController?.pushViewController(UIStoryboard.uploadVideo, animated: true)
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusTxt.text!]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        UserDefault.standard.setStatus(type: self.statusTxt.text!)
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    
    
}
