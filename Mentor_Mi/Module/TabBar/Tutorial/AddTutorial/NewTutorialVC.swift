//
//  NewTutorialVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import AssetsLibrary
import Photos
import DropDown
import Alamofire
import SwiftyJSON
import SkyFloatingLabelTextField


class NewTutorialVC: UIViewController {
    
    var isOn = true
    var dataTutorial = Data()
    let pathVideo = UserDefaults.standard.object(forKey: "videoPath")
    var imgData = Data()
    var arrLevel = NSArray()
    var isShowLevel = false
    var dropDown = DropDown()
    var arrMyClassName = [String]()
    var arrMyClassId = [String]()
    var dropDownClass = DropDown()
    var strClassname = String()
    var strClassId = String()
    var imgView = UIImageView()
    var thumbnail = String()
    var strAccess = "0"
    var level = ""
    var strAboutMe = String()
    var strDate = String()
    var strTime = String()
    var strDateTime = String()
    var isSchedule = "No"

    
    @IBOutlet weak var txtClass: SkyFloatingLabelTextField!
    @IBOutlet weak var bgViewTitle: UIView!
    @IBOutlet weak var bgViewDificultyLevel: UIView!
    @IBOutlet weak var txtDescription: SkyFloatingLabelTextField!
    @IBOutlet weak var bgViewtag: UIView!
    @IBOutlet weak var switchPubliPrivate: UISwitch!
    @IBOutlet weak var txtLevelDificulty: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtTag: SkyFloatingLabelTextField!
    @IBOutlet weak var btnDificultLevel: UIButton!
    @IBOutlet weak var tbleDifiLevel: UITableView!
    @IBOutlet weak var threedotsinfo: UIView!
    @IBOutlet weak var btnClassSelection: UIButton!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var bgCalender: UIView!
    @IBOutlet weak var datePick: UIDatePicker!
    
    var isCheckTutorialCreated = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationUI()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }

        arrLevel = ["Advanced","Intermediate","Beginner"]
        
        self.dropDown.dataSource = self.arrLevel as! [String]
        dropDown.anchorView = self.txtLevelDificulty
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLevelDificulty.text = self.arrLevel[index] as? String
            self.level = String(format:"%d",index + 1)
        }
        
        do {
//            print(pathVideo)
        }
            
        catch let error as NSError {
            print("Error generating thumbnail: \(error)")
        }
        
    }
    
    func setNavigationUI() {
        self.title = "Create New Tutorial"
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        tabBarController?.navigationController?.isNavigationBarHidden = true
        
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        arrMyClassName = [String]()
        classCreateByMeAPI(userId: UserDefault.standard.getCurrentUserId()!)
        
        dropDownClass.anchorView = self.btnClassSelection
        dropDownClass.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.strClassname = (self.arrMyClassName[index])
            self.strClassId = (self.arrMyClassId[index])
           
            self.txtClass.text = "\(self.strClassname)"
            self.btnClassSelection.setTitle(self.strClassname, for: .normal)
        }
        
    }
    
    func classCreateByMeAPI(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.myClassList(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleMyClassListResponse(response: response)
        }
    }
    
    func handleMyClassListResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            print(tempResponse["data"].arrayValue)
            let tempArray = tempResponse["data"].arrayValue
            for i in 0..<tempArray.count {
                let singleState = tempArray[i]["class_name"].stringValue
                self.arrMyClassName.append(singleState)
                let singleStateId = tempArray[i]["id"].stringValue
                self.arrMyClassId.append(singleStateId)
            }
            self.dropDownClass.dataSource = self.arrMyClassName
        }
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
            threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func actionClass(_ sender: Any) {
        if arrMyClassName.count > 0 {
            dropDownClass.show()
        }
        else {
            
            let alert = UIAlertController(title: "Mentor Mi", message: "No class found ! Please create class", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { action in
                
               // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
    }
    
    @IBAction func actionLevelDifficult(_ sender: Any) {
        if isShowLevel == false {
            isShowLevel = true
            dropDown.show()
        }
        else {
            isShowLevel = false
            dropDown.hide()
        }
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
       
            strAboutMe = statusTxt.text!
            uploadStatus(text: strAboutMe, user_id: UserDefault.standard.getCurrentUserId()!)
        }
        statusTxt.text = ""
    
    }
    
    @IBAction func actionSend(_ sender: Any) {
        if validationCheck() {
            if isSchedule == "Yes" {
                submitWithSchedule()
            }
            else {
                submitTutorial()
            }
        }
    }
    
    func validationCheck() -> Bool {
        if txtTitle.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorTutorialTitle, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtLevelDificulty.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorDificultyLevel, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtTag.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorTag, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtDescription.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorDescription, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if strClassname == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorClassName, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false

        }
        return true
    }
    
    func submitTutorial() {
        if isCheckTutorialCreated == false{
            isCheckTutorialCreated = true
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.postTutorial)"
        print("\(apiURL)")
        
        var movieData:Data?
        //            do{
        
        movieData = UserDefaults.standard.object(forKey: "videoPath") as? Data
        
        if let data=UserDefaults.standard.object(forKey: "videoPathURLLOCALThumbnail") as? Data{
        
        imgData = data
        }
        
        let userID: String = String(format: "%d", UserDefault.standard.getCurrentUserId()!)
        
        let newTodo: [String: Any] = ["user_id":userID,"class_id":strClassId,"post_title":"\(txtTitle.text ?? "")","post_content":"\(txtDescription.text ?? "")","post_accessibility": self.strAccess,"difficulty_level":self.level,"tag":"\(txtTag.text ?? "")"]
        print("parameters are \(newTodo)")
        
        UtilityMethods.showIndicator()
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.imgData, withName: "thumbimg",fileName: "profile.jpg", mimeType: "image/jpg")
            multipartFormData.append(movieData!, withName: "uploadedvediofile", fileName: "file.mp4", mimeType: "video/mp4")
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:apiURL)
        {
            (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
                upload.responseJSON { response in
                    
                    UtilityMethods.hideIndicator()

//                    print(response.result.value!)
                    if let response = response.result.value{
                    let swiftyJsonVar = JSON(response)
                    print(swiftyJsonVar)
                    
                    if swiftyJsonVar["status"].string == "success" {
                        self.isCheckTutorialCreated = false
                        let sending = self.storyboard?.instantiateViewController(withIdentifier: "SendTutorialVC") as! SendTutorialVC
                        sending.strPostId = swiftyJsonVar["data"]["post_id"].stringValue
                        sending.modalPresentationStyle = .fullScreen
                        self.present(sending, animated: true, completion: nil)
                    }
                    else {
                        self.isCheckTutorialCreated = false
                        if swiftyJsonVar["message"].stringValue == "Mentor-Mi, You are not a Subscribed user to Add more Tutorials." {
                            
                            let alert = UIAlertController(title: "Mentor Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                              

                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            self.isCheckTutorialCreated = false
                            let alert = UIAlertController(title: "Mentor-Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.isCheckTutorialCreated = false
                let alert = UIAlertController(title: "Mentor Mi", message: "The request timed out.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                }))
                self.present(alert, animated: true, completion: nil)
                UtilityMethods.hideIndicator()

            }
        }
    }
    }
    
    func submitWithSchedule() {
        let sending = self.storyboard?.instantiateViewController(withIdentifier: "SendTutorialVC") as! SendTutorialVC
        sending.fromWhere = "Calender"
        sending.strClassId = strClassId
        sending.strTitle = txtTitle.text!
        sending.strContent = txtDescription.text!
        sending.strAccess = strAccess
        sending.strLevel = level
        sending.strTag = txtTag.text!
        sending.date = strDate
        sending.timing = strTime
//        self.navigationController?.pushViewController(sending, animated: true)
        sending.modalPresentationStyle = .fullScreen
        self.present(sending, animated: true)
    }
    
    func uploadStatus(text: String, user_id: Int) {
        let parameters = ["user_id" : user_id, "status":text] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.uploadStatus(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleStatusUploadResponse(response: response)
        }
    }
    
    func handleStatusUploadResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            UserDefault.standard.setStatus(type: strAboutMe)
        }
    }
   

    @IBAction func actionSelectDate(_ sender: Any) {
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: (sender as AnyObject).date)
        strDateTime = selectedDate
        let line = selectedDate.components(separatedBy: " ")
        strDate = line[0]
        strTime = line[1]
    }
    
    @IBAction func actionDone(_ sender: Any) {
        isSchedule = "Yes"
        bgCalender.isHidden = true
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        strDate = ""
        strTime = ""
        strDateTime = ""
        bgCalender.isHidden = true
        isSchedule = "No"
        
    }
    @IBAction func actionSchedule(_ sender: Any) {
        
        bgCalender.isHidden = false
        
    }
    @IBAction func actionback(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
