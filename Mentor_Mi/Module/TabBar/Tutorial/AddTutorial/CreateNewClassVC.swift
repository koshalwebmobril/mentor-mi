
//
//  CreateNewClassVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 10/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import SkyFloatingLabelTextField
import Alamofire
import SVProgressHUD
import SwiftyJSON

class CreateNewClassVC: UIViewController {

    @IBOutlet weak var txtClassName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnCrreate: UIButton!
    
     let appConstant:AppConstants = AppConstants()
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCrreate.layer.cornerRadius = 5.0
        btnCrreate.clipsToBounds = true
        btnCrreate.layer.borderColor = UIColor.black.cgColor
        btnCrreate.layer.borderWidth = 1.5
        
       if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden=true
        
        tabBarController?.navigationController?.isNavigationBarHidden=true
        
    }
  
 
    @IBAction func actionCreate(_ sender: Any) {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.createClass)"
        print("\(apiURL)")
        
        let userID = UserDefaults.standard.object(forKey: "user_id") as! String
        let newTodo: [String: Any] = ["user_id": userID,"class_name":"\(txtClassName.text ?? "")","class_accessibility":"1"]
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                   
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        
        let newClass = self.storyboard?.instantiateViewController(withIdentifier: "NewTutorialVC") as! NewTutorialVC
        newClass.modalPresentationStyle = .fullScreen
        self.present(newClass, animated: true, completion: nil)
    }
    
    
    
    
    

}
