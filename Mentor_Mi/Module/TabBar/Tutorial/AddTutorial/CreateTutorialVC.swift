//
//  CreateTutorialVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import AssetsLibrary
import Photos
import DropDown
import Alamofire
import SwiftyJSON
import SkyFloatingLabelTextField


class CreateTutorialVC: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    var isOn = true
    var dataTutorial = Data()
    let pathVideo = UserDefaults.standard.object(forKey: "videoPath")
    var imgData = Data()
    var arrLevel = [JSON]()
    var isShowLevel = false
    var dropDown = DropDown()
    var arrMyClassName = [String]()
    var arrMyClassId = [String]()
    var dropDownClass = DropDown()
    var strClassname = String()
    var strClassId = String()
    var imgView = UIImageView()
    var thumbnail = String()
    var strAccess = "0"
    var level = ""
    var strAboutMe = String()
    var arrTimeLine = [JSON]()

    
    @IBOutlet weak var bgViewTitle: UIView!
    @IBOutlet weak var bgViewDificultyLevel: UIView!
    @IBOutlet weak var txtDescription: SkyFloatingLabelTextField!
    @IBOutlet weak var bgViewtag: UIView!
    @IBOutlet weak var switchPubliPrivate: UISwitch!
    @IBOutlet weak var txtLevelDificulty: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtTag: SkyFloatingLabelTextField!
    @IBOutlet weak var btnDificultLevel: UIButton!
    @IBOutlet weak var tbleDifiLevel: UITableView!
    @IBOutlet weak var btnClassSelection: UIButton!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    
    var isChecktutorialCreated = false

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden=true
        tabBarController?.navigationController?.isNavigationBarHidden=true
        
        arrLevel = ["Advanced","Intermediate","Beginner"]
        
        self.dropDown.dataSource = self.arrLevel as! [String]
        dropDown.anchorView = self.txtLevelDificulty
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLevelDificulty.text = self.arrLevel[index] as? String
            self.level = String(format:"%d",index + 1)
        }
        
        do {
            print(pathVideo)
        }
            
        catch let error as NSError {
            print("Error generating thumbnail: \(error)")
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        arrMyClassName = [String]()
        createClass(id: UserDefault.standard.getCurrentUserId()!)
        
        dropDownClass.anchorView = self.btnClassSelection
        dropDownClass.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.strClassname = (self.arrMyClassName[index])
            self.strClassId = (self.arrMyClassId[index])
            self.btnClassSelection.setTitle(self.strClassname, for: .normal)
        }
    }
    
    func createClass(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.myClassList(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleMyClassResponse(response: response)
        }
    }
    
    func handleMyClassResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            let tempArray = tempResponse["data"].arrayValue
            for i in 0..<tempArray.count {
                let singleState = tempArray[i]["class_name"].stringValue
                self.arrMyClassName.append(singleState)
                let singleStateId = tempArray[i]["id"].stringValue
                self.arrMyClassId.append(singleStateId)
            }
            self.dropDownClass.dataSource = self.arrMyClassName
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
            strAboutMe = statusTxt.text!
            uploadStatus(text: strAboutMe, user_id: UserDefault.standard.getCurrentUserId()!)
        }
        statusTxt.text = ""
    
    }
    
    func uploadStatus(text: String, user_id: Int) {
        let parameters = ["user_id" : user_id, "status":text] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.uploadStatus(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleStatusUploadResponse(response: response)
        }
    }
    
    func handleStatusUploadResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            getProfile(userId: UserDefault.standard.getCurrentUserId()!)
        }
    }
    
    
    func getProfile(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
    }
    
    func handleUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            myData.myDetails = tempResponse["data"]
            getTimeLine(userId: UserDefault.standard.getCurrentUserId()!)
        }
    }
    
    func getTimeLine(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getTimeLine(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTimeLineResponse(response: response)
        }
    }
    
    
    func handleTimeLineResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrTimeLine = tempResponse["data"].arrayValue
            
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
