//
//  UploadVideoVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import AssetsLibrary
import Photos
import MobileCoreServices
import Fusuma
import MobileCoreServices

class UploadVideoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var movieData:Data?
    var isMedia = false
    var path:URL?
    var isGroup = false
    var groupId = String()
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var imagePickerController = UIImagePickerController()
    var videoURL : NSURL?
    let appConstant:AppConstants = AppConstants()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var btnUpload: UIButton!
    
    var tempURL : URL!
    var finalurl = String()
    var strGroupId = String()
    var isClassType = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.isNavigationBarHidden=true
//        tabBarController?.navigationController?.isNavigationBarHidden=true

        tabBarController?.navigationController?.isNavigationBarHidden = true
        navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = true
        
        VideoHelper.startMediaBrowser(delegate: self as UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate, sourceType: .camera)
               let movieOutput = AVCaptureMovieFileOutput()

               movieOutput.maxRecordedDuration = CMTime(seconds: 10, preferredTimescale: 600)
               if let session = session?.canAddOutput(movieOutput) {
                   //session.addOutput(movieOutput)
               }
        
//        setVideoLayout()
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
               
    }
    
    func setVideoLayout() {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        let backCamera =  AVCaptureDevice.default(for: AVMediaType.video)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey:  AVVideoCodecJPEG]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity =    AVLayerVideoGravity.resizeAspect
                videoPreviewLayer!.connection?.videoOrientation =   AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(videoPreviewLayer!)
//                self.view.addSubview(previewView)
                session!.startRunning()
            }
        }
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
        //videoPreviewLayer!.frame = CGRect(x:0 , y:0, width:view.frame.width, height:view.frame.height)
        
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
        let title = (error == nil) ? "Success" : "Error"
        let message = (error == nil) ? "Video was saved" : "Video failed to save"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL else { return }
        if info[UIImagePickerController.InfoKey.mediaURL] as? URL != nil{
        let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as! URL
            finalurl = String(describing: videoURL)
        }
        
        let thumbnail = PhotoServices.shared.getThumbnailFrom(path: url)
        let imgData = thumbnail!.jpegData(compressionQuality: 1.0)!
        let defaults = UserDefaults.standard
         defaults.set(finalurl, forKey: "videoPathhhh")
        defaults.set(url, forKey: "videoPathURLLOCAL")
        defaults.set(imgData, forKey: "videoPathURLLOCALThumbnail")
        
        do {
            let imageData = try Data(contentsOf: url as URL)
            let defaults = UserDefaults.standard
            defaults.set(imageData, forKey: "videoPath")
           
        } catch {
            print("Unable to load data: \(error)")
        }
        
        if isClassType == "GroupChat"{
            let upload = UIStoryboard.tutorialByGroup
             upload.strGroupId = strGroupId
            self.navigationController!.pushViewController(upload, animated: true)
        }
        else{
            let newClass = self.storyboard?.instantiateViewController(withIdentifier: "NewTutorialVC") as! NewTutorialVC
                   newClass.modalPresentationStyle = .fullScreen
            self.present(newClass, animated: true, completion: nil)
        }
        
       
        
        let imagePicker = UIImagePickerController()
        imagePicker.videoMaximumDuration = TimeInterval(30.0)
        
        // Handle a movie capture
//        UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(video(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    @IBAction func gallery(_ sender: Any) {
        imagePickerController.sourceType = .savedPhotosAlbum
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [kUTTypeMovie as String]
        imagePickerController.modalPresentationStyle = .fullScreen
        present(imagePickerController, animated: true, completion: nil)
        navigationController?.navigationBar.barTintColor = appDelegate.appConstant.navigationColor
        
    }
    
    @IBAction func video(_ sender: Any) {
        VideoHelper.startMediaBrowser(delegate: self as UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate, sourceType: .camera)
        let movieOutput = AVCaptureMovieFileOutput()
        
        movieOutput.maxRecordedDuration = CMTime(seconds: 10, preferredTimescale: 600)
        if let session = session?.canAddOutput(movieOutput) {
            //session.addOutput(movieOutput)
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   

}
