//
//  MyTutorialVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import  SwiftyJSON
import Alamofire
import SDWebImage
//import <#module#>

var dictVideoData = JSON()

class MyTutorialVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var arrMyTutorial = [JSON]()
    var isLists = false
    var arrBeforeFilter = [JSON]()
    var isFilterEnabled = false
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var lblVideosCount: UILabel!
    @IBOutlet weak var tableMentees: UITableView!

    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var advanceBtn: UIButton!
    
    @IBOutlet weak var beginerBtn: UIButton!
    @IBOutlet weak var intermediateBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
         tableMentees.heightAnchor.constraint(equalToConstant:
           tableMentees.contentSize.height + 50).isActive = true
    }

    @objc func reloadData(notification: NSNotification) {
        
        menteesTutorialAPI(id: UserDefault.standard.getCurrentUserId()!)
        
        print("reloadData")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        menteesTutorialAPI(id: UserDefault.standard.getCurrentUserId()!)
            advanceBtn.isSelected = false
            intermediateBtn.isSelected = false
            beginerBtn.isSelected = false
        
        isFilterEnabled = false
       
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name(rawValue: "reloadData"), object: nil)
        

    }
    
    
    func menteesTutorialAPI(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.myTutorial(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleMyTutorialResponse(response: response)
        }
    }
    
    func handleMyTutorialResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            
            if tempResponse["message"].stringValue == "No Post Found !!"{
             if isFilterEnabled {
                arrBeforeFilter.removeAll()
                self.tableMentees.reloadData()
            }
             else{
                arrMyTutorial.removeAll()
                self.tableMentees.reloadData()
            }
            }
            let alert = UIAlertController(title: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: {UIAction in
                self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrMyTutorial = tempResponse["data"].arrayValue
//            arrBeforeFilter = tempResponse["data"].arrayValue
            self.lblVideosCount.text = "\(arrMyTutorial.count) Videos"
            
            self.tableMentees.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFilterEnabled ? arrBeforeFilter.count : arrMyTutorial.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let CellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? TutorialCells
        var customCell = [Any]()
        if cell == nil {
            customCell = Bundle.main.loadNibNamed("TutorialCells", owner: self, options: nil) ?? [Any]()
        }
        
        messageLabel.isHidden = true
        cell?.selectionStyle = .default
        cell = customCell[0] as? TutorialCells
        cell?.deletePost.tag = indexPath.row
        cell?.deletePost.addTarget(self, action: #selector(buttonDeletePodt(sender:)), for: .touchUpInside)
        if isFilterEnabled{
            
            let strURL = String(format:"%@",arrBeforeFilter[indexPath.row]["video_thumbnail"].stringValue)
            cell?.imgTutorial.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            cell?.tutorialTitle.text = String(format:"%@",arrBeforeFilter[indexPath.row]["post_title"].stringValue)
            cell?.tutorialDuration.text = String(format:"%@",arrBeforeFilter[indexPath.row]["post_content"].stringValue)
            cell?.tutorialTag.text = String(format:"%@",arrBeforeFilter[indexPath.row]["post_tag"].stringValue)
            
            let post_id = String(format:"%@",arrBeforeFilter[indexPath.row]["id"].stringValue)
            
            
           // cell?.deletePost.tag = Int(post_id)!
            
        }else{
        
        let strURL = String(format:"%@",arrMyTutorial[indexPath.row]["video_thumbnail"].stringValue)
        cell?.imgTutorial.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
        cell?.tutorialTitle.text = String(format:"%@",arrMyTutorial[indexPath.row]["post_title"].stringValue)
        cell?.tutorialDuration.text = String(format:"%@",arrMyTutorial[indexPath.row]["post_content"].stringValue)
        cell?.tutorialTag.text = String(format:"%@",arrMyTutorial[indexPath.row]["post_tag"].stringValue)
        
        let post_id = String(format:"%@",arrMyTutorial[indexPath.row]["id"].stringValue)
        
        
        //cell?.deletePost.tag = Int(post_id)!
        }
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isLists {
            return 50
        }
        else {
            return 235
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        dictVideoData = isFilterEnabled ? arrBeforeFilter[indexPath.row] : arrMyTutorial[indexPath.row]
        NotificationCenter.default.post(name: Notification.Name("playFromTutorialbyMe"), object: nil)

//        let controller = UIStoryboard.playerThumbnail
//        controller.fromWhere = "myTutorial"
//        controller.dictTutorial = arrMyTutorial[indexPath.row]
//        print("SoapBox: \(arrMyTutorial[indexPath.row]), fromWhere: myTutorial")
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    @objc func buttonDeletePodt(sender: UIButton){
    var postID = String()
    var userID = String()
    if isFilterEnabled {
        postID = String(format:"%@",arrBeforeFilter[sender.tag]["id"].stringValue)
         userID = String(format:"%@",arrBeforeFilter[sender.tag]["user_id"].stringValue)
    }
    else{
       postID = String(format:"%@",arrMyTutorial[sender.tag]["id"].stringValue)
         userID = String(format:"%@",arrMyTutorial[sender.tag]["user_id"].stringValue)
    }
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.deletePost)"
                print("\(apiURL)")
                
               // let userID = UserDefault.standard.getCurrentUserId()!
                
                let newTodo: [String: Any] = ["user_id": userID, "post_id": postID]
                print(newTodo)
                
        //        SVProgressHUD.show(withStatus: "Loading...")
                Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                    if((response.result.value) != nil) {
        //                SVProgressHUD.dismiss()
                        let swiftyJsonVar = JSON(response.result.value!)
                        print(swiftyJsonVar)
                        if swiftyJsonVar["status"].string == "success" {
                            let responseJson = swiftyJsonVar["data"]
                            print(responseJson)
                            
                            let alertController = UIAlertController(title: StringConstants.Alert.Titles.Error, message: "Post Deleted Successfully", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                               self.menteesTutorialAPI(id: UserDefault.standard.getCurrentUserId()!)
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else {
                            
                        }
                        NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)
                        
                    }
                    else {
                        
        //                SVProgressHUD.dismiss()
                        
                    }
                }
        
    }
    
    
    
    @IBAction func addClassAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("moveto"), object: nil)
    }
    
    
    
    @IBAction func advancedAction(_ sender: Any) {
        
        advanceBtn.isSelected = true
        intermediateBtn.isSelected = false
        beginerBtn.isSelected = false
    
        isFilterEnabled = true
        
        arrBeforeFilter = arrMyTutorial.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "1"
        })
        
        self.lblVideosCount.text = "\(arrBeforeFilter.count) Videos"
        
        if arrBeforeFilter.count>0{
               DispatchQueue.main.async {
                   self.tableMentees.reloadData()
               }
               }else       {
                   messageLabel.isHidden = false
                   DispatchQueue.main.async {
                       self.tableMentees.reloadData()
                   }
               }
        
        
    }
    
    
    @IBAction func intermediateAction(_ sender: Any) {
        
        advanceBtn.isSelected = false
        intermediateBtn.isSelected = true
        beginerBtn.isSelected = false
        
        isFilterEnabled = true
        
        arrBeforeFilter = arrMyTutorial.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "2"
        })
        
        self.lblVideosCount.text = "\(arrBeforeFilter.count) Videos"
        
         if arrBeforeFilter.count>0{
               DispatchQueue.main.async {
                   self.tableMentees.reloadData()
               }
               }else       {
                   messageLabel.isHidden = false
                   DispatchQueue.main.async {
                       self.tableMentees.reloadData()
                   }
               }
    }
    
    @IBAction func beginerAction(_ sender: Any) {
        
        advanceBtn.isSelected = false
        intermediateBtn.isSelected = false
        beginerBtn.isSelected = true
        
        isFilterEnabled = true
        
        arrBeforeFilter = arrMyTutorial.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "3"
        })
        
        self.lblVideosCount.text = "\(arrBeforeFilter.count) Videos"
        
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableMentees.reloadData()
        }
        }else       {
            messageLabel.isHidden = false
            DispatchQueue.main.async {
                self.tableMentees.reloadData()
            }
        }
        
    }
}
