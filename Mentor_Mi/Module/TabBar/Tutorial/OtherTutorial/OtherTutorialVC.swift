//
//  OtherTutorialVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage


class OtherTutorialVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableMentors: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblVideosCount: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var advancedButton: UIButton!
    
    @IBOutlet weak var beginerButton: UIButton!
    @IBOutlet weak var intermediateButton: UIButton!
    
    
    var dictTutorial = JSON()
    var isOn = true
    var arrMentorsTute = [JSON]()
    var arrBeforeFilter = [JSON]()
    var isFilterEnabled = false
    let notificationKey = "callCreateClass"
    var parentNavigationController = UINavigationController()
    var isLists = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        

        isFilterEnabled = false
       getOtherTutorialData(id: UserDefault.standard.getCurrentUserId()!)
        
        
    }
    override func viewDidLayoutSubviews() {
         tableMentors.heightAnchor.constraint(equalToConstant:
           tableMentors.contentSize.height + 50).isActive = true
    }
    
    func getOtherTutorialData(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.otherTutorial(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleOtherTutorialResponse(response: response)
        }
    }
    
    func handleOtherTutorialResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            
            if tempResponse["message"].stringValue == "No Post Found !!"{
                arrMentorsTute.removeAll()
                self.tableMentors.reloadData()
            }
            
            if tempResponse["message"].stringValue == "No Post Found !!"{
            if isFilterEnabled {
            
                arrBeforeFilter.removeAll()
                self.tableMentors.reloadData()
            
            }
             else{
                arrMentorsTute.removeAll()
                self.tableMentors.reloadData()
            }
        }
            
            let alert = UIAlertController(title: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: {UIAction in
                self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            
                    advancedButton.isSelected = false
                    beginerButton.isSelected = false
                    intermediateButton.isSelected = false
            arrMentorsTute = tempResponse["data"].arrayValue
//            arrBeforeFilter = tempResponse["data"].arrayValue
            self.lblVideosCount.text = "\(arrMentorsTute.count) Videos"
            
            self.tableMentors.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFilterEnabled ? arrBeforeFilter.count : arrMentorsTute.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLists {
            let CellIdentifier = "cell"
            var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? TutorialCells
            var customCell = [Any]()
            if cell == nil {
                customCell = Bundle.main.loadNibNamed("TutorialCells", owner: self, options: nil) ?? [Any]()
            }
            cell?.selectionStyle = .default
            cell = customCell[1] as? TutorialCells
            
            cell?.lblTitle.text = arrMentorsTute[indexPath.row]["post_title"].stringValue
            cell?.lblContent.text = arrMentorsTute[indexPath.row]["posted"].stringValue
            
            return cell!
        }
        else {
            let CellIdentifier = "cell"
            var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? TutorialCells
            var customCell = [Any]()
            if cell == nil {
                customCell = Bundle.main.loadNibNamed("TutorialCells", owner: self, options: nil) ?? [Any]()
            }
        
            messageLabel.isHidden = true
            cell?.selectionStyle = .default
            cell = customCell[0] as? TutorialCells
            
            cell?.deletePost.tag = indexPath.row
            cell?.deletePost.addTarget(self, action: #selector(buttonDeletePodt(sender:)), for: .touchUpInside)
            
            if isFilterEnabled {
                
                let strURL = arrBeforeFilter[indexPath.row]["video_thumbnail"].stringValue
                cell?.imgTutorial.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
                cell?.imgTutorial.layer.cornerRadius = 2.5
                cell?.imgTutorial.clipsToBounds = true
                
                cell?.tutorialTitle.text = arrBeforeFilter[indexPath.row]["post_title"].stringValue
                cell?.tutorialDuration.text = arrBeforeFilter[indexPath.row]["posted"].stringValue
                cell?.tutorialTag.text = arrBeforeFilter[indexPath.row]["post_tag"].stringValue
                //let post_id = String(format:"%@",arrBeforeFilter[indexPath.row]["post_id"].stringValue)
               // cell?.deletePost.tag = Int(post_id)!
                
            }else{
            
            let strURL = arrMentorsTute[indexPath.row]["video_thumbnail"].stringValue
            cell?.imgTutorial.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            cell?.imgTutorial.layer.cornerRadius = 2.5
            cell?.imgTutorial.clipsToBounds = true
            
            cell?.tutorialTitle.text = arrMentorsTute[indexPath.row]["post_title"].stringValue
            cell?.tutorialDuration.text = arrMentorsTute[indexPath.row]["posted"].stringValue
            cell?.tutorialTag.text = arrMentorsTute[indexPath.row]["post_tag"].stringValue
            //let post_id = String(format:"%@",arrMentorsTute[indexPath.row]["post_id"].stringValue)
               // cell?.deletePost.tag = Int(post_id)!
            }
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isLists {
            return 50
        }
        else {
            return 235
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        dictVideoData = isFilterEnabled ? arrBeforeFilter[indexPath.row] : arrMentorsTute[indexPath.row]
        NotificationCenter.default.post(name: Notification.Name("playFromTutorial"), object: nil)

        
    }
    
    @objc func buttonDeletePodt(sender: UIButton){
        var postID = String()
        var userID = String()
        if isFilterEnabled {
            postID = String(format:"%@",arrBeforeFilter[sender.tag]["post_id"].stringValue)
            userID = String(format:"%@",arrBeforeFilter[sender.tag]["user_id"].stringValue)
        }
        else{
           postID = String(format:"%@",arrMentorsTute[sender.tag]["post_id"].stringValue)
            userID = String(format:"%@",arrMentorsTute[sender.tag]["user_id"].stringValue)
        }
        
         let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.deletePost)"
                print("\(apiURL)")
               
                //let userID = UserDefault.standard.getCurrentUserId()!
                let newTodo: [String: Any] = ["user_id": userID, "post_id": postID]
                print(newTodo)
                
        //        SVProgressHUD.show(withStatus: "Loading...")
                Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                    if((response.result.value) != nil) {
        //                SVProgressHUD.dismiss()
                        let swiftyJsonVar = JSON(response.result.value!)
                        print(swiftyJsonVar)
                        if swiftyJsonVar["status"].string == "success" {
                            let responseJson = swiftyJsonVar["data"]
                            print(responseJson)
                            
                            let alertController = UIAlertController(title: StringConstants.Alert.Titles.Error, message: "Post Deleted Successfully", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                               self.getOtherTutorialData(id: UserDefault.standard.getCurrentUserId()!)
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        else {
                            
                        }
                        NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)
                        
                    }
                    else {
                        
        //                SVProgressHUD.dismiss()
                        
                    }
                }
        
    }
    
    @IBAction func actionAddClass(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("moveto"), object: nil)
    }
    

    @IBAction func advancedAction(_ sender: Any) {
        
advancedButton.isSelected = true
       beginerButton.isSelected = false
       intermediateButton.isSelected = false
        
        isFilterEnabled = true
        
        arrBeforeFilter = arrMentorsTute.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "1"
        })
        
        self.lblVideosCount.text = "\(arrBeforeFilter.count) Videos"
        
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableMentors.reloadData()
        }
        }else       {
            messageLabel.isHidden = false
            DispatchQueue.main.async {
                self.tableMentors.reloadData()
            }
        }
        
        
    }
    
    @IBAction func intermediateAction(_ sender: Any) {
        
advancedButton.isSelected = false
       beginerButton.isSelected = false
       intermediateButton.isSelected = true
        
        isFilterEnabled = true
        
        arrBeforeFilter = arrMentorsTute.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "2"
        })
        
        self.lblVideosCount.text = "\(arrBeforeFilter.count) Videos"
        
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableMentors.reloadData()
        }
        }else       {
            messageLabel.isHidden = false
            DispatchQueue.main.async {
                self.tableMentors.reloadData()
            }
        }
        
        
    }
    
    
    
    
    @IBAction func beginerAction(_ sender: Any) {
        
advancedButton.isSelected = false
       beginerButton.isSelected = true
       intermediateButton.isSelected = false
        
        isFilterEnabled = true
        
        arrBeforeFilter = arrMentorsTute.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "3"
        })
        
        self.lblVideosCount.text = "\(arrBeforeFilter.count) Videos"
        
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableMentors.reloadData()
        }
        }else       {
            messageLabel.isHidden = false
            DispatchQueue.main.async {
                self.tableMentors.reloadData()
            }
        }
        
        
    }
    
    
    
}
