//
//  SendTutorialCell.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class SendTutorialCell: UITableViewCell {
    
    var isCheck = true
    //    @IBOutlet weak var imgUncheck: UIImageView!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgDP: UIImageView!
    @IBOutlet weak var btnOK: UIButton!
    
    @IBOutlet weak var btnUncheck: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
