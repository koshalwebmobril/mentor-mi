//
//  TutorialCells.swift
//  Mentor Mi
//
//  Created by WebMobril on 29/10/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SwiftyJSON


class TutorialCells: UITableViewCell {
    @IBOutlet weak var imgTutorial: UIImageView!
    @IBOutlet weak var imgTute: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var tutorialTitle: UILabel!
    
    @IBOutlet weak var tutorialDuration: UILabel!
    
    @IBOutlet weak var tutorialTag: UILabel!
    
    @IBOutlet weak var deletePost: UIButton!
    
    var arrMentorsTute = NSArray()
    var arrBeforeFilter = NSArray()
    
    var arr_post = NSMutableArray()
    
    
    
    /*
    @IBAction func deletpost(_ sender: UIButton) {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.deletePost)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        
        let postID = UserDefaults.standard.object(forKey: "post_id") as! String //"\(sender.tag)"
        
        
        let newTodo: [String: Any] = ["user_id": userID, "post_id": postID]
        print(newTodo)
        
//        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
//                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    
                }
                else {
                    
                }
                NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)
                
            }
            else {
                
//                SVProgressHUD.dismiss()
                
            }
        }
    }
    
    
    
    @IBAction func deletePost(_ sender: Any) {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.deletePost)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let postID = UserDefaults.standard.object(forKey: "post_id") as! String
        let newTodo: [String: Any] = ["user_id": userID, "post_id": postID]
        print(newTodo)
        
//        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
//                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    
                }
                else {
                    
                }
                NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)
                
            }
            else {
                
//                SVProgressHUD.dismiss()
                
            }
        }
        
    }
    
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
