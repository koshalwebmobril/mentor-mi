//
//  SearchVC.swift
//  Mentor_Mi
//
//  Created by Manish on 01/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var searchBAR: UISearchBar!
    @IBOutlet weak var tbleSearchResult: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!
    var isOn = true
    var isShow = 1
    
    var arrSearch = [JSON]()
    var arrSearchActive = [JSON]()
    var isSearchActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setUI()
        
    }
    
    func setUI() {
        self.tabBarController?.title = "Search"
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.barTintColor = appDelegate.appConstant.navigationColor
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        isOn = true
    }
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {
        
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
        
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }

    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
           statusView.isHidden = true
        let dict = ["text": statusTxt.text]
        NotificationCenter.default.post(name: NSNotification.Name("uploadstatus"), object: nil, userInfo: dict as [AnyHashable : Any])
           statusTxt.text = ""
        }
       }
       
       @IBAction func cancel(_ sender: Any) {
           statusView.isHidden = true
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchActive.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbleSearchResult.dequeueReusableCell(withIdentifier: "cell") as! SearchTableCell
        
        cell.imgBanner.layer.cornerRadius = 2.5
        cell.imgBanner.clipsToBounds = true
        
//        cell.btnProfile.layer.cornerRadius = cell.btnProfile.frame.height/2
//        cell.btnProfile.layer.borderColor = UIColor.white.cgColor
//        cell.btnProfile.layer.borderWidth = 5.0
        
        let tempBannerURL = arrSearchActive[indexPath.row]["video_thumbnail"].stringValue
        cell.imgBanner.sd_setImage(with: URL(string: tempBannerURL), placeholderImage: UIImage(named: "trending3"))
        
        let tempProfile = arrSearchActive[indexPath.row]["profile_pic"].stringValue
        
//        cell.btnProfile.sd_setBackgroundImage(with: URL(string:tempProfile), for: .normal)
//
//        cell.btnProfile.tag = indexPath.row
//        cell.btnProfile.addTarget(self, action: #selector(self.profileBySearch(_:)), for: .touchUpInside)
        cell.profileImage.sd_setImage(with: URL(string: tempProfile), placeholderImage: UIImage(named: "trending3"))
        cell.profileImage.tag = indexPath.row
        cell.profileImage.isUserInteractionEnabled = true
        cell.profileImage.layer.masksToBounds = true
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
        cell.profileImage.layer.borderColor = UIColor.white.cgColor
                cell.profileImage.layer.borderWidth = 5.0
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(profileBySearch(_:)))
            
        cell.profileImage.addGestureRecognizer(gesture)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
    
    //    MARK:- SearchBar Delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if arrSearchActive.count > 0 {
            isSearchActive = true
        }
        else {
            isSearchActive = false
        }
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            
        }
        else {
            searchBar.showsCancelButton = true
            isSearchActive = true
            
            if arrSearchActive.count > 0 {
                
            }
            else {
                if searchText == "" {
                    
                }
            }
        }
        tbleSearchResult.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        searchBar.text = nil
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        arrSearchActive.removeAll()
        tbleSearchResult.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        searchAPICall(text : searchBar.text!)
    }
    
    func searchAPICall(text: String) {
        let parameters = ["sstr": text] as [String: Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.search(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleSearchResponse(response: response)
        }
    }
    
    func handleSearchResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.arrSearchActive = tempResponse["data"].arrayValue
            self.tbleSearchResult.reloadData()
        }
    }
    @objc func profileBySearch(_ sender: UITapGestureRecognizer) {
        
        let view = sender.view
        guard let tag = view?.tag else{return}
        let profileDetail = UIStoryboard.searchProfile
        profileDetail.dictData = arrSearchActive[tag]
        profileDetail.strTitle = "Profile"
        profileDetail.strId = String(format:"%@",arrSearchActive[tag]["user_id"].stringValue)
        self.navigationController?.pushViewController(profileDetail, animated: true)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
