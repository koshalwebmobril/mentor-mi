//
//  SearchTableCell.swift
//  Mentor_Mi
//
//  Created by Manish on 01/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class SearchTableCell: UITableViewCell {
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitlePost: UILabel!
    @IBOutlet weak var btnProfile: UIButton!

    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
