//
//  ProfileSearchCell.swift
//  Mentor_Mi
//
//  Created by Manish on 21/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class ProfileSearchCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblImages: UIImageView!
    
    @IBOutlet weak var bannerImgProfile: UIImageView!
    
    @IBOutlet weak var lblMenteesCount: UILabel!
    @IBOutlet weak var lblMentorCount: UILabel!
    @IBOutlet weak var lblTutorialCount: UILabel!
    @IBOutlet weak var lblTrophyCount: UILabel!
    @IBOutlet weak var imgTrophy: UIImageView!
    @IBOutlet weak var imgLikeDislike: UIImageView!
    @IBOutlet weak var imgTutorial: UIImageView!
    @IBOutlet weak var lblTutorialName: UILabel!
    
    @IBOutlet weak var lblbeginners: UILabel!
    @IBOutlet weak var lblIntermediate: UILabel!
    @IBOutlet weak var lblAdvanced: UILabel!
    
    @IBOutlet weak var btnPersonalChat: UIButton!
    @IBOutlet weak var btnCentre: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
