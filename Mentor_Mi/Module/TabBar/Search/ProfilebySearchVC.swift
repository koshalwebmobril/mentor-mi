//
//  ProfilebySearchVC.swift
//  Mentor_Mi
//
//  Created by Manish on 02/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage


class ProfilebySearchVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let appConstant:AppConstants = AppConstants()
    var strId = String()
    var dicUser = JSON()
    var strTitle = String()
    var dictData = JSON()
    var arrTutorial = NSArray()
    var strNotifyId = String()
    var dicNotifyData = NSDictionary()
    var isOn = true
    
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var threedotsinfo: UIView!
    @IBOutlet weak var lblHading: UILabel!
    @IBOutlet weak var tbleProfile: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        tbleProfile.delegate = self
        tbleProfile.dataSource = self
        
        setUI()
        getData(id: UserDefault.standard.getCurrentUserId()!, otherId: strId)
        getDetailsFromNotification(id: strNotifyId)
        print("DicData: \(dictData)")
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    func setUI() {
        lblHading.text = "Profile"
        strNotifyId = String(format: "%@", dictData["user_id"].stringValue)
    }
    
    func getData(id:Int, otherId: String) {
        let parameters = ["user_id" : id, "other_id": otherId] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
    }
    
    func handleUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {

        }
        else {
            self.dicUser = tempResponse["data"]
            print("profile: \(dicUser)")
            tbleProfile.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        isOn = true
        tabBarController?.tabBar.isHidden = false
        if strTitle == "Notify" {
            likeORnotLike()
        }
        setNavigationUI()
    }
    
    func likeORnotLike() {
        
    }
    
    func setNavigationUI() {
        self.tabBarController?.title = "Profile"
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        isOn = true

    
    }
    
    @objc func notification() {
           self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
       }
       
       @objc func threedots() {
           if isOn == true {
               threedotsinfo.isHidden = false
               threedotsinfo.layer.borderWidth = 2
               threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               isOn = false
           }
           else{
               threedotsinfo.isHidden = true
               isOn = true
           }
       }
    
    @IBAction func premium(_ sender: Any) {
           threedotsinfo.isHidden = true
           self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
       }
       
       @IBAction func settings(_ sender: Any) {
           threedotsinfo.isHidden = true
           self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
       }
       @IBAction func status(_ sender: Any) {
           threedotsinfo.isHidden = true
           statusView.isHidden = false
           statusView.layer.borderWidth = 2
           statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           isOn =  true
       }

       @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
              statusView.isHidden = true
           let dict = ["text": statusTxt.text]
           NotificationCenter.default.post(name: NSNotification.Name("uploadstatus"), object: nil, userInfo: dict as [AnyHashable : Any])
              statusTxt.text = ""
        }
          }
          
          @IBAction func cancel(_ sender: Any) {
              statusView.isHidden = true
          }
    
    func getDetailsFromNotification(id: String) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
    }
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tbleProfile.dequeueReusableCell(withIdentifier: "cell") as! ProfileSearchCell
            
            if dicUser.count > 0 {
                cell.lblName.text = String(format:"%@ %@",dicUser["first_name"].stringValue, dicUser["last_name"].stringValue)
                cell.lblPosition.text = String(format:"%@",dicUser["user_occupation"].stringValue)
                
                let strUrl = String(format:"%@",dicUser["profile_pic"].stringValue)
                cell.lblImages.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
                cell.lblImages.layer.cornerRadius = cell.lblImages.frame.height/2
                cell.lblImages.clipsToBounds = true
                
                
                cell.bannerImgProfile.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
               
                
                cell.lblMentorCount.text = String(format:"%@",dicUser["Total Mentors"].stringValue)
                cell.lblMenteesCount.text = String(format:"%@",dicUser["Total Mentees"].stringValue)
                cell.lblTutorialCount.text = String(format:"%@",dicUser["Total Tutorials"].stringValue)
                if dicUser["follow_flag"].stringValue == "0" {
                    cell.btnCentre.setImage(UIImage(named: "Unfollow"), for: .normal)
                }
                else {
                    cell.btnCentre.setImage(UIImage(named: "Follow"), for: .normal)
                }
                cell.btnCentre.addTarget(self, action: #selector(methodFollowUnfollow), for: .touchUpInside)
            }
           
            return cell
        }
        else {
            let cell = self.tbleProfile.dequeueReusableCell(withIdentifier: "cell1") as! ProfileSearchCell
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 300
        }
        else {
            return 220
        }
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            tbleProfile.reloadData()
        }
    }
    
    @objc func methodFollowUnfollow(_ sender: UIButton) {
        if strTitle == "Notify" {
            
        }
        else {
            let tempFlag = String(format:"%@",dictData["follow_flag"].stringValue)
            if tempFlag == "1" {
                unFollowMethod(myId: UserDefault.standard.getCurrentUserId()!, followId: String(format:"%@",dictData["user_id"].stringValue))
            }
            else {
                followMethod(myId: UserDefault.standard.getCurrentUserId()!, followId: String(format:"%@",dictData["user_id"].stringValue))
            }
        }
    }
    
    func unFollowMethod(myId: Int, followId: String) {
        let parameters = ["user_id" : myId, "follow_user_id" :followId] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.unFollowUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleFollowUnfollowResponse(response: response)
        }
    }
    
    func followMethod(myId: Int, followId: String) {
        let parameters = ["user_id" : myId, "follow_user_id" : followId] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.followUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleFollowUnfollowResponse(response: response)
        }
    }
    
    func handleFollowUnfollowResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            
        }
        else {
             getData(id: UserDefault.standard.getCurrentUserId()!, otherId: strId)
        }
    }
    
    @objc func chatOpen() {
        
        
        let tempFlag = String(format:"%@",dicUser["follow_flag"].stringValue)
        if tempFlag == "1" {
            
            let currentChatView = UIStoryboard.singleChat
            
            let name :String = dictData["first_name"].stringValue + " " + dictData["last_name"].stringValue
            let image : String = dictData["profile_pic"].stringValue
            let friendId : String = dictData["id"].stringValue

            let friend = ChatFriend(id: friendId , name: name, image: image)
            currentChatView.friend = friend
            currentChatView.toId = friendId
            
            self.navigationController?.pushViewController(currentChatView, animated: true)
           
        }
        else {
            
            let alertController = UIAlertController(title: "Mentor Mi", message: "Please follow this profile to start chatting!", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(okAction)
                
                
                self.present(alertController, animated: true, completion: nil)
            }
            
         
        
    }
    
    
    @IBAction func chatOpenButtonAction(_ sender: Any) {
        
//        let tempFlag = String(format:"%@",dictData["follow_flag"].stringValue)
//               if tempFlag == "1" {
                   
                   let currentChatView = UIStoryboard.singleChat
                   
                   let name :String = dictData["first_name"].stringValue + " " + dictData["last_name"].stringValue
                   let image : String = dictData["profile_pic"].stringValue
                   let friendId : String = dictData["user_id"].stringValue

                   let friend = ChatFriend(id: friendId , name: name, image: image)
                   currentChatView.friend = friend
                   currentChatView.toId = friendId
                   
                   self.navigationController?.pushViewController(currentChatView, animated: true)
                  
//               }
//               else {
//
//                   let alertController = UIAlertController(title: "Mentor Mi", message: "Please follow this profile to start chatting!", preferredStyle: .alert)
//
//                   let okAction = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//                       self.dismiss(animated: true, completion: nil)
//                   }
//
//                   alertController.addAction(okAction)
//
//
//                       self.present(alertController, animated: true, completion: nil)
//                   }
        
        
    }
    
    
    
    
    @IBAction func callButtonAction(_ sender: Any) {
        
        let number = dictData["phone"].stringValue
        
        if number.isEmpty{
            
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Contact number not found", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
        }else{
        let tempCall = String(format:"%@",number)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
        
        
        
    }
   
}
