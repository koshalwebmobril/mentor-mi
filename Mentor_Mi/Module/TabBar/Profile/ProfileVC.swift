//
//  ProfileVC.swift
//  Mentor_Mi
//
//  Created by Manish on 10/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let appConstant:AppConstants = AppConstants()
    var dataUserResponse = JSON()
    var dataTimeLine = JSON()
    var arrTimeLine = [JSON]()
    var todayThought = String()
    var stringAbout = String()
    var fromWhere = String()
    var userId = String()
    var isOn = true
    var isThought = false
    var strAboutMe = String()
    
    

    @IBOutlet weak var threedotsinfo: UIView!
    @IBOutlet weak var tbleProfiles: UITableView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var statusView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)
        
        view.addSubview(threedotsinfo)
        
        

        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
//        getProfileData()
//        getTimelineData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationUI()
        
        getData(id: UserDefault.standard.getCurrentUserId()!)
        getTimelineData()
        
//        isOn = true
        
//        threedotsinfo.isHidden = true
       
//        getMyTutorial(id: UserDefault.standard.getCurrentUserId()!)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        threedotsinfo.isHidden = true
    }
    
    func getProfileData() {
        
        print(myData.myDetails)

    }
    
    func getData(id:Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
    }
    
    func handleUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            myData.myDetails = tempResponse["data"]
            DispatchQueue.main.async {
                self.tbleProfiles.reloadData()
                   }
            
        }
    }
    
    func getMyTutorial(id:Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.postByUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTutorialDataResponse(response: response)
        }
    }
    
    func handleTutorialDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            dataTimeLine = tempResponse["data"]
            tbleProfiles.reloadData()
        }
    }
    
    
    func navigationUI() {
        
        self.tabBarController?.title = "Profile"
        tabBarController?.navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        
        threedotsinfo.isHidden = true
        isOn = true
    }
    
    
    @objc func notification() {
      
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    
    
    @objc func threedots() {
        
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    
    @IBAction func ok(_ sender: Any) {
    if statusTxt.text == ""{
    let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    self.present(alert, animated: true, completion: nil)
    }
    else{
              statusView.isHidden = true
           let dict = ["text": statusTxt.text]
           NotificationCenter.default.post(name: NSNotification.Name("uploadstatus"), object: nil, userInfo: dict as [AnyHashable : Any])
        uploadStatus(userId: UserDefault.standard.getCurrentUserId()!, status: statusTxt.text! )
              statusTxt.text = ""
       // self.getData(id: UserDefault.standard.getCurrentUserId()!)
        }
        
          }
          
          @IBAction func cancel(_ sender: Any) {
              statusView.isHidden = true
          }
    
    func getTimelineData() {
        
        var id = String()
        
        id = String(format: "%d", UserDefault.standard.getCurrentUserId()!)
//        if fromWhere == "" {
//            id = String(format: "%d", UserDefault.standard.getCurrentUserId()!)
//        }
//        else if fromWhere == "" {
//            id = userId
//        }
        getTimeLine(userId: id)
    }
    
    
    func getTimeLine(userId: String) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getTimeLine(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTimelineDataResponse(response: response)
        }
    }
    
    func handleTimelineDataResponse(response: [String : Any]) {
        UtilityMethods.hideIndicator()
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrTimeLine = tempResponse["data"].arrayValue
            print("TimeLine data: \(arrTimeLine.count)")
            tbleProfiles.reloadData()
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isThought == false {
            return arrTimeLine.count + 1
        }
        else {
            return arrTimeLine.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isThought == false {
            if section == 0 {
                return 1
            }
            else {
                let postArray = arrTimeLine[section-1]["posts"].arrayValue
                return postArray.count
            }
        }
        else {
            if section == 0 || section == 1 {
                return 1
            }
            else {
                let postArray = arrTimeLine[section-2]["posts"].arrayValue
                return postArray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isThought == false {
            if indexPath.section == 0 {
                let cell = tbleProfiles.dequeueReusableCell(withIdentifier: "cell") as! ProfileCell
                
                cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
                cell.imgProfile.layer.borderWidth = 5.0
                cell.imgProfile.layer.borderColor = UIColor.white.cgColor
                
                
                let strURL = String(format:"%@",myData.myDetails["profile_pic"].stringValue)
                cell.imgProfile.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
                
                cell.imgProfileBanner.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
                
                 cell.lblName.text = String(format:"%@ %@",myData.myDetails["first_name"].stringValue,myData.myDetails["last_name"].stringValue)
                 cell.lblLastName.text = String(format:"%@",myData.myDetails["last_name"].stringValue)
                
                cell.lblDesignation.text = String(format:"%@",myData.myDetails["user_occupation"].stringValue)
                cell.lblMentorsCount.text = String(format:"%@",myData.myDetails["Total Mentors"].stringValue)
                cell.lblManteesCiunt.text = String(format:"%@",myData.myDetails["Total Mentees"].stringValue)
                cell.lblTutorialCount.text = String(format:"%@",myData.myDetails["Total Tutorials"].stringValue)
                cell.lblAboutMe.text = String(format:"%@",myData.myDetails["my_status"].stringValue)
                
                let tap1 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.MentorFunction))
                cell.lblMentors.isUserInteractionEnabled = true
                cell.lblMentors.addGestureRecognizer(tap1)
                
                let tap2 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.MenteesFunction))
                cell.lblMentees.isUserInteractionEnabled = true
                cell.lblMentees.addGestureRecognizer(tap2)
                
                let tap3 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.TutorialFunction))
                cell.lblTutorial.isUserInteractionEnabled = true
                cell.lblTutorial.addGestureRecognizer(tap3)
                
                cell.btnTutorial .addTarget(self, action: #selector(ProfileVC.thoughtOftheDay), for: .touchUpInside)
                cell.btnMsgs.addTarget(self, action: #selector(ProfileVC.Chattings), for: .touchUpInside)
                
                return cell
            }
            else {
                
                
                let cell = tbleProfiles.dequeueReusableCell(withIdentifier: "timeLine") as! ProfileCell
               
                let postArray = arrTimeLine[indexPath.section-1]["posts"].arrayValue
                cell.timelineTitle.text = postArray[indexPath.row]["post_title"].stringValue
                
                if indexPath.row == postArray.count - 1 {
                    cell.pathLine.isHidden = true
                }else{
                    
                    cell.pathLine.isHidden = false
                }
                
                return cell
            }
        }
        else {
            if indexPath.section == 0 {
                let cell = tbleProfiles.dequeueReusableCell(withIdentifier: "cell") as! ProfileCell
                
                cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
                cell.imgProfile.layer.borderWidth = 5.0
                cell.imgProfile.layer.borderColor = UIColor.white.cgColor
                
                let strURL = String(format:"%@", myData.myDetails["profile_pic"].stringValue)
                cell.imgProfile.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
                
                cell.lblName.text = String(format:"%@" + " " + "%@",myData.myDetails["first_name"].stringValue, myData.myDetails["last_name"].stringValue)
                
                cell.lblDesignation.text = String(format:"%@", myData.myDetails["user_occupation"].stringValue)
                cell.lblAboutMe.text = myData.myDetails["my_status"].stringValue
                cell.lblMentorsCount.text = myData.myDetails["Total Mentors"].stringValue
                cell.lblManteesCiunt.text = myData.myDetails["Total Mentees"].stringValue
                cell.lblTutorialCount.text = myData.myDetails["Total Tutorials"].stringValue
                
                let tap1 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.MentorFunction))
                cell.lblMentors.isUserInteractionEnabled = true
                cell.lblMentors.addGestureRecognizer(tap1)
                
                let tap2 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.MenteesFunction))
                cell.lblMentees.isUserInteractionEnabled = true
                cell.lblMentees.addGestureRecognizer(tap2)
                
                let tap3 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.TutorialFunction))
                cell.lblTutorial.isUserInteractionEnabled = true
                cell.lblTutorial.addGestureRecognizer(tap3)
                
                cell.btnTrophy.addTarget(self, action: #selector(ProfileVC.tropphy), for: .touchUpInside)
                cell.btnTutorial .addTarget(self, action: #selector(self.thoughtOftheDay), for: .touchUpInside)
                
                return cell
            }
            else if indexPath.section == 1 {
                let cell = tbleProfiles.dequeueReusableCell(withIdentifier: "cell1A") as! ProfileCell
                
                //                cell.lbl
                return cell
            }
            else {
        
               let cell = tbleProfiles.dequeueReusableCell(withIdentifier: "timeLine") as! ProfileCell
                
                let postArray = arrTimeLine[indexPath.section-2]["posts"].arrayValue
                
                return cell
            }
        }
    }
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isThought == false {
            if indexPath.section == 0 {
                return 320
            }
            else {
//                return (arrTimeLine[indexPath.row]["posts"].count > 1) ?  CGFloat(arrTimeLine[indexPath.row]["posts"].count * 55) : 100.0
                let postArray = arrTimeLine[indexPath.section-1]["posts"].arrayValue
                if postArray.count>1{
                    return 55
                }else{
                    return 30
                }
            }
        }
        else {
            if indexPath.section == 0 {
                return 275
            }
            else if indexPath.section == 1 {
                return UITableView.automaticDimension
            }
            else {
//                return (arrTimeLine[indexPath.row]["posts"].count > 1) ?  CGFloat(arrTimeLine[indexPath.row]["posts"].count * 55) : 100.0\
                let postArray = arrTimeLine[indexPath.section-2]["posts"].arrayValue
                if postArray.count>1{
                    return 55
                }else{
                    return 30
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0{
            let controller = UIStoryboard.playerThumbnail
            controller.dictTutorial = arrTimeLine[indexPath.row]
            controller.fromWhere = "myProfile"
            controller.strURL =  arrTimeLine[indexPath.row]["post_vedio"].stringValue
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if isThought == false {
                   if section == 0 {
                    return CGFloat.leastNormalMagnitude
                   }else{
                    return 80
            }
        }else{

            if section == 0 || section == 1 {
                       return CGFloat.leastNormalMagnitude
                   }else{
                    return 80
            }

        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tbleProfiles.dequeueReusableCell(withIdentifier: "cell1") as! ProfileCell
        
        tableView.tableHeaderView?.backgroundColor = .white
        
        cell.backgroundColor = .white
        
        if isThought == false {
        
            if section > 0 {
        cell.imgTutorials.layer.cornerRadius = cell.imgTutorials.frame.height/2
        cell.imgTutorials.layer.borderWidth = 3.5
        cell.imgTutorials.layer.borderColor = UIColor.white.cgColor
        
        let strURL1 = String(format:"%@",myData.myDetails["profile_pic"].stringValue)
        cell.imgTutorials.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMMM dd,yyyy"
        
        let date: NSDate? = dateFormatterGet.date(from: arrTimeLine[section-1]["created_date"].stringValue) as NSDate?
        print(dateFormatterPrint.string(from: date! as Date))
        
        cell.lblDateTutorial.text = dateFormatterPrint.string(from: date! as Date)
                return cell
            }
        }else{
            if section > 1{
                cell.imgTutorials.layer.cornerRadius = cell.imgTutorials.frame.height/2
                cell.imgTutorials.layer.borderWidth = 3.5
                cell.imgTutorials.layer.borderColor = UIColor.white.cgColor
                
                let strURL1 = String(format:"%@",myData.myDetails["profile_pic"].stringValue)
                cell.imgTutorials.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMMM dd,yyyy"
                
                let date: NSDate? = dateFormatterGet.date(from: arrTimeLine[section-2]["created_date"].stringValue) as NSDate?
                print(dateFormatterPrint.string(from: date! as Date))
                
                cell.lblDateTutorial.text = dateFormatterPrint.string(from: date! as Date)
                
              return cell
            }
        }
        return nil
    }
    
    
    
    @objc func MentorFunction(sender:UITapGestureRecognizer) {
        let mentor = UIStoryboard.mentormenteesList
        mentor.strTitle = "Mentors"
        mentor.countMentorMentees = myData.myDetails["Total Mentors"].stringValue
        self.navigationController?.pushViewController(mentor, animated: true)
    }
    
    @objc func MenteesFunction(sender:UITapGestureRecognizer) {
        let mentor = UIStoryboard.mentormenteesList
        mentor.strTitle = "Mentees"
        mentor.countMentorMentees = myData.myDetails["Total Mentees"].stringValue
        self.navigationController?.pushViewController(mentor, animated: true)
    }
    
    @objc func TutorialFunction(sender:UITapGestureRecognizer) {
        self.navigationController?.pushViewController(UIStoryboard.tutorial, animated: true)
    }
   
    @objc func tropphy(sender: UIButton) {
       
    }
    
    @objc func thoughtOftheDay(sender: UIButton) {
        let alert = UIAlertController(title: String(format:"%@",myData.myDetails["thought_of_the_day"].stringValue), message: todayThought, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
            self.tbleProfiles.reloadData()
        }
    }
    
    @objc func Chattings(sender: UIButton) {
        let controller = UIStoryboard.singleChatList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func actionEdit(_ sender: UIButton) {
        self.navigationController?.pushViewController(UIStoryboard.editProfile, animated: true)
    }
    
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    
   
    
    func uploadStatus() {
        
       
//        uploadStatus(userId: UserDefault.standard.getCurrentUserId()!, status: "\(text)")
    }
    
    func uploadStatus(userId: Int, status: String) {
        let parameters = ["user_id": userId, "status": status] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        APIManager.sharedInstance.uploadStatus(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleStatusResponse(response: response)
        }
    }
    
    func handleStatusResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
          
            getData(id: UserDefault.standard.getCurrentUserId()!)
            
        }
    }
    
    
    @IBAction func btnCallAction(_ sender: Any) {
        
        let number = myData.myDetails["phone"].stringValue
        
        if number.isEmpty{
            
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Contact number not found", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
        }else{
        let tempCall = String(format:"%@",number)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
        }
    
        
    

}


extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
