//
//  OtherUserProfileDetailVC.swift
//  Mentor_Mi
//
//  Created by Manish on 15/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON

class OtherUserProfileDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var tableDetailsMentorMentee: UITableView!
    @IBOutlet weak var lblTotalTutorial: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!

    
    @IBOutlet weak var messageLabel: UILabel!
    
    var isOn = true
    var strTitle = String()
    var IDprofile = String()
    var countBeginner = String()
    var countIntermediate = String()
    var countAdvanced = String()
    var countFlag = String()
    var strName = String()
    var strDesignation = String()
    var strTrophyCount = String()
    var strTotalTutorial = String()
    var strAdvanced = String()
    var strIntermediate = String()
    var strBeginner = String()
    var arrTutorial = [JSON]()
    var arrBeforeFilter = [JSON]()
    var dictDetails = JSON()
    var likeORdislike = String()
    var fromWhere = String()

    var isFilterEnabled :Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        strTitle = fromWhere
        print(fromWhere)
        print(strTitle)
        getData()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        isFilterEnabled = false
        
        setNavigationUI()
    }
    
    @objc func advancedAction(_ sender : UITapGestureRecognizer){
        
//        let view = sender.view as! UILabel
//        view.textColor = .appDefaultColor
        
        isFilterEnabled = true
        
        arrBeforeFilter = arrTutorial.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "1"
        })
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableDetailsMentorMentee.reloadData()
            
        }
        }else{
            messageLabel.isHidden = false
            self.tableDetailsMentorMentee.reloadData()
        }
        
    }
    
    @objc func intermediateAction(_ sender : UITapGestureRecognizer){
        
        isFilterEnabled = true
//        let view = sender.view as! UILabel
//        view.textColor = .appDefaultColor
        
        arrBeforeFilter = arrTutorial.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "2"
        })
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableDetailsMentorMentee.reloadData()
            
        }
        }else{
            messageLabel.isHidden = false
            self.tableDetailsMentorMentee.reloadData()
        }
        
    }
    
    @IBAction func ok(_ sender: Any) {
         if statusTxt.text == ""{
         let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
         self.present(alert, animated: true, completion: nil)
         }
         else{
                 statusView.isHidden = true
              let dict = ["text": statusTxt.text]
              NotificationCenter.default.post(name: NSNotification.Name("uploadstatus"), object: nil, userInfo: dict as [AnyHashable : Any])
                 statusTxt.text = ""
        }
             }
             
             @IBAction func cancel(_ sender: Any) {
                 statusView.isHidden = true
             }
    
    @objc func beginerAction(_ sender : UITapGestureRecognizer){
        
        isFilterEnabled = true
        
//        let view = sender.view as! UILabel
//        view.textColor = .appDefaultColor
        
        arrBeforeFilter = arrTutorial.filter({ (JSON) -> Bool in
            JSON["difficulty_level"].stringValue == "3"
        })
        
        if arrBeforeFilter.count>0{
        DispatchQueue.main.async {
            self.tableDetailsMentorMentee.reloadData()
            
        }
        }else{
            messageLabel.isHidden = false
            self.tableDetailsMentorMentee.reloadData()
        }
        
    }
    
    
    func getData() {
        if fromWhere == "Mentors" {
            IDprofile = String(format:"%@",dictDetails["m_id"].stringValue)
            likeORdislike = String(format:"%@",dictDetails["like_flag"].stringValue)
        }
        
        if fromWhere == "Mentees" {
            IDprofile = String(format:"%@",dictDetails["m_id"].stringValue)
            likeORdislike = String(format:"%@",dictDetails["like_flag"].stringValue)
        }
        mentorsDetails(id: IDprofile)
        detailsByUserIdAPI(id: IDprofile)
    }
    
    func mentorsDetails(id: String) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleProfileDetailsResponse(response: response)
        }
    }
    
    
    
    func detailsByUserIdAPI(id: String) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.postByUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTutorialDataResponse(response: response)
        }
    }
    
    func handleProfileDetailsResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.dictDetails = tempResponse["data"]
            self.strName = tempResponse["data"]["first_name"].stringValue + " " + tempResponse["data"]["last_name"].stringValue
            self.strDesignation = tempResponse["data"]["user_occupation"].stringValue
            self.strTrophyCount = tempResponse["data"]["trophy_count"].stringValue
            self.strTotalTutorial = tempResponse["data"]["Total Tutorials"].stringValue
            tableDetailsMentorMentee.reloadData()

        }
    }
    
    func handleTutorialDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrTutorial = tempResponse["data"].arrayValue
            tableDetailsMentorMentee.reloadData()
        }
    }
    
    func setNavigationUI() {
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        tabBarController?.tabBar.isHidden = false
    tabBarController?.navigationController?.isNavigationBarHidden = false
        navigationController?.isNavigationBarHidden = true
        
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {
        
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            if fromWhere == "Mentees" {
                return 0
            }
            else {
                return isFilterEnabled ? arrBeforeFilter.count : arrTutorial.count
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = tableDetailsMentorMentee.dequeueReusableCell(withIdentifier: "cell1") as! ManterMenteeDetailCell
            
            cell.lblImages.layer.cornerRadius = cell.lblImages.frame.height/2
            cell.lblImages.layer.borderWidth = 1.5
            cell.lblImages.layer.borderColor = UIColor.white.cgColor
            cell.lblImages.clipsToBounds = true
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(OtherUserProfileDetailVC.profileDetail))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(OtherUserProfileDetailVC.profileDetail))
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(OtherUserProfileDetailVC.profileDetail))
            cell.lblName.isUserInteractionEnabled = true
            cell.lblName.addGestureRecognizer(tap1)
            cell.lblPosition.isUserInteractionEnabled = true
            cell.lblPosition.addGestureRecognizer(tap2)
            cell.lblImages.isUserInteractionEnabled = true
            cell.lblImages.addGestureRecognizer(tap3)
            
            cell.dollarButton.isHidden=true
            cell.lblName.text = strName
            cell.lblPosition.text = strDesignation
            //cell.lblTrophyCount.text = strTrophyCount
            cell.lblTutorialCount.text = "\(strTotalTutorial) Tutorials"
            
//            cell.lblAdvanced.frame=CGRect(x: 0, y: 20, width: view.frame.width/3, height: 30)
//            cell.lblIntermediate.frame=CGRect(x: cell.lblAdvanced.frame.origin.x+cell.lblAdvanced.frame.width, y: 20, width: view.frame.width/3, height: 30)
//            cell.lblbeginners.frame=CGRect(x: cell.lblIntermediate.frame.origin.x+cell.lblIntermediate.frame.width, y: 20, width: view.frame.width/3, height: 30)
            
            cell.lblAdvanced.text = String(format:"%@",dictDetails["post_count_advanced"].stringValue)
            cell.lblIntermediate.text = String(format:"%@",dictDetails["post_count_beginner"].stringValue)
            cell.lblbeginners.text = String(format:"%@",dictDetails["post_count_intermediate"].stringValue)
            
            let tap4 = UITapGestureRecognizer(target: self, action: #selector(OtherUserProfileDetailVC.editLikeDisLike))
            cell.imgLikeDislike.isUserInteractionEnabled = true
            cell.imgLikeDislike.addGestureRecognizer(tap4)
            
            let temp = String(format:"%@",likeORdislike)
            if temp == "1" {
                cell.imgLikeDislike.image = UIImage(named: "heartgreen")
            }
            else {
                cell.imgLikeDislike.image = UIImage(named: "heartGreenBordered")
            }
            
            let strURL1 = String(format:"%@",dictDetails["profile_pic"].stringValue)
            cell.lblImages.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
            
            let advanceTap = UITapGestureRecognizer(target: self, action: #selector(advancedAction(_:)))
            let intermediateTap = UITapGestureRecognizer(target: self, action: #selector(intermediateAction(_:)))
            let beginerTap = UITapGestureRecognizer(target: self, action: #selector(beginerAction(_:)))
           
            cell.advancedLabel.isUserInteractionEnabled = true
            cell.advancedLabel.addGestureRecognizer(advanceTap)
            
            cell.intermediateLabel.isUserInteractionEnabled = true
            cell.intermediateLabel.addGestureRecognizer(intermediateTap)
           
            
            
            cell.beginerLabel.isUserInteractionEnabled = true
            cell.beginerLabel.addGestureRecognizer(beginerTap)
            
            return cell
        }
        else {
            
            let cell = tableDetailsMentorMentee.dequeueReusableCell(withIdentifier: "cell2") as! ManterMenteeDetailCell
            
            messageLabel.isHidden = true
            
            if fromWhere == "Mentees" {
                cell.imgTutorial.image = UIImage(named: "trending3")
                cell.lblTutorialName.text = ""
            }
            else {
                
                let url =  isFilterEnabled ? arrBeforeFilter[indexPath.row]["video_thumbnail"].stringValue : arrTutorial[indexPath.row]["video_thumbnail"].stringValue
                let strURL1 = String(format:"%@",url )
                cell.imgTutorial.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
                
                let title =  isFilterEnabled ? arrBeforeFilter[indexPath.row]["post_title"].stringValue : arrTutorial[indexPath.row]["post_title"].stringValue
                cell.lblTutorialName.text = String(format:"%@",title)
            }
            
            return cell
        }
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 265
        }
        else {
            return 220
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            tableDetailsMentorMentee.reloadData()
        }
        else {
            print(arrTutorial[indexPath.row])
            let play = UIStoryboard.playerThumbnail
            play.dictTutorial = isFilterEnabled ? arrBeforeFilter[indexPath.row] : arrTutorial[indexPath.row]
            play.fromWhere = "Profile"
            play.strURL = isFilterEnabled ? arrBeforeFilter[indexPath.row]["post_vedio"].stringValue : arrTutorial[indexPath.row]["post_vedio"].stringValue
            self.navigationController?.pushViewController(play, animated: true)
        }
    }
    
    
    @objc func editLikeDisLike ( sender : UITapGestureRecognizer) {
        if likeORdislike == "1" {
            unlikeUser(userId: UserDefault.standard.getCurrentUserId()!, profileId: IDprofile)
        }
        else if likeORdislike == "2" {
            likeUser(userId: UserDefault.standard.getCurrentUserId()!, profileId: IDprofile)
        }
        else {
            
        }
    }
    
    @objc func profileDetail (sender : UITapGestureRecognizer) {
        
        let profileDetail = UIStoryboard.otherProfile
        profileDetail.dictData = dictDetails
        profileDetail.arrTutorial = arrTutorial
        profileDetail.fromWhere = "Details"
        profileDetail.strTitle = fromWhere
        self.navigationController?.pushViewController(profileDetail, animated: true)
    }
    
    func unlikeUser(userId: Int, profileId: String) {
        let parameters = ["user_id" : userId, "profile_id": profileId ] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.unLikeProfile(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleResponse(response: response)
        }
    }
    
    func likeUser(userId: Int, profileId: String) {
        let parameters = ["user_id" : userId, "profile_id": profileId ] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.likeProfile(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleResponse(response: response)
        }
    }
    
    func handleResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.likeORdislike = tempResponse["data"]["like_flag"].stringValue
            self.tableDetailsMentorMentee.reloadData()
        }
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
    self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
    self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    

    @IBAction func callButtonAction(_ sender: Any) {
        
        let number = dictDetails["phone"].stringValue
            
            if number.isEmpty{
                
                 UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Contact number not found", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
                
            }else{
            let tempCall = String(format:"%@",number)
                if let url = URL(string: "tel://\(tempCall)"),
                    UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler:nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                } else {
                    
                }
            }
        
        
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
        
//        let tempFlag = String(format:"%@",dictDetails["follow_flag"].stringValue)
//        if tempFlag == "1" {
            
            let currentChatView = UIStoryboard.singleChat
            
            let name :String = dictDetails["first_name"].stringValue + " " + dictDetails["last_name"].stringValue
            let image : String = dictDetails["profile_pic"].stringValue
            let friendId : String = dictDetails["id"].stringValue

            let friend = ChatFriend(id: friendId , name: name, image: image)
            currentChatView.friend = friend
            currentChatView.toId = friendId
            
            self.navigationController?.pushViewController(currentChatView, animated: true)
           
//        }
//        else {
//            
//            let alertController = UIAlertController(title: "Mentor Mi", message: "Please follow this profile to start chatting!", preferredStyle: .alert)
//            
//            let okAction = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//                self.dismiss(animated: true, completion: nil)
//            }
//            
//            alertController.addAction(okAction)
//                
//                
//                self.present(alertController, animated: true, completion: nil)
//            }
    }
    
    
    
}
