//
//  OtherUserProfileVC.swift
//  Mentor_Mi
//
//  Created by Manish on 01/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON



class OtherUserProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var fromWhere = String()
    var userId = String()
    var isOn = true
    var strTitle = String()
    var followerId = String()
    var arrTutorial = [JSON]()
    var dictData = JSON()
    var followerCount = String()
    var dictFromData = JSON()
    
   
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var tbleProfile: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//         self.title = strTitle
        tbleProfile.dataSource = self
        tbleProfile.delegate = self

        getOtherUserData()
        
       if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
        setNavigationUI()
    }
    
   
    
    func setNavigationUI() {
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        tabBarController?.tabBar.isHidden = true
        tabBarController?.navigationController?.isNavigationBarHidden = true
        navigationController?.isNavigationBarHidden = true
//        lblHeading.text = strTitle
        
        lblHeading.text = "Profile"
        
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {

        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    func getOtherUserData() {
        if fromWhere == "Mentors" {
            followerId = ""
            let myId = dictFromData["m_id"].stringValue
            getOtherUserData(id: myId, otherId: followerId)
        }
        else  if fromWhere == "Mentees" {
            followerId = ""
            let myId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)
        }
        else if fromWhere == "Notify" {
            print(dictFromData)
//            followerId = dictData["id"].stringValue
            let myId = String(format:"%@",dictFromData["from_user_id"].stringValue)
            
            getOtherUserData(id: myId, otherId: followerId)
        }
        else if fromWhere == "Details" {
            print(dictData)
            followerId = String(format:"%@",dictData["id"].stringValue)
            let myId = String(format:"%d", UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)
        }
        else if fromWhere == "soapBox" {
            followerId = userId
            let myId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)
        }
        else if fromWhere == "MentorFeed" {
            followerId = userId
            let myId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)

        }else if fromWhere == "otherTutorial" {
            followerId = userId
            let myId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)

        }else if fromWhere == "myTutorial" {
            followerId = userId
            let myId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)

        }
        else {
            followerId = ""
            let myId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
            getOtherUserData(id: myId, otherId: followerId)
        }
    }
    
    func getOtherUserData(id: String, otherId: String) {
        let parameters = ["user_id" : id, "other_id": otherId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleOtherUserDataResponse(response: response)
        }
    }
    
    func handleOtherUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            self.navigationController?.popViewController(animated: false)
        }
        else {
            dictData = tempResponse["data"]
            print(dictData)
            tbleProfile.reloadData()
        }
    }
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        else {
            if strTitle == "Mentees" {
                return 0
            }
            else if strTitle == "Mentors" {
                return arrTutorial.count
            }
            else if strTitle == "Profile" {
                return 0
            }
            else {
                return arrTutorial.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            let cell = self.tbleProfile.dequeueReusableCell(withIdentifier: "cell") as! OtherUserProfileCell
            
            cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
            cell.imgProfile.layer.borderWidth = 5.0
            cell.imgProfile.layer.borderColor = UIColor.white.cgColor
            
            if dictData.count > 0 {
                let strURL1 = String(format:"%@",dictData["profile_pic"].stringValue)
                cell.imgProfile.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
                cell.imgBanner.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
                cell.lblName.text = String(format:"%@ %@",dictData["first_name"].stringValue,dictData["last_name"].stringValue)
                if strTitle == "Profile" {
                    cell.lblDesignation.text = ""
                    cell.lblMentorsCount.text = ""
                    cell.lblManteesCiunt.text = ""
                    cell.lblTutorialCount.text = ""
                }
                else if strTitle == "Notify" {
                    cell.lblDesignation.text = String(format:"%@",dictData["user_occupation"].stringValue)
                    cell.lblMentorsCount.text = String(format:"%@",dictData["Total Mentors"].stringValue)
                    cell.lblManteesCiunt.text = String(format:"%@",dictData["Total Mentees"].stringValue)
                    cell.lblTutorialCount.text = String(format:"%@",dictData["Total Tutorials"].stringValue)
                }
                else {
                    cell.lblDesignation.text = String(format:"%@",dictData["user_occupation"].stringValue)
                    cell.lblMentorsCount.text = String(format:"%@",dictData["Total Mentors"].stringValue)
                    cell.lblManteesCiunt.text = String(format:"%@",dictData["Total Mentees"].stringValue)
                    cell.lblTutorialCount.text = String(format:"%@",dictData["Total Tutorials"].stringValue)
                }
                
                if dictData["follow_flag"].stringValue == "1" {
                    cell.btnTutorial.setImage(UIImage(named: "Follow"), for: .normal)
                }
                else {
                    cell.btnTutorial.setImage(UIImage(named: "Unfollow"), for: .normal)

                }
                
                cell.btnTutorial.addTarget(self, action: #selector(methodFollowUnfollow), for: .touchUpInside)
                cell.btnMsgs.addTarget(self, action: #selector(chatOpen), for: .touchUpInside)
                
                cell.btncall.addTarget(self, action: #selector(calltoUser), for: .touchUpInside)
            }
            
            return cell
        }
        else {
            let cell = self.tbleProfile.dequeueReusableCell(withIdentifier: "cell1") as! OtherUserProfileCell
            
            if arrTutorial.count > 0 {
                
                if strTitle == "Mentees" || strTitle == "Profile" {
                    cell.imgTutorials.image = UIImage(named: "trending3")
                    cell.lblNameTute.text = ""
                }
                else {
                    let strURL1 = String(format:"%@",arrTutorial[indexPath.row]["video_thumbnail"].stringValue)
                    cell.imgTutorials.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "trending3"))
                    cell.lblNameTute.text = String(format:"%@",arrTutorial[indexPath.row]["post_title"].stringValue)
                }
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 315
        }
        else {
            return 220
        }
    }
    
    @IBAction func actionFollowUnfollow(_ sender: UIButton) {
        if fromWhere == "Notify" {
            
        }
        else {
            let tempFlag = String(format:"%@",dictData["follow_flag"].stringValue)
            if tempFlag == "1" {
                unFollowMethod()
                
            }
            else {
                followMethod()
            }
        }
    }
    
    @objc func methodFollowUnfollow(_ sender: UIButton) {
        if fromWhere == "Notify" {
            
        }
        else {
            let tempFlag = String(format:"%@",dictData["follow_flag"].stringValue)
            if tempFlag == "1" {
                unFollowMethod()
                
            }
            else {
                followMethod()
            }
        }
    }
    
    func unFollowMethod() {
        if fromWhere == "Mentors" {
            followerId = dictFromData["m_id"].stringValue
        }
        if fromWhere == "Mentees" {
            followerId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
        }
        if fromWhere == "Notify" {
            followerId = String(format:"%@",dictFromData["from_user_id"].stringValue)
        }
        if fromWhere == "Details" {
            followerId = String(format:"%@",dictData["id"].stringValue)
        }
       
         unFollower(id: UserDefault.standard.getCurrentUserId()!, followerId: followerId)
    }
    
    @objc func chatOpen() {
        
        
        let tempFlag = String(format:"%@",dictData["follow_flag"].stringValue)
        if tempFlag == "1" {
            
            let currentChatView = UIStoryboard.singleChat
            
            let name :String = dictData["first_name"].stringValue + " " + dictData["last_name"].stringValue
            let image : String = dictData["profile_pic"].stringValue
            let friendId : String = dictData["id"].stringValue

            let friend = ChatFriend(id: friendId , name: name, image: image)
            currentChatView.friend = friend
            currentChatView.toId = friendId
            
            self.navigationController?.pushViewController(currentChatView, animated: true)
           
        }
        else {
            
            let alertController = UIAlertController(title: "Mentor Mi", message: "Please follow this profile to start chatting!", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(okAction)
                
                
                self.present(alertController, animated: true, completion: nil)
            }
            
         
        
    }
    
    func followMethod() {
        if fromWhere == "Mentors" {
            followerId = dictFromData["m_id"].stringValue
        }
        if fromWhere == "Mentees" {
            followerId = String(format:"%d",UserDefault.standard.getCurrentUserId()!)
        }
        if fromWhere == "Notify" {
            followerId = String(format:"%@",dictFromData["from_user_id"].stringValue)
        }
        if fromWhere == "Details" {
            followerId = String(format:"%@",dictData["id"].stringValue)
        }
        
        follower(id: UserDefault.standard.getCurrentUserId()!, followerId: followerId)
    }
    
    func unFollower(id: Int, followerId: String) {
        let parameters = ["user_id" : id, "follow_user_id": followerId] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.unFollowUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleFollowerResponse(response: response)
        }
    }
    
    func follower(id: Int, followerId: String) {
        let parameters = ["user_id" : id, "follow_user_id": followerId] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.followUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleFollowerResponse(response: response)
        }
    }
    
    func handleFollowerResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
//            self.navigationController?.popViewController(animated: false)
            self.getOtherUserData()
        }
    }

    
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.present(UIStoryboard.premium, animated: true, completion: nil)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.present(UIStoryboard.setting, animated: true, completion: nil)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    
    @objc func calltoUser(_ sender: Any) {
        
        let number = dictData["phone"].stringValue
        
        if number.isEmpty{
            
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Contact number not found", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
        }else{
        let tempCall = String(format:"%@",number)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
        
        
        
    }
    
    
    
    
    
    
}

