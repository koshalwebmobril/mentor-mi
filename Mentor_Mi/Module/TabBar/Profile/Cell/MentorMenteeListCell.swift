//
//  MentorMenteeListCell.swift
//  Mentor_Mi
//
//  Created by Manish on 15/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class MentorMenteeListCell: UITableViewCell {
    
    @IBOutlet weak var imgDP: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var imgLikeDislike: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
