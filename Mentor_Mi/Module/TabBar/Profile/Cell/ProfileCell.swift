//
//  ProfileCell.swift
//  Mentor Mi
//
//  Created by WebMobril on 29/10/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblAboutMe: UILabel!
    @IBOutlet weak var lblMentorsCount: UILabel!
    @IBOutlet weak var lblManteesCiunt: UILabel!
    @IBOutlet weak var lblTutorialCount: UILabel!
    @IBOutlet weak var imgTutorials: UIImageView!
    @IBOutlet weak var lblDateTutorial: UILabel!
    @IBOutlet weak var btnTutorial: UIButton!
    @IBOutlet weak var btnMsgs: UIButton!
    @IBOutlet weak var btncall: UIButton!
    
    @IBOutlet weak var pathLine: UIView!
    @IBOutlet weak var timeLineBtn: UIButton!
    @IBOutlet weak var lblMentors: UILabel!
    @IBOutlet weak var lblMentees: UILabel!
    @IBOutlet weak var lblTutorial: UILabel!
    
    @IBOutlet weak var timelineTitle: UILabel!
    @IBOutlet weak var btnMoney: UIButton!
    @IBOutlet weak var btnTrophy: UIButton!
    @IBOutlet weak var dotTutorial: UIView!
    @IBOutlet weak var lblNameTute: UILabel!
    @IBOutlet weak var lblTiming: UILabel!
    
    @IBOutlet weak var lblPostTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var imgProfileBanner: UIImageView!
    
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var imgBanner: UIImageView!
    
    var timeLineView = UIView()
    
    @IBAction func btnTutorial(_ sender: Any) {
    }
    @IBAction func btnCall(_ sender: Any) {
        if let url = URL(string: "tel:55698") {
            UIApplication.shared.openURL(url)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
