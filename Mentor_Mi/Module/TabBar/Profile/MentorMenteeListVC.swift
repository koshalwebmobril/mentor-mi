//
//  MentorMenteeListVC.swift
//  Mentor_Mi
//
//  Created by Manish on 13/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class MentorMenteeListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var isOn = true
    var strTitle = String()
    var countMentorMentees = String()
    var count = Int()
    var isSearch = false
    var arrLists = [JSON]()
    var arrSearchActive = [JSON]()

    @IBOutlet weak var tableMentorMenteesLists: UITableView!
    @IBOutlet weak var lblheading: UILabel!
    @IBOutlet weak var searchLists: UISearchBar!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        getData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setNavigationUI()
        
    }
    
    func getData() {
        count = Int(countMentorMentees)!
        
        if strTitle == "Mentors" {
            MentorsListAPI(id: UserDefault.standard.getCurrentUserId()!)
        }
        if strTitle == "Mentees" {
            MenteesListAPI(id: UserDefault.standard.getCurrentUserId()!)
        }
    }
    
    func MentorsListAPI(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.mentorLists(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleListsDataResponse(response: response)
        }
    }
    
    func MenteesListAPI(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.menteesLists(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleListsDataResponse(response: response)
        }
    }
    
    func handleListsDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.arrLists = tempResponse["data"].arrayValue
            tableMentorMenteesLists.reloadData()
        }
    }
    
    func setNavigationUI() {
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        tabBarController?.tabBar.isHidden = false
        tabBarController?.title = strTitle
//        self.tabBarController?.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func notification() {
        
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch == false {
            if strTitle == "Mentors" {
                return arrLists.count
            }
            else {
                return arrLists.count
            }
        }
        else {
            if strTitle == "Mentors" {
                return arrSearchActive.count
            }
            else {
                return arrSearchActive.count
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableMentorMenteesLists.dequeueReusableCell(withIdentifier: "cell") as! MentorMenteeListCell
        
        if isSearch == false {
            if strTitle == "Mentees" {
                cell.imgLikeDislike.isHidden = true
            }
            else {
                let tempLike = arrLists[indexPath.row]["like_flag"].stringValue
                
                if tempLike == "1" {
                    cell.imgLikeDislike.image = UIImage(named: "heartgreen")
                }
                else {
                    cell.imgLikeDislike.image = UIImage(named: "heartGreenBordered")
                }
                
                cell.imgLikeDislike.isHidden = false
            }
            
            cell.lblName.text = String(format:"%@ %@", arrLists[indexPath.row]["first_name"].stringValue, arrLists[indexPath.row]["last_name"].stringValue)
            
            cell.lblPosition.text = arrLists[indexPath.row]["occupation"].stringValue
            
            let strURL = String(format:"%@", arrLists[indexPath.row]["profile_pic"].stringValue)
            cell.imgDP.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            
            cell.imgDP.layer.cornerRadius = cell.imgDP.frame.height/2
            cell.imgDP.layer.borderColor = UIColor.white.cgColor
            cell.imgDP.layer.borderWidth = 1.5
            cell.imgDP.clipsToBounds = true
        }
        else {
            if strTitle == "Mentees" {
                cell.imgLikeDislike.isHidden = true
            }
            else {
                let tempLike = arrSearchActive[indexPath.row]["like_flag"].stringValue
                
                if tempLike == "1" {
                    cell.imgLikeDislike.image = UIImage(named: "heartgreen")
                }
                else {
                    cell.imgLikeDislike.image = UIImage(named: "heartGreenBordered")
                }
                cell.imgLikeDislike.isHidden = false
            }
            
            cell.lblName.text = String(format:"%@ %@", arrSearchActive[indexPath.row]["first_name"].stringValue, arrSearchActive[indexPath.row]["last_name"].stringValue)
            
            cell.lblPosition.text = arrSearchActive[indexPath.row]["occupation"].stringValue
            
            let strURL = String(format:"%@", arrSearchActive[indexPath.row]["profile_pic"].stringValue)
            cell.imgDP.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            
            cell.imgDP.layer.cornerRadius = cell.imgDP.frame.height/2
            cell.imgDP.layer.borderColor = UIColor.white.cgColor
            cell.imgDP.layer.borderWidth = 1.5
            cell.imgDP.clipsToBounds = true
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearch == false {
            let controller = UIStoryboard.otherProfileDetail
            controller.dictDetails = arrLists[indexPath.row]
            controller.fromWhere = strTitle
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else {
            let controller = UIStoryboard.otherProfileDetail
            controller.dictDetails = arrSearchActive[indexPath.row]
            controller.fromWhere = strTitle
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    //    MARK:- SearchBar Delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if arrSearchActive.count > 0 {
            isSearch = true
        }
        else {
            isSearch = false
        }
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            
        }
        else {
            searchBar.showsCancelButton = true
            isSearch = true
            arrSearchActive.removeAll()
            for index in 0..<arrLists.count {
                let dict = arrLists[index]
                
                let name = dict["first_name"]
                let r: NSRange = (name as! NSString).range(of: searchText, options: .caseInsensitive)
                if r.location != NSNotFound {
                    arrSearchActive.append(dict)
                }
            }
            if arrSearchActive.count > 0 {
                
            }
            else {
                if searchText == "" {
                    
                }
            }
        }
        tableMentorMenteesLists.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearch = false
        searchBar.text = nil
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        arrSearchActive.removeAll()
        tableMentorMenteesLists.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        isSearch = false
        tableMentorMenteesLists.reloadData()
    }
    
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
