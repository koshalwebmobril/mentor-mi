//
//  EditProfileVC.swift
//  Mentor_Mi
//
//  Created by Manish on 20/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SkyFloatingLabelTextField

class EditProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var imagePicker = UIImagePickerController()
    var dictProfile = NSDictionary()
    var dictData = JSON()
    var imgData = Data()
    var isOn = true
    var strPrivate = "1"
    
   
    @IBOutlet weak var lblFirstName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var lblEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var lblOccupation: SkyFloatingLabelTextField!
    @IBOutlet weak var lblQualification: SkyFloatingLabelTextField!
    @IBOutlet weak var lblExperience: SkyFloatingLabelTextField!
    @IBOutlet weak var lblSkills: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var subscriptionSwitch: UISwitch!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var btnEditPic: UIButton!
    @IBOutlet weak var threedotsinfo: UIView!    
    var strEmail = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        getDetailsAPI(userId: UserDefault.standard.getCurrentUserId()!)
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        if UserDefaults.standard.string(forKey: "Apple") != nil{
            if UserDefaults.standard.string(forKey: "Apple") == "AppleLogin"{
                //lblEmail.isUserInteractionEnabled = true
                strEmail = "AppleLogin"
            }
            else{
               strEmail = ""
            }
        }
        else{
            strEmail = ""
        }
        
        
        imagePicker.delegate = self
        
        imgPic.layer.cornerRadius = imgPic.frame.height/2
        imgPic.layer.borderColor = UIColor.white.cgColor
        imgPic.layer.borderWidth = 2.5
        btnEditPic.layer.cornerRadius = btnEditPic.frame.height/2
        btnEditPic.layer.borderColor = UIColor.lightGray.cgColor
        btnEditPic.layer.borderWidth = 1.0
        
        btnSave.layer.cornerRadius = 5.0
        btnSave.clipsToBounds = true
    }
    
    
    func  getDetailsAPI(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserProfile(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
    }
    
    func handleUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.dictData = tempResponse["data"]
            print(self.dictData)
            
            self.lblFirstName.text = String(format:"%@",(self.dictData["first_name"].stringValue))
            self.lblLastName.text = String(format:"%@",(self.dictData["last_name"].stringValue))
            self.lblEmail.text = String(format:"%@",(self.dictData["email"].stringValue))
            self.lblOccupation.text = String(format:"%@",(self.dictData["occupation"].stringValue))
            self.lblQualification.text = String(format:"%@",(self.dictData["qualification"].stringValue))
            self.lblExperience.text = String(format:"%@",(self.dictData["levelofexp"].stringValue))
            self.lblSkills.text = String(format:"%@",(self.dictData["skills"].stringValue))
            
            let strURL = String(format:"%@",(self.dictData["profile_pic"].stringValue))
            print(strURL)
            self.imgPic.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            
            let subscription = String(format:"%@",(self.dictData["profile_accessibility"].stringValue))
            
            if subscription == "1" {
                self.subscriptionSwitch.isOn = false
            }
            else {
                self.subscriptionSwitch.isOn = true
            }
        }
    }
    
    func getMyTutorial(id:Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.postByUser(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTutorialDataResponse(response: response)
        }
    }
    
    func handleTutorialDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setNavigationUI()
    }
    
    func setNavigationUI() {
        isOn = true
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        
        tabBarController?.tabBar.isHidden = false
        self.tabBarController?.title = "Edit Profile"
        navigationController?.isNavigationBarHidden = true
        tabBarController?.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func notification() {
       self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
        
    }
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
        
    }
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
              statusView.isHidden = true
           let dict = ["text": statusTxt.text]
           NotificationCenter.default.post(name: NSNotification.Name("uploadstatus"), object: nil, userInfo: dict as [AnyHashable : Any])
              statusTxt.text = ""
        }
          }
          
          @IBAction func cancel(_ sender: Any) {
              statusView.isHidden = true
          }
    
    
     @IBAction func actionSave(_ sender: Any) {
        if checkValidation() {
            updateProfile(userId: UserDefault.standard.getCurrentUserId()!, FName: lblFirstName.text!, LName: lblLastName.text!, email: lblEmail.text!, occupation: lblOccupation.text!, qualification: lblQualification.text!, experience: lblOccupation.text!, skill: lblSkills.text!, accessibility: strPrivate)
        }
    }
    
    
    func updateProfile(userId: Int, FName: String, LName: String, email: String, occupation: String, qualification: String, experience: String, skill: String, accessibility: String) {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.updateProfile)"
        print("\(apiURL)")
        
        let image = imgPic.image
        imgData = image!.pngData()!
        
        UtilityMethods.showIndicator()
        
        let newTodo: [String: Any] = ["user_id":"\(userId)","first_name":FName ,"last_name": LName,"email": email,"occupation": occupation,"qualification": qualification,"levelofexp":experience,"skills": skill,"profile_accessibility":accessibility]
        
        print("parameters are \(newTodo)")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.imgData, withName: "file_url",fileName: "profile.jpg", mimeType: "image/jpg")
            
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: apiURL, encodingCompletion:
            {
                (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseJSON { response in

                        UtilityMethods.hideIndicator()

                        let swiftyJsonVar = JSON(response.result.value!)
                        print(swiftyJsonVar)
                        if swiftyJsonVar["status"].string == "success" || swiftyJsonVar["status"].string == "Success" {
                            let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                                self.navigationController?.popViewController(animated: false)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            let alert = UIAlertController(title: "Mentor-Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                case .failure(let encodingError):
                    UtilityMethods.hideIndicator()

                    print(encodingError)
                }
        })
    }
    
    @IBAction func actionUploadPic(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: 750, width: 20, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var pickedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        pickedImage = pickedImage.squareImage(with: CGSize(width: 300, height: 300))
        imgPic.contentMode = .scaleAspectFill
        imgPic.image = pickedImage
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func isValidEmail(email: String)-> Bool{
        let emailRegEx = "[A-Za-z0-9.%+-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: email)
    }
    
    func checkValidation() -> Bool {
        let emal = lblEmail.text?.lowercased()
        let finalEmail = emal?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if lblFirstName.text == "" {

            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorFirstName, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }
        else if lblLastName.text == "" {

            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorLasttName, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }
        else if strEmail != "AppleLogin"{
        if lblEmail.text == "" {

            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmail, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }
        else if isValidEmail(email: finalEmail!) == false {
            
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmailFormat, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
                
            return false
            
        }
        }
        else if lblOccupation.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorOccupation, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }
        else if lblQualification.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorQualification, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }
        else if lblExperience.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorExperience, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }
        return true
    }
    
    
    @IBAction func actionpublicPrivate(_ sender: UISwitch) {
        if sender.isOn {
            let alert = UIAlertController(title: "Mentor Mi", message: "You are not subscribed user. Do You want to subscribe.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: {action in
                sender.isOn = false
            }))
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                self.strPrivate = "2"
                let subscribe = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
                subscribe.isfromProfile = "yes"
                subscribe.modalPresentationStyle = .fullScreen
                self.present(subscribe, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            self.strPrivate = "1"
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
