//
//  SoapboxCell.swift
//  Mentor_Mi
//
//  Created by Manish on 30/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class SoapboxCell: UICollectionViewCell {
    
    @IBOutlet weak var imgTrending: UIImageView!
    @IBOutlet weak var imgRecommended: UIImageView!

}
