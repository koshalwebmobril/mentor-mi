//
//  SoapBoxVC.swift
//  Mentor_Mi
//
//  Created by Manish on 30/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import SDWebImage
//import youtube_ios_player_helper
import NVActivityIndicatorView

class SoapBoxVC: UIViewController, NVActivityIndicatorViewable, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {

    let appConstant:AppConstants = AppConstants()
    
    @IBOutlet weak var viewbanner: UIView!
    @IBOutlet weak var collectionTreding: UICollectionView!
    @IBOutlet weak var bgViewGridShow: UIView!
    @IBOutlet weak var colletionRecommended: UICollectionView!
    @IBOutlet weak var btnHide: UIButton!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var bgTrending: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!
    @IBOutlet weak var thoughtView: UIView!
    @IBOutlet weak var thoughtLbl: UIButton!
    @IBOutlet weak var thoughts: UILabel!

    var arrTrending = [JSON]()
    var arrRecommend = [JSON]()
    var arrTop = [JSON]()
    var arrRated = [JSON]()
    var myTableView = UITableView()
    var strAboutMe = String()

    var isOn = true
    var isShow = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)
        getTrendingAPI()
        getRecommendAPI()
        setUI()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(uploadStatusNotify(_:)), name: NSNotification.Name(rawValue: "uploadstatus"), object: nil)

    }
    
    @objc func uploadStatusNotify(_ sender:Notification){
        self.strAboutMe = sender.userInfo?["text"] as? String ?? ""
        self.uploadStatusMethod()
    }
    
    func getTrendingAPI() {
        getTrending(userId: UserDefault.standard.getCurrentUserId()!)
        getProfileData(userId: UserDefault.standard.getCurrentUserId()!)
    }
    
    func getTrending(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getTrending(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTrendingDataResponse(response: response)
        }
    }

    func handleTrendingDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrTrending = tempResponse["data"].arrayValue
            print("Trending: \(arrTrending)")
            collectionTreding.reloadData()
            
        }
    }
    
    func getRecommendAPI() {
         getRecommend(userId: UserDefault.standard.getCurrentUserId()!)
    }
    
    func getRecommend(userId: Int) {
       // let parameters = ["user_id" : userId]
         let parameters = ["user_id" : userId,"type":"0"] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getRecomended(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleRecommendDataResponse(response: response)
        }
    }
    
    func handleRecommendDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrRecommend = tempResponse["data"].arrayValue
            print("Recommend: \(arrRecommend)")
            let tempSubscription = tempResponse["data"][0]["subscription_type"].stringValue
            UserDefault.standard.setCurrentSupscriptionType(type: tempSubscription)
            colletionRecommended.reloadData()
            
        }
    }
    
    func getTopRated() {
        let parameters = ["user_id" : UserDefault.standard.getCurrentUserId()!,"type":"1"] as [String : Any]
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getRecomended(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleRecommendDataResponse(response: response)
        }
    }
    func getRecentData() {
        let parameters = ["user_id" : UserDefault.standard.getCurrentUserId()!,"type":"2"] as [String : Any]
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getRecomended(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleRecommendDataResponse(response: response)
        }
    }
    
    @IBAction func topRatedData(_ sender: Any) {
        getTopRated()
    }
    @IBAction func recentData(_ sender: Any) {
          getRecentData()
       }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationUI()
    }
    
    func setUI() {
        myTableView = UITableView(frame: CGRect(x: 0, y: bgTrending.bounds.origin.y + bgTrending.frame.height + 94, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - (bgTrending.bounds.origin.y + bgTrending.frame.height + 94)))
        
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)
        myTableView.backgroundColor = UIColor.clear
        myTableView.isHidden = true

    }
   
    
    func navigationUI() {
        
        self.tabBarController?.title = "SoapBox"
        tabBarController?.navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        isOn = true
        
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
     @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionTreding {
            return arrTrending.count
        }
        else {
            return arrRecommend.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionTreding {
            let cell = collectionTreding.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SoapboxCell
            
            let strURL = String(format:"%@", self.arrTrending[indexPath.item]["video_thumbnail"].stringValue)
            cell.imgTrending.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            
            cell.imgTrending.layer.borderWidth = 2.5
            cell.imgTrending.clipsToBounds = true
            cell.imgTrending.layer.borderColor = UIColor.white.cgColor
            
            return cell
        }
        else  {
            let celll = colletionRecommended.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! SoapboxCell
            
            let strURL = String(format:"%@",self.arrRecommend[indexPath.item]["video_thumbnail"].stringValue)
            celll.imgRecommended.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            
             let thought = String(format:"%@",self.arrRecommend[indexPath.item]["thought_of_day"].stringValue)
            UserDefault.standard.setThoughtofDay(type: thought)
            
            celll.imgRecommended.layer.borderWidth = 2.5
            celll.imgRecommended.layer.borderColor = UIColor.white.cgColor
            celll.imgRecommended.clipsToBounds = true
            
            return celll
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == colletionRecommended {
            let yourWidth = collectionView.bounds.width/3.0 - 5.0
            let yourHeight = yourWidth
            
            return CGSize(width: yourWidth, height: yourHeight)
        }
        else {
            return CGSize(width: collectionView.bounds.height - 15, height: collectionView.bounds.height - 15)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colletionRecommended {
            let controller = UIStoryboard.playerThumbnail
            controller.fromWhere = "soapBox"
            controller.dictTutorial = arrRecommend[indexPath.row]
            print("SoapBox: \(arrRecommend[indexPath.row]), fromWhere: soapBox")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else {
            let controller = UIStoryboard.playerThumbnail
            controller.fromWhere = "soapBox"
            controller.dictTutorial = arrTrending[indexPath.row]
            print("SoapBox: \(arrRecommend[indexPath.row]), fromWhere: soapBox")
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let controller = UIStoryboard.premium
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let controller = UIStoryboard.setting
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func thought(_ sender: Any) {
        if isOn == true {
            thoughtView.isHidden = false
            thoughtViewUI()
            isOn = false
        }
        else{
            thoughtView.isHidden = true
            isOn = true
        }
    }
    
    @IBAction func actionOpenTable(_ sender: Any) {
        
        if isShow == 1 {
            isShow = 0
           
            bgViewGridShow.isHidden = true
        }
        else {
            isShow = 1
            bgViewGridShow.isHidden = true
        }
        myTableViewUI()
        colletionRecommended.isHidden = true
        myTableView.isHidden = false

        myTableView.reloadData()
    }
    
    func myTableViewUI() {
         myTableView.frame = CGRect(x: myTableView.frame.origin.x, y: bgViewGridShow.frame.origin.y + 5, width: myTableView.frame.width, height: myTableView.frame.height)
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        strAboutMe = statusTxt.text!
        UserDefault.standard.setStatus(type: strAboutMe)
//        uploadStatus()
        uploadStatusMethod()
        statusTxt.text = ""
        }
    }
    
    @IBAction func actionRecomendedShowHide(_ sender: Any) {
        if isShow == 1 {
            isShow = 0
            bgViewGridShow.isHidden = false
            colletionRecommended.frame = CGRect(x: colletionRecommended.frame.origin.x, y: bgViewGridShow.frame.origin.y + bgViewGridShow.frame.height + 5, width: colletionRecommended.frame.width, height: colletionRecommended.frame.height)
        }
        else {
            isShow = 1
            bgViewGridShow.isHidden = true
            thoughtView.isHidden = true
            isOn = true
            colletionRecommended.frame = CGRect(x: colletionRecommended.frame.origin.x, y: bgViewGridShow.frame.origin.y + 5, width: colletionRecommended.frame.width, height: colletionRecommended.frame.height)
        }
        colletionRecommended.isHidden = false
        myTableView.isHidden = true
        colletionRecommended.reloadData()
    }
    
    
    func thoughtViewUI() {
        thoughtView.layer.borderWidth = 1
        thoughtView.layer.borderColor = UIColor.black.cgColor
        let yourAttributes : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont(name: "Helvetica Neue", size: 25)!,
            NSAttributedString.Key.foregroundColor : UIColor.darkGray,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Thought of the day", attributes: yourAttributes)
        
        thoughtLbl.setAttributedTitle(attributeString, for: .normal)
        
        let thoughtoftheday = UserDefault.standard.getThoughtofDay()
        thoughts.text = thoughtoftheday
    }
    
    
    func uploadStatus() {
        uploadStatus(userId: "\(String(describing: UserDefault.standard.getCurrentUserId()!))", status: strAboutMe)
    }
    
    func uploadStatus(userId: String, status: String) {
        let parameters = ["user_id": userId, "status": status]
        print(parameters)
        
        UtilityMethods.showIndicator()
        APIManager.sharedInstance.uploadStatus(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleStatusResponse(response: response)
        }
    }
    
    func handleStatusResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            getProfileData(userId: UserDefault.standard.getCurrentUserId()!)
        }
    }
    
    func getProfileData(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
        
    }
    
    func handleUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
//            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            if let dict = tempResponse["data"].dictionaryObject {
                let firstName = dict["first_name"] as? String ?? ""
                let lastName = dict["last_name"] as? String ?? ""
                let fullName = firstName+" "+lastName
                let userImage = dict["profile_pic"] as? String ?? ""
                UserDefault.standard.setCurrentUserName(userName: fullName)
                UserDefault.standard.setCurrentUserImage(userImage: userImage)
                let data = ["active_now": "true", "user_name": fullName, "user_image":userImage]
                let id = "\(UserDefault.standard.getCurrentUserId()!)"
                FireBaseManager.defaultManager.createNewUser(studentId: id, detailDict: data as NSDictionary)
            }
            
            myData.myDetails = tempResponse["data"]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRecommend.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath) as UITableViewCell
        
        let  imgTable = UIImageView(frame: CGRect(x: 2, y: 2, width: cell.frame.size.width - 4, height: cell.frame.size.height - 4 ))
        
        let strURL = String(format:"%@", self.arrRecommend[indexPath.row]["video_thumbnail"].stringValue)
        imgTable.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))

        cell.addSubview(imgTable)
        imgTable.contentMode = .scaleAspectFill
        imgTable.clipsToBounds = true
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = UIStoryboard.playerThumbnail
        controller.fromWhere = "soapBox"
        controller.dictTutorial = arrRecommend[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }

    func uploadStatusMethod()  {
        
        
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID,"status":self.strAboutMe]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    UserDefault.standard.setStatus(type: self.strAboutMe)
                    self.strAboutMe = ""
                }
                else {
                    
                }
            }
            else {
                
            }
        }
    }
   
}
