//
//  MentorFeedCell.swift
//  Mentor_Mi
//
//  Created by Manish on 13/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class MentorFeedCell: UITableViewCell {

    @IBOutlet weak var collectionMentors: UICollectionView!
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var lblTitleTutorial: UILabel!
    @IBOutlet weak var imgTutorials: UIImageView!

    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
