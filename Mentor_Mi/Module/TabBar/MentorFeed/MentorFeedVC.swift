//
//  MentorFeedVC.swift
//  Mentor_Mi
//
//  Created by Manish on 12/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire

class MentorFeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let appConstant:AppConstants = AppConstants()
    
    var arrMantorsLists1 = [JSON]()
    var arrMentorsVideoLists = [JSON]()
    var storedOffsets = [Int: CGFloat]()
    var colectionMentees: UICollectionView?
    var isOn = true
    var strMentorPopUp = String()
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var tbleMentors: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)
        print(myData.myDetails)
        
        mentorLists(id: UserDefault.standard.getCurrentUserId()!)
        followPosts(id: UserDefault.standard.getCurrentUserId()!)

        // Do any additional setup after loading the view.
    }
    
    func mentorLists(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.mentorLists(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleMentorListsResponse(response: response)
        }
    }
    
    func handleMentorListsResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            print(tempResponse["data"].arrayValue)
            arrMantorsLists1 = tempResponse["data"].arrayValue
            tbleMentors.reloadData()
        }
    }
    
    func followPosts(id: Int) {
        let parameters = ["user_id" : id]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.mentorPost(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleFollowPostsResponse(response: response)
        }
    }
    
    func handleFollowPostsResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            print(tempResponse["data"].arrayValue)
            arrMentorsVideoLists = tempResponse["data"].arrayValue
            tbleMentors.reloadData()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setUI()
        
    }
    
    func setUI() {
        
        tabBarController?.navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false

        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        self.tabBarController?.title = "Mentors Feed"
        
        isOn = true
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return arrMentorsVideoLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let celll1 = tbleMentors.dequeueReusableCell(withIdentifier: "cell1") as! MentorFeedCell
            
            colectionMentees?.removeFromSuperview()
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.sectionInset = UIEdgeInsets(top: 1, left: 5, bottom: 1, right: 5)
            
            colectionMentees = UICollectionView(frame: CGRect(x: 5, y: 3, width: celll1.frame.size.width - 10, height: celll1.bounds.height - 20), collectionViewLayout: layout)
            colectionMentees?.dataSource = self
            colectionMentees?.delegate = self

            colectionMentees?.backgroundColor = UIColor.clear
            colectionMentees?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
            colectionMentees?.showsHorizontalScrollIndicator = false
           
            celll1.addSubview(colectionMentees!)
            
            return celll1
        }
        else {
            let celll1 = tbleMentors.dequeueReusableCell(withIdentifier: "cell2") as! MentorFeedCell
            
            let strURL = String(format:"%@",arrMentorsVideoLists[indexPath.row]["video_thumbnail"].stringValue)
            celll1.imgTutorials.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
            celll1.lblTitleTutorial.text = String(format:"%@",arrMentorsVideoLists[indexPath.row]["post_title"].stringValue)
            celll1.duration.text = String(format:"%@",arrMentorsVideoLists[indexPath.row]["posted"].stringValue)
            celll1.lblTag.text = String(format:"%@",arrMentorsVideoLists[indexPath.row]["post_tag"].stringValue)
            
            celll1.imgTutorials.layer.cornerRadius = 2.5
            celll1.imgTutorials.clipsToBounds = true
            
            return celll1
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 135
        }
        else {
            return 220
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            tbleMentors.reloadData()
        }
        else {
            let controller = UIStoryboard.playerThumbnail
            controller.dictTutorial = arrMentorsVideoLists[indexPath.row]
            print("Selected mentor feed: \(arrMentorsVideoLists[indexPath.row])")
            controller.fromWhere = "MentorFeed"
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMantorsLists1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: UICollectionViewCell = (colectionMentees?.dequeueReusableCell(withReuseIdentifier: "cellIdentifier", for: indexPath))!
        
//        cell.backgroundColor = UIColor.darkGray
        
        let imgMentors = UIImageView.init(frame: CGRect(x: 1, y: 1, width: cell.bounds.height - 2, height: cell.bounds.height - 2))
        let strURL = String(format:"%@",arrMantorsLists1[indexPath.item]["profile_pic"].stringValue)
        imgMentors.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: ""))
        
        cell.addSubview(imgMentors)
        
        imgMentors.layer.cornerRadius = imgMentors.frame.size.height/2
        imgMentors.layer.borderWidth = 2.0
        imgMentors.layer.borderColor = UIColor.white.cgColor
        imgMentors.clipsToBounds = true
        imgMentors.contentMode = .scaleAspectFill 
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let details = UIStoryboard.otherProfileDetail
        let details = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfileDetailVC") as! OtherUserProfileDetailVC
        details.fromWhere = "Mentors"
        details.IDprofile = arrMantorsLists1[indexPath.item]["m_id"].stringValue
        details.dictDetails = arrMantorsLists1[indexPath.item]
        self.navigationController?.pushViewController(details, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.height - 35, height: collectionView.bounds.height - 35)
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let controller = UIStoryboard.premium
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let controller = UIStoryboard.setting
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        uploadStatus()
        }
    }
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusTxt.text!]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        UserDefault.standard.setStatus(type: self.statusTxt.text!)
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
