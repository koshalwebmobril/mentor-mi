//
//  FirebaseManager.swift
//  Weldone
//
//  Created by WebMobril on 30/11/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

typealias Completion = (Bool) -> Void
typealias onCompletion = (String) -> Void
typealias UserCompletion = ([String:Any]?) -> Void
typealias UserOnlineStatusCompletion = (Bool?, NSInteger?) -> Void

@objc protocol FirebaseContactManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingContacts()
}
@objc protocol FirebaseMessageManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingMessages()
}
@objc protocol FirebaseRecentChatsManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingRecentChats()
}
class FireBaseManager: NSObject {
    // Can't init is singleton
    
    static let defaultManager = FireBaseManager()
    var contactManagerDelegate: FirebaseContactManagerDelegate?
    var messageManagerDelegate: FirebaseMessageManagerDelegate?
    var recentChatsManagerDelegate: FirebaseRecentChatsManagerDelegate?
    var contacts:[Contacts] = []
    var messages:[Message] = []
    var recentChats:[RecentChats] = []
    
    var usersRef: DatabaseReference = Database.database().reference().child("users")
    var msgRef: DatabaseReference = Database.database().reference().child("messages")
    var recentsRef: DatabaseReference = Database.database().reference().child("Recents")

    var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://mentormi-fcb42.appspot.com")
    var isChannelPresent = false
    var isMessagePresent: Bool?
    
    //Function to check whether a user exists or not under "Users" node on Firebase.
    func checkUserExistence(studentId: String, onCompletion: @escaping Completion) -> (){
        usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChild(studentId){
                onCompletion(true)
                print("rooms exist")
            }else{
                onCompletion(false)
                print("room doesn't exist")
            }
            
        })
    }
    
    //Function to create a new node under "Users" node on Firebase.
    func createNewUser(studentId: String, detailDict: NSDictionary){
        usersRef.child(studentId).setValue(detailDict)
    }
    
    /** Gets the Contacts object for the specified user id */
    func getUser(_ userID: String, completion: @escaping (Contacts) -> Void) {
        usersRef.child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
            let activeNow = snapshot.childSnapshot(forPath: "active_now").value as? String ?? ""
            let deviceToken = snapshot.childSnapshot(forPath: "device_token").value as? String ?? ""
            let name = snapshot.childSnapshot(forPath: "user_name").value as? String ?? ""
            let email = snapshot.childSnapshot(forPath: "user_email").value as? String ?? ""
            let image = snapshot.childSnapshot(forPath: "user_image").value as? String ?? ""
            let mobile = snapshot.childSnapshot(forPath: "user_mobile").value as? String ?? ""
            let rollNo = snapshot.childSnapshot(forPath: "roll_no").value as? String ?? ""
            
            let id = snapshot.key
            completion(Contacts(activeNow: activeNow, deviceToken: deviceToken, name: name, email: email, image: image, mobile: mobile, id: id, rollNo: rollNo, isFriend: false))
        })
    }
    
    //Function to set data for each message under "Messages" node on Firebase.
    func sendNewMsg(atNode: String, detailDict: [String:Any], recentDict: [String:Any]){
        
        let messageRef = msgRef.child(atNode).childByAutoId()
        //detailDict.setValue(messageRef.key, forKey: "message_id")
        messageRef.setValue(detailDict)
        
        let channelRef = recentsRef.child(atNode)
        //recentDict.setValue(channelRef.key, forKey: "node")
        channelRef.setValue(recentDict)
    }
    
    func fetchAllUsers() {
        usersRef.observe(.childAdded, with: { (snapshot) in
            
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                let activeNow = messageData["active_now"] as? String ?? ""
                let deviceToken = messageData["device_token"] as? String ?? ""
                let name = messageData["user_name"] as? String ?? ""
                let email = messageData["user_email"] as? String ?? ""
                let image = messageData["user_image"] as? String ?? ""
                let mobile = messageData["user_mobile"] as? String ?? ""
                let rollNo = messageData["roll_no"] as? String ?? ""
                
                let studentId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
                
                if snapshot.key != "\(studentId)"{
                    
                    self.contacts.append(Contacts(activeNow: activeNow, deviceToken: deviceToken, name: name, email: email, image: image, mobile: mobile, id: snapshot.key, rollNo: rollNo, isFriend: false))
                }
                if self.contactManagerDelegate != nil{
                    self.contactManagerDelegate?.fireBaseManagerDidCompleteFetchingContacts!()
                }
            }
        })
    }
    
    //Fetch a new message.
    func fetchMessages(node: String){
        
        msgRef.child(node).observe(.childAdded, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                guard let messageData = snapshot.value as? Dictionary<String, Any> else {return}
                
                let message = messageData["message"] as? String ?? ""
                let messageStatus = messageData["messageStatus"] as? String ?? ""
                let user = messageData["user"] as? String ?? ""
                let userId = messageData["userId"] as? String ?? ""
                let msgNode = snapshot.key
                
                self.isMessagePresent = false
                
                if self.messages.count != 0{
                    for index in 0..<self.messages.count {
                        if msgNode == self.messages[index].node{
                            self.isMessagePresent = true
                            break
                        }
                    }
                    if self.isMessagePresent == false{
                        self.messages.append(Message(message: message, messageStatus: messageStatus, user: user, userId: userId, node: snapshot.key))
                    }
                }else{
                    self.messages.append(Message(message: message, messageStatus: messageStatus, user: user, userId: userId, node: snapshot.key))
                }
                if self.messageManagerDelegate != nil{
                    self.messageManagerDelegate?.fireBaseManagerDidCompleteFetchingMessages!()
                }
            }
        })
    }
    
    //Fetch the users whom we have contacted.
    func fetchRecentChats(){
        
        let studentId = UserDefault.standard.getCurrentUserId() ?? 0
        
        self.recentsRef.queryOrdered(byChild: "sender_id").queryEqual(toValue: "\(studentId)").observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                
                guard let channelData = snapshot.value as? Dictionary<String, Any> else {return}
                let node = snapshot.key
                let ids = node.components(separatedBy: "-")
                let frndId = "\(studentId)" == ids.first ? ids.last! : ids.first!
                
                let activeNow = channelData["active_now"] as? String ?? ""
                let lastMsg = channelData["last_message"] as? String ?? ""
                let senderName = channelData["sender_name"] as? String ?? ""
                let senderId = channelData["sender_id"] as? String ?? ""
                let senderImg = channelData["sender_image"] as? String ?? ""
                let recieverName = channelData["receiver_name"] as? String ?? ""
                let recieverId = channelData["receiver_id"] as? String ?? ""
                let recieverImg = channelData["receiver_image"] as? String ?? ""
                                
                if self.recentChats.count != 0{
                    for index in 0..<self.recentChats.count {
                        if node == self.recentChats[index].node{
                            
                            self.isChannelPresent = true
                            break
                        }
                    }
                    if self.isChannelPresent == false{
                        self.recentChats.append(RecentChats(activeNow: activeNow, lastMsg: lastMsg, receiverId: recieverId, receiverImage: recieverImg, receiverName: recieverName, senderId: senderId, senderImg: senderImg, senderName: senderName, node: node))
                    }
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }else{
                    self.recentChats.append(RecentChats(activeNow: activeNow, lastMsg: lastMsg, receiverId: recieverId, receiverImage: recieverImg, receiverName: recieverName, senderId: senderId, senderImg: senderImg, senderName: senderName, node: node))
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }
                
                
            }
        })
        
        self.recentsRef.queryOrdered(byChild: "receiver_id").queryEqual(toValue: "\(studentId)").observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                
                let channelData = snapshot.value as! Dictionary<String, Any>
                let node = snapshot.key
                let ids = node.components(separatedBy: "-")
                let frndId = "\(studentId)" == ids.first ? ids.last! : ids.first!
                
                let activeNow = channelData["active_now"] as? String ?? ""
                let lastMsg = channelData["last_message"] as? String ?? ""
                let senderName = channelData["sender_name"] as? String ?? ""
                let senderId = channelData["sender_id"] as? String ?? ""
                let senderImg = channelData["sender_image"] as? String ?? ""
                let recieverName = channelData["receiver_name"] as? String ?? ""
                let recieverId = channelData["receiver_id"] as? String ?? ""
                let recieverImg = channelData["receiver_image"] as? String ?? ""
                
                if self.recentChats.count != 0{
                    for index in 0..<self.recentChats.count {
                        if node == self.recentChats[index].node{
                            
                            self.isChannelPresent = true
                            break
                        }
                    }
                    if self.isChannelPresent == false{
                        self.recentChats.append(RecentChats(activeNow: activeNow, lastMsg: lastMsg, receiverId: recieverId, receiverImage: recieverImg, receiverName: recieverName, senderId: senderId, senderImg: senderImg, senderName: senderName, node: node))
                    }
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }else{
                    self.recentChats.append(RecentChats(activeNow: activeNow, lastMsg: lastMsg, receiverId: recieverId, receiverImage: recieverImg, receiverName: recieverName, senderId: senderId, senderImg: senderImg, senderName: senderName, node: node))
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }
                
                if self.recentChatsManagerDelegate != nil{
                    self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                }
            }
        })
        
    }
    
    
    func getUserDeviceToken(userId: String, onCompletion: @escaping onCompletion) -> () {
        usersRef.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                if let deviceToken = messageData["device_token"] as? String {
                    onCompletion(deviceToken)
                }
            }
        })
    }
    
    
    
    //Convert timestamp to date string.
    func convertTimeStampToDateString(timeStamp: NSInteger) -> String{
        
        let time = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        //let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.medium
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        let dateString = formatter.string(from: time)
        return dateString
        /*
        if calendar.isDateInToday(time) {return dateString}
        else if calendar.isDateInYesterday(time) {return "Yesterday at \(dateString)"}
        else{
            formatter.dateFormat = "d MMM, yyyy"
            let dateString = formatter.string(from: time)
            return dateString
        }
        */
    }
}
