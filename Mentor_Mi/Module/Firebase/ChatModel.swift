//
//  ChatModel.swift
//  Mentor_Mi
//
//  Created by Rajeev Sharma on 18/01/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation

class Contacts {
    
    var activeNow: String?
    var deviceToken: String?
    var name: String?
    var email: String?
    var image: String?
    var mobile: String?
    var id: String?
    var rollNo: String
    var isFriend: Bool
    
    init(activeNow: String?, deviceToken: String?, name: String?, email: String?, image: String?, mobile: String?, id: String?, rollNo: String, isFriend: Bool) {
        
        self.activeNow = activeNow
        self.deviceToken = deviceToken
        self.name = name
        self.email = email
        self.image = image
        self.mobile = mobile
        self.id = id
        self.rollNo = rollNo
        self.isFriend = isFriend
    }
}

class Message {
    var message: String
    var messageStatus: String
    var user: String
    var userId: String
    var node: String
    
    init(message: String, messageStatus: String, user: String, userId: String, node: String ) {
        
        self.message = message
        self.messageStatus = messageStatus
        self.user = user
        self.userId = userId
        self.node = node
    }
    
}

class RecentChats{
    var activeNow: String
    var lastMsg: String
    var receiverId: String
    var receiverImage: String
    var receiverName: String
    var senderId: String
    var senderImg: String
    var senderName: String
    var node: String
    
    init(activeNow: String, lastMsg: String, receiverId: String, receiverImage: String, receiverName: String, senderId: String, senderImg: String, senderName: String, node: String) {
        
        self.activeNow = activeNow
        self.lastMsg = lastMsg
        self.receiverId = receiverId
        self.receiverImage = receiverImage
        self.receiverName = receiverName
        self.senderId = senderId
        self.senderImg = senderImg
        self.senderName = senderName
        self.node = node
    }
}

class ChatFriend {
    var id: String
    var name: String
    var image: String
    
    init(id: String, name: String, image: String){
        self.id = id
        self.name = name
        self.image = image
    }
}

enum FileType: CaseIterable {
    case audio, video, pdf, doc, docx, ppt, pptx
    
    var fileExtension: String {
        switch self {
        case .audio: return "mp3"
        case .video: return "mov"
        case .pdf: return "pdf"
        case .doc: return "doc"
        case .docx: return "docx"
        case .ppt: return "ppt"
        case .pptx: return "pptx"
        }
    }
    
    var contentType: String {
        switch self {
        case .audio: return "audio/mp3"
        case .video: return "video/mov"
        case .pdf: return "file/pdf"
        case .doc: return "file/doc"
        case .docx: return "file/docx"
        case .ppt: return "file/ppt"
        case .pptx: return "file/pptx"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .audio: return nil
        case .video: return nil
        case .pdf: return #imageLiteral(resourceName: "pdf")
        case .doc, .docx: return #imageLiteral(resourceName: "doc")
        case .ppt, .pptx: return #imageLiteral(resourceName: "ppt")
        }
    }
}

enum MessageType {
    case image, video, audio, file
    
    var icon: UIImage {
        switch self {
        case .image: return #imageLiteral(resourceName: "camera")
        case .video: return #imageLiteral(resourceName: "video")
        case .audio: return #imageLiteral(resourceName: "audio")
        case .file: return UIImage(named: "file")!
        }
    }
}
