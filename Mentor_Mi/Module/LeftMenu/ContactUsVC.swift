//
//  ContactUsVC.swift
//  Mentor_Mi
//
//  Created by deepkohli on 31/07/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ContactUsVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var titleTextFild: UITextField!
    @IBOutlet weak var emailTextFild: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var viewForTextFiled: UIView!
    @IBOutlet weak var viewForTextView: UIView!
     @IBOutlet weak var viewForEmail: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        messageTextView.textColor = UIColor.lightGray
               messageTextView.text = "Description"
               setUpIntial()
    }
    func setUpIntial(){
        viewForTextView.layer.cornerRadius = 5
        viewForTextView.layer.borderWidth = 1.0
        viewForTextView.layer.borderColor = UIColor.black.cgColor
        viewForTextView.clipsToBounds = true
        viewForTextFiled.layer.cornerRadius = 5
        viewForTextFiled.layer.borderWidth = 1.0
        viewForTextFiled.layer.borderColor = UIColor.black.cgColor
        viewForTextFiled.clipsToBounds = true
        cancelButton.layer.cornerRadius = cancelButton.frame.size.height / 2
        cancelButton.clipsToBounds = true
        submitButton.layer.cornerRadius = submitButton.frame.size.height / 2
        submitButton.clipsToBounds = true
        messageTextView.delegate = self
       
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
         if textView.textColor == UIColor.lightGray {
             textView.text = nil
             textView.textColor = UIColor.black
         }
     }
     func textViewDidEndEditing(_ textView: UITextView) {
         if textView.text.isEmpty {
             textView.text = "Description"
             textView.textColor = UIColor.lightGray
         }
     }
     @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
     }
     @IBAction func cancelButtonAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
     }
     @IBAction func submitButtonAction(_ sender: Any) {
         if self.areValidInputs() {
             contactUSAPI()
         }
     }
     
     //MARK:-- Validate input fields.
     private func areValidInputs() -> Bool {
         
         let whiteSpaces = CharacterSet.whitespacesAndNewlines
     
         if titleTextFild.text!.isEmpty || titleTextFild.text!.trimmingCharacters(in: whiteSpaces).isEmpty
         {
             titleTextFild.becomeFirstResponder()
             UIAlertController.showAlert(withTitle: "Mentor_Mi", message: "Please enter title.", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
             return false
             
         }
         
         if messageTextView.text!.isEmpty || messageTextView.text!.trimmingCharacters(in: whiteSpaces).isEmpty || messageTextView.text == "Description"
         {
             messageTextView.becomeFirstResponder()
            UIAlertController.showAlert(withTitle: "Mentor_Mi", message: "Please enter description.", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
             return false
             
         }
         return true
     }

    func contactUSAPI(){
        let parameters = ["title": titleTextFild.text!,
                          "message": messageTextView.text!,
                          "user_id" : UserDefault.standard.getCurrentUserId()!] as [String:Any]
        UtilityMethods.showIndicator()
        APIManager.sharedInstance.contactUS(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleContactUSResponse(response: response)
        }
    }
    func handleContactUSResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
             UIAlertController.showAlert(withTitle: "Mentor_Mi", message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            let alert = UIAlertController(title: "Mentor_Mi", message: "Message sent successfully", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)

            }))
            self.present(alert, animated: true, completion: nil)
        }
    }

}
