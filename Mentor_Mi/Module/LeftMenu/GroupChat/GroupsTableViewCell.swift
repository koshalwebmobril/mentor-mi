//
//  GroupsTableViewCell.swift
//  Mentor Mi
//
//  Created by WebMobril on 06/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

class GroupsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectView: UICollectionView!
    @IBOutlet weak var imgGroupIcon: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var btnAddVideos: UIButton!
    @IBOutlet weak var btnMsgs: UIButton!
    @IBOutlet weak var btnForMember: UIButton!
    @IBOutlet weak var btnAddMyGroup: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension GroupsTableViewCell{
    
    func setCollectonViewDataourceDelegate
        <D:UICollectionViewDelegate & UICollectionViewDataSource > (_ dataSourceDelegate: D, forRow row:Int)
    {
        
        collectView.delegate = dataSourceDelegate
        collectView.dataSource = dataSourceDelegate
        
        collectView.reloadData()
        
        
    }
    
    
}
