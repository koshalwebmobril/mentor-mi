//
//  GroupsListsVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 05/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import SDWebImage
import SkyFloatingLabelTextField

class GroupsListsVC: UIViewController , UITableViewDelegate , UITableViewDataSource , UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var noGroup: UIImageView!
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var statusTxt: UITextField!
    
    var isClick = true
    var isOn = true
    let appConstant:AppConstants = AppConstants()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var tableGroups: UITableView!
    @IBOutlet weak var imgNewgroup: UIImageView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var bgCreateGroup: UIView!
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var imgCreateGroup: UIImageView!
    var arrGroup = [JSON]()
    var arrVideos = [JSON]()
    
    @IBOutlet weak var txtgroupname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtgroupDiscript: SkyFloatingLabelTextField!
    @IBOutlet weak var btnCreateGroup: UIButton!
    
    var userID = Int()
    var arrTimeLine = [JSON]()
    
    @IBOutlet weak var threedotsinfo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view.endEditing(true)
        isClick = true
        
        if isClick == false{
            bgCreateGroup.isHidden = false
        }
        
        noGroup.isHidden = true
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.navigationItem.rightBarButtonItems = rightBarButtons
        navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        tableGroups.isHidden = true
        bgCreateGroup.isHidden = true
        
        bgCreateGroup.layer.cornerRadius = 20.0
        bgCreateGroup.layer.borderWidth = 1.5
        bgCreateGroup.layer.borderColor = UIColor.lightGray.cgColor
        bgCreateGroup.clipsToBounds = true
        
        btnCreateGroup.layer.cornerRadius = 5.0
        btnCreateGroup.clipsToBounds = true
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(GroupsListsVC.NewGroupForm))
        imgNewgroup.isUserInteractionEnabled = true
        imgNewgroup.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(GroupsListsVC.openMediaFiles))
        imgCreateGroup.isUserInteractionEnabled = true
        imgCreateGroup.addGestureRecognizer(tap2)
        
        imgCreateGroup.layer.cornerRadius = imgCreateGroup.frame.height/2
        imgCreateGroup.clipsToBounds = true
        
        self.title = "Group Chat"
        imagePicker.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeStatusOfCreateGroup(_ :)), name: NSNotification.Name("changeGroupStatus"), object: nil)
        
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
    }
    
    @objc func changeStatusOfCreateGroup(_ sender:Notification){
        
        isClick = true
        
    }

    //    Mark :- NotificationClick

    @objc func notification() {
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }
    

     //    Mark :- ThreeDots
    
    @objc func threedots() {
     
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.isHidden = false
        navigationController?.isNavigationBarHidden = false
        
        //isClick = true
        
        if isClick == false{
            bgCreateGroup.isHidden = false
        }
        else{
            bgCreateGroup.isHidden = true
            
            noGroup.isHidden = true
             isOn = true
            
             userID = UserDefault.standard.getCurrentUserId()!
             //bgCreateGroup.isHidden = true
             
//             if arrGroup.isEmpty == false {
//                 arrGroup.removeAll()
//                 arrVideos.removeAll()
//             }
             
             myGroupListAPI()
            
        }
        
       

    }
    
   
    
    
     //    Mark :- apiGetProfileData
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    
                    
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
            }
        }
    }
    
     //    Mark :- apiTimeLine
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = swiftyJsonVar["data"].arrayValue
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
            }
        }
    }
    
     //    Mark :- uploadStatus
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefault.standard.getStatus()
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusTxt.text!]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        UserDefault.standard.setStatus(type: self.statusTxt.text!)
                        let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                             let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                                 self.getProfileData()
                             })
                             alert.addAction(ok)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    //    Mark :- actionButton
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
       
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
        
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
    }
    
    @objc func NewGroupForm () {
        if bgCreateGroup.isHidden == true {
            tableGroups.isHidden = true
            bgCreateGroup.isHidden = false
            isClick = false
            self.title = "Create Group"
        }
        else {
            txtgroupname.text = ""
            txtgroupDiscript.text = ""
            tableGroups.isHidden = false
            bgCreateGroup.isHidden = true
            imgCreateGroup.image = UIImage (named: "camera")
            isClick = true
            self.title = "Create Group"
        }
    }
    
    // Mark :- mediaFiles
    
    @objc func openMediaFiles () {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
     // Mark :- ImagePickerDelegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var pickedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        pickedImage = pickedImage.squareImage(with: CGSize(width: 300, height: 300))
        imgCreateGroup.image = pickedImage
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
     // Mark :- apiGroupLists
    
    func myGroupListAPI() {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.groupsByUsers)"
        print("\(apiURL)")
        
        
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrGroup.removeAll()
                    self.arrGroup = swiftyJsonVar["data"].arrayValue
                    if self.arrGroup.count > 0 {
                        self.tableGroups.isHidden = false
                        self.tableGroups.reloadData()
                    }
                    
                }
                else {
                    self.noGroup.isHidden = false
                    if swiftyJsonVar["message"].string == "No Groups Found !!"{
                        self.tableGroups.isHidden = true
                         self.tableGroups.reloadData()
                    }
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
            }
        }
    }
    
    
    //MARK:- Tableview Delegates and Datasource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGroup.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupsTableViewCell") as? GroupsTableViewCell
        if cell == nil {
            
        }
        
        if arrGroup.count > 0 {
            
            arrVideos = arrGroup[indexPath.row]["videos"].arrayValue
            
            for index in 0..<arrVideos.count {
                let temp = arrVideos[index]["thumbnail"].stringValue
                print(temp)
            }
            
            let tempAdminId = arrGroup[indexPath.row]["admin_id"].intValue
            if tempAdminId == userID {
                cell?.btnAddMyGroup.isHidden = false
            }
            else {
                cell?.btnAddMyGroup.isHidden = true
            }
            
            cell?.lblGroupName.text = arrGroup[indexPath.row]["group_name"].stringValue
            let iconURL = arrGroup[indexPath.row]["group_icon"].stringValue
            cell?.imgGroupIcon.sd_setImage(with: URL(string: iconURL), placeholderImage: UIImage(named: "imgUnSelected"))
            cell?.collectView.tag = indexPath.row
        }
        
       // camera
        cell?.btnAddVideos.setImage(UIImage(named: "camera"), for: .normal)
        cell?.btnAddMyGroup.tag = indexPath.row
        cell?.btnAddMyGroup.addTarget(self, action: #selector(addOnMygroup), for: .touchUpInside)
        cell?.btnAddMyGroup.layer.cornerRadius = (cell?.btnAddMyGroup.frame.height)!/2
        cell?.btnAddMyGroup.clipsToBounds = true
        cell?.btnForMember.tag = indexPath.row
        cell?.btnForMember.addTarget(self, action: #selector(groupLists(_:)), for: .touchUpInside)
        cell?.imgGroupIcon.layer.cornerRadius = (cell?.imgGroupIcon.frame.height)!/2
        cell?.imgGroupIcon.layer.borderWidth = 1.0
        cell?.imgGroupIcon.layer.borderColor = UIColor.white.cgColor
        cell?.btnMsgs.tag = indexPath.row
        cell?.btnAddVideos.tag = indexPath.row
        cell?.btnAddVideos.addTarget(self, action: #selector(GroupsListsVC.addVideo), for: .touchUpInside)
        
        cell?.btnMsgs.addTarget(self, action: #selector(GroupsListsVC.messages), for: .touchUpInside)
        
        return cell!
    }
    
    @objc func addOnMygroup(_ sender: UIButton) {
       
        let lists = self.storyboard?.instantiateViewController(withIdentifier: "UserListsVC") as! UserListsVC
        lists.byGroupLists = true
//        lists.idGroup = ((arrGroup.object(at: sender.tag) as! NSDictionary).object(forKey: "group_id") as! String)
        lists.idGroup = arrGroup[sender.tag]["group_id"].stringValue
        self.navigationController?.pushViewController(lists, animated: true)
    
    }
    
    @objc func addVideo(_ sender: UIButton) {
        let temp = arrGroup[sender.tag]["group_id"].stringValue
        print(temp)
        let upload = UIStoryboard.uploadVideo
        //let upload = UIStoryboard.tutorialByGroup
         upload.strGroupId = temp
         upload.isClassType = "GroupChat"
        self.navigationController!.pushViewController(upload, animated: true)
    }
    
    @objc func messages(_ sender: UIButton) {
        
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatsVC") as! GroupChatsVC
//        chat.dictChatgroup = (arrGroup.object(at: sender.tag) as! NSDictionary)
        chat.dictChatgroup = arrGroup[sender.tag].dictionary! as NSDictionary
        self.navigationController?.pushViewController(chat, animated: true)
        
    }
    
    
    @objc func groupLists(_ sender: UIButton) {
//        let temp = String(format: "%@",(arrGroup.object(at: sender.tag) as! NSDictionary).object(forKey: "group_id") as! String)
        let temp = arrGroup[sender.tag]["group_id"].stringValue
        print(temp)
        
        let member = self.storyboard?.instantiateViewController(withIdentifier: "GroupMembersVC") as! GroupMembersVC
        member.strGroupId = temp
        self.navigationController?.pushViewController(member, animated: true)
        
    }
    
    //    Mark :- CollectionViewDelegates&Datasources
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrVideos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InsideCollectionViewCell", for: indexPath) as! InsideCollectionViewCell

        if arrVideos.count > 0 {
//            let imgUrl = arrVideos[indexPath.row]["thumbnail"].stringValue
            
            let imgUrl = arrGroup[collectionView.tag]["videos"][indexPath.row]["thumbnail"].stringValue
            cell.imgTutorial.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "imgUnSelected"))

//            let tempPostId = arrVideos[indexPath.row]["postid"].stringValue
//            print("Group Video Id: \(tempPostId)")
//            print("Videos Count: \(arrVideos.count)")
        }

        cell.imgTutorial.layer.cornerRadius = 2.5
        cell.imgTutorial.clipsToBounds = true

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let tempPostId = arrGroup[collectionView.tag]["videos"][indexPath.row]["postid"].stringValue
//        let tempPostId = arrVideos[indexPath.row]["postid"].stringValue
        print(tempPostId)

        if tempPostId == "" {
            let alert  = UIAlertController(title: "Mentor-Mi" , message: "No videos added yet.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
        else {
            let play = self.storyboard?.instantiateViewController(withIdentifier: "PlayerThumbnailVC") as! PlayerThumbnailVC
            print(arrGroup[collectionView.tag]["videos"])
            play.dictTutorial = arrGroup[collectionView.tag]["videos"][indexPath.row]
            play.strURL = arrGroup[collectionView.tag]["videos"][indexPath.row]["video"].stringValue
            play.fromWhere = "Group"
            self.navigationController?.pushViewController(play, animated: true)
        }
    }
    
    //    Mark :- actionCreateGroup
    
    @IBAction func actionCreateGroup(_ sender: Any) {
       if txtgroupname.text == "" {
        let alert  = UIAlertController(title: "Mentor-Mi" , message: "Please enter group name", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
       }
       else if  txtgroupDiscript.text == "" {
            let alert  = UIAlertController(title: "Mentor-Mi" , message: "Please enter description", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
       }
       else {
           
        
            let lists = self.storyboard?.instantiateViewController(withIdentifier: "UserListsVC") as! UserListsVC
            lists.strtitle = txtgroupname.text!
            lists.strDescription = txtgroupDiscript.text!
            lists.imgGroupicon.image = imgCreateGroup.image
            self.navigationController?.pushViewController(lists, animated: true)
        }
    }
    
    
    
    @IBAction func actionCretaegroup(_ sender: Any) {
        
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.createGroup)"
            print("\(apiURL)")
            
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        let responseJson = swiftyJsonVar["data"]
                        print(responseJson)
                        
                        self.arrGroup = swiftyJsonVar["data"].arrayValue
//                        self.arrGroup = swiftyJsonVar["data"].arrayObject! as NSArray
                        if self.arrGroup.count > 0 {
                            self.tableGroups.isHidden = false
                            self.tableGroups.reloadData()
                        }
                        
                    }
                    else {
                        let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                }
            }
    }
    

}


