//
//  UsersListsCell.swift
//  Mentor Mi
//
//  Created by WebMobril on 18/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

class UsersListsCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var btnLikeDislike: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
