//
//  CreateTutorialByGroupVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 19/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SkyFloatingLabelTextField
import DropDown
import AVKit
import AVFoundation
import MobileCoreServices
import SVProgressHUD


class CreateTutorialByGroupVC: UIViewController {
    @IBOutlet weak var txtTite: SkyFloatingLabelTextField!
    @IBOutlet weak var txtContent: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtTag: SkyFloatingLabelTextField!
    @IBOutlet weak var btnLevel: UIButton!
    @IBOutlet weak var btnCreate: UIButton!
     @IBOutlet weak var catlabel: UILabel!
    
    @IBOutlet weak var topConstraints: NSLayoutConstraint!
    
    
    var dropDownLevel = DropDown()
    var arrLevel = ["Advanced","Intermediate","Begineer"]
    var pathVideo : URL!
    var imgData = Data()
    var strGroupId = String()
    var btnTitle = "Select Level"
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var imgView = UIImageView()
    var isOn = true
    var arrTimeLine = [JSON]()
    var isCheckGroupCreated = false
    
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var statusTxt: UITextField!
    
    @IBAction func cancel(_ sender: Any) {
         statusView.isHidden = true
    }
    
    
    override func viewDidLayoutSubviews() {
        if UIScreen.main.bounds.width > 375{
        //topConstraints.constant = 40
        }
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
            }
        }
    }
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = swiftyJsonVar["data"].arrayValue
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
            }
        }
    }
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefault.standard.getStatus()
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusAbtMe]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        
                        self.getProfileData()
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    @IBOutlet weak var threedotsinfo: UIView!
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
    }
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: "videoPathhhh") != nil{
            
            pathVideo = URL(string:UserDefaults.standard.string(forKey: "videoPathhhh")!)
            //pathVideo = (UserDefaults.standard.object(forKey: "videoPath") as! URL)
        }
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        self.dropDownLevel.dataSource = self.arrLevel
        dropDownLevel.anchorView = self.btnLevel
        dropDownLevel.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnTitle = self.arrLevel[index]
            
            self.btnLevel.titleLabel?.textColor = UIColor.clear
            self.btnLevel.titleLabel!.text = self.btnTitle
            self.catlabel.text = self.arrLevel[index]
         
        }
        
       
        btnLevel.layer.cornerRadius = 5.0
        btnLevel.clipsToBounds = true
        btnLevel.layer.borderWidth = 1.5
        btnLevel.layer.borderColor = UIColor.black.cgColor
        
        btnCreate.layer.cornerRadius = 5.0
        btnCreate.clipsToBounds = true
        
        do {
            if pathVideo != nil{
            let asset = AVURLAsset(url: pathVideo!, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            
            imgView = UIImageView(image: rotateImage(image: uiImage, angle: 270.0, flipVertical: 0.0, flipHorizontal: 0.0))
            }
        }
        catch let error as NSError {
            print("Error generating thumbnail: \(error)")
        }
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        
        self.navigationController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        

        // Do any additional setup after loading the view.
    }
    
    @objc func notification() {
        
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isOn = true
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.barTintColor = appDelegate.appConstant.navigationColor
        
        threedotsinfo.isHidden = true
        
    }
    
   
    @objc func threedots() {
        
        
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }
    
    
    func rotateImage(image:UIImage, angle:CGFloat, flipVertical:CGFloat, flipHorizontal:CGFloat) -> UIImage? {
        let ciImage = CIImage(image: image)
        let filter = CIFilter(name: "CIAffineTransform")
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        // let newAngle = angle * CGFloat(-1)
        let newAngle = (angle * .pi) / 180.0
        var transform = CATransform3DIdentity
        transform = CATransform3DRotate(transform, CGFloat(newAngle), 0, 0, 1)
        transform = CATransform3DRotate(transform, CGFloat(Double(flipVertical) * M_PI), 0, 1, 0)
        transform = CATransform3DRotate(transform, CGFloat(Double(flipHorizontal) * M_PI), 1, 0, 0)
        let affineTransform = CATransform3DGetAffineTransform(transform)
        filter?.setValue(NSValue(cgAffineTransform: affineTransform), forKey: "inputTransform")
        let contex = CIContext(options: [CIContextOption.useSoftwareRenderer:true])
        let outputImage = filter?.outputImage
        let cgImage = contex.createCGImage(outputImage!, from: (outputImage?.extent)!)
        let result = UIImage(cgImage: cgImage!)
        return result
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       // self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCretae(_ sender: Any) {
        if txtTite.text == "" {
            let alert = UIAlertController(title: "Mentor-Mi", message: "Please enter title", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if txtContent.text == "" {
            let alert = UIAlertController(title: "Mentor-Mi", message: "Please enter content of Tutorials", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        else if txtTag.text == "" {
            let alert = UIAlertController(title: "Mentor-Mi", message: "Please enter tag", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if btnTitle == "Select Level" {
            let alert = UIAlertController(title: "Mentor-Mi", message: "Please select difficulty level of tutorial.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else  {
            upload()
        }
    }
    
    func upload() {
        
        if isCheckGroupCreated == false{
            isCheckGroupCreated = true
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addTuteInGroup)"
        print("\(apiURL)")
        
        var movieData:Data?
         movieData = UserDefaults.standard.object(forKey: "videoPath") as? Data
         /*
        do{
            if pathVideo != nil{
               // movieData = try NSData(contentsOfFile: "file:///private/var/mobile/Containers/Data/PluginKitPlugin/FC176881-4D44-4F1F-A9A7-1FDB504DD08A/tmp/trim.9068C9F6-B2FC-40F4-9A57-C689D384884E.MOV", options: []) as Data?
                movieData =  try Data.init(contentsOf: pathVideo!, options: .dataReadingMapped)
//            movieData =  try Data.init(contentsOf: pathVideo!, options: .alwaysMapped)
            print(movieData!)
            }
        }catch{
    
        }
        */
        if imgView.image != nil{
        let image = imgView.image
        imgData = image!.jpegData(compressionQuality: 0.5)!
        }
           if UIScreen.main.bounds.width > 375{
            SVProgressHUD.show(withStatus: "Loading...")
            }
           else{
            UtilityMethods.showIndicator()
            }
        let userID = String(format: "%d", UserDefault.standard.getCurrentUserId()!)
    
        let newTodo: [String: Any] = ["user_id":userID,"group_id":strGroupId,"post_title":"\(txtTite.text ?? "")","post_content":"\(txtContent.text ?? "")","tag":"\(txtTag.text ?? "")","difficulty_level":"\(btnTitle)"]
        print("parameters are \(newTodo)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if movieData != nil{
            multipartFormData.append(movieData!, withName: "uploadedvediofile", fileName: "file.mp4", mimeType: "video/mp4")
            multipartFormData.append(self.imgData, withName: "thumbimg",fileName: "thumbimg.jpg", mimeType: "image/jpg")
            }
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:apiURL)
        {
            (result) in
            switch result {
            case .success(let upload, _, _):
           
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
    
                })
                upload.responseJSON { response in
                   UtilityMethods.hideIndicator()
                    print(response.result.value!)
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if UIScreen.main.bounds.width > 375{
                        SVProgressHUD.dismiss()
                    }
                    if swiftyJsonVar["status"].string == "success" {
                        self.isCheckGroupCreated = false
                        let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            self.appDelegate.goToGroup()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else {
                        self.isCheckGroupCreated = false
                             UtilityMethods.hideIndicator()
                        if swiftyJsonVar["message"].stringValue == "Sorry, You are not a Subscribed user to Add more Tutorials." {
    
                            let alert = UIAlertController(title: "Mentor Mi", message: "\(swiftyJsonVar["message"].stringValue) want to subscribe?", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                                
                                self.appDelegate.goToGroup()
                               // let subscribe = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
                               // self.present(subscribe, animated: true, completion: nil)
    
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            
                            UtilityMethods.hideIndicator()
                            let alert = UIAlertController(title: "Mentor-Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
    
                    }
                }
            case .failure(let encodingError):
                UtilityMethods.hideIndicator()
                print(encodingError)
                self.isCheckGroupCreated = false
            }
        }
        }
    }
    
    @IBAction func actionlevel(_ sender: Any) {
        dropDownLevel.show()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
