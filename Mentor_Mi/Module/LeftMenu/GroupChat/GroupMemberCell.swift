//
//  GroupMemberCell.swift
//  Mentor Mi
//
//  Created by WebMobril on 23/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

class GroupMemberCell: UITableViewCell {
    
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMember: UILabel!
    @IBOutlet weak var lblOccupMember: UILabel!
    @IBOutlet weak var btnDeleteMember: UIButton!
    
    @IBOutlet weak var btnExitGroup: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
