//
//  GroupMembersVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 19/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import SDWebImage


class GroupMembersVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var strGroupId = String()
    var arrMembers = [JSON]()
    @IBOutlet weak var tableGroupMembers: UITableView!
    var isCheckAdmin = false
    var strDeleteMemberId = ""
    var isOn = true
    var arrTimeLine = [JSON]()
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    
    @IBAction func cancel(_ sender: Any) {
         statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
            }
        }
    }
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = swiftyJsonVar["data"].arrayValue
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
            }
        }
    }
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefault.standard.getStatus()
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusTxt.text]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        
                        self.getProfileData()
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    @IBOutlet weak var threedotsinfo: UIView!
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableGroupMembers.delegate = self
        tableGroupMembers.dataSource = self
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        
        //        self.navigationController?.navigationItem.rightBarButtonItems  = rightBarButtons
        self.navigationItem.rightBarButtonItems = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        
       if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isOn = true
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
         memberLists()
        self.navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = false
        
    }
    
    
    @objc func notification() {
        
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }
    
    
    
    @objc func threedots() {
        
       
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }
    
//    view_users_by_groupid
    func memberLists() {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.groupMembers)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["group_id": strGroupId]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrMembers = swiftyJsonVar["data"].arrayValue
                    
                    let tempAdminId = self.arrMembers[0]["admin_id"].intValue
                    if tempAdminId == UserDefault.standard.getCurrentUserId() {
                        self.isCheckAdmin = true
                    }
                    self.tableGroupMembers.reloadData()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrMembers.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell0 = tableGroupMembers.dequeueReusableCell(withIdentifier: "cell0") as! GroupMemberCell
            if isCheckAdmin == true {
                cell0.btnDeleteMember.isHidden = false
            }
            else {
                cell0.btnDeleteMember.isHidden = true
            }
            cell0.btnDeleteMember.tag = indexPath.row
            cell0.btnDeleteMember.addTarget(self, action: #selector(deleteMember), for: .touchUpInside)
            
            cell0.lblMember.text = "\(arrMembers[indexPath.row]["first_name"].stringValue)" + " " + "\(arrMembers[indexPath.row]["last_name"].stringValue)"
            cell0.lblOccupMember.text = "\(arrMembers[indexPath.row]["occupation"].stringValue)"
            
            let iconURL = "\(arrMembers[indexPath.row]["profile_pic"].stringValue)"
            cell0.imgMember.sd_setImage(with: URL(string: iconURL), placeholderImage: UIImage(named: ""))
            
            cell0.imgMember.layer.cornerRadius = cell0.imgMember.frame.height/2
            cell0.imgMember.clipsToBounds = true
            cell0.imgMember.layer.masksToBounds = true
            
            
            return cell0
        }
        else  {
            let cell1 = tableGroupMembers.dequeueReusableCell(withIdentifier: "cell1") as! GroupMemberCell
            
            cell1.btnExitGroup.addTarget(self, action: #selector(removeGroup), for: .touchUpInside)
            
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        else {
            return 75
        }
    }
    
    @objc func deleteMember(_ sender: UIButton) {
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Want to delete this member from your group?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
            self.strDeleteMemberId = "\(self.arrMembers[sender.tag]["user_id"].stringValue)"
            self.removeMember()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func removeMember() {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.removeMemberFromGroup)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id":"\(userID)","group_id":"\(strGroupId)","member_id":"\(strDeleteMemberId)"]
        print("Parameter are : \(newTodo)")
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let alert  = UIAlertController(title: "Success" , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.memberLists()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
            }
        }
    }
    
    @objc func removeGroup(_ sender: UIButton) {
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Want to exit from group ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            self.exitGroup()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func exitGroup() {
        UtilityMethods.showIndicator()
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.exitGroup)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["group_id": strGroupId,"user_id":"\(userID)"]
        print("Parameter are : \(newTodo)")
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            
             UtilityMethods.hideIndicator()
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                if swiftyJsonVar["status"].string == "success" {
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {

            }
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
