//
//  UserListsVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 12/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import SDWebImage


class UserListsVC: UIViewController , UITableViewDelegate, UITableViewDataSource , UISearchBarDelegate {
    
    var dictParameter = [String: Any]() 
    @IBOutlet weak var btnCreateGroup: UIButton!
    @IBOutlet weak var tableUsers: UITableView!
    @IBOutlet weak var search: UISearchBar!
    var arrUserLists = [JSON]()
    var arrUserSearch = [JSON]()
    var isSearchActive = false
    var imgGroupicon = UIImageView()
    var arrSelection = NSMutableArray()
    let appConstant:AppConstants = AppConstants()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let  preClass: GroupsListsVC = GroupsListsVC()
    var strtitle = String()
    var strDescription = String()
    var idGroup = String()
    var byGroupLists = false
    var isOn = true
    var arrTimeLine = [JSON]()
    var isCheckCreateGroupOrNot = false
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var statusTxt: UITextField!
    
    
    
    @IBOutlet weak var threedotsinfo: UIView!
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.isHidden = false
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        
       //self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        self.navigationItem.rightBarButtonItems = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        tabBarController?.navigationController?.isNavigationBarHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.title = "Create Group"
        isOn = true
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        navigationController?.isNavigationBarHidden = false
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.tintColor = UIColor.black

       
        allUsersLists()
    }
    
    @objc func notification() {
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }
    
  
    @objc func threedots() {
        
        
        if isOn == true {
            
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }
    
  
    func allUsersLists() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getAllUser)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        print(userID)
        
        let newTodo: [String: Any] = ["user_id":userID]
        print(newTodo)
        
        Alamofire.request(apiURL, method: .get, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    self.arrUserLists = swiftyJsonVar["data"].arrayValue
                    if self.arrUserLists.count > 0 {
                        self.tableUsers.reloadData()
                    }
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {

            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive {
            return arrUserSearch.count
        }
        else {
            return arrUserLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableUsers.dequeueReusableCell(withIdentifier: "celll") as! UsersListsCell
        
        var tempId = String()
        
        if isSearchActive {
            tempId = arrUserSearch[indexPath.row]["user_id"].stringValue
            
            let strURL1 = arrUserSearch[indexPath.row]["user_profile_pic"].stringValue
            cell.imgUser.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "profile"))
            
            let flagMentor =  arrUserSearch[indexPath.row]["Mentor_flag"].stringValue
            let flagMentees = arrUserSearch[indexPath.row]["Mentee_flag"].stringValue
            if flagMentor == "1" {
                cell.btnLikeDislike.setImage(UIImage(named: ""), for: .normal)
            }
            else if flagMentees == "1" {
                cell.btnLikeDislike.setImage(UIImage(named: ""), for: .normal)
            }
            else {
                cell.btnLikeDislike.isHidden = true
            }
            
            cell.lblName.text = String(format:"%@ %@", arrUserSearch[indexPath.row]["user_first_name"].stringValue , arrUserSearch[indexPath.row]["user_last_name"].stringValue )
            
            cell.lblDesignation.text = String(format:"%@",arrUserSearch[indexPath.row]["user_occupation"].stringValue)
            
        }
        else  {
            tempId = String(format:"%@",(arrUserLists[indexPath.row]["user_id"].stringValue))
            let strURL1 = String(format:"%@",arrUserLists[indexPath.row]["user_profile_pic"].stringValue)
            cell.imgUser.sd_setImage(with: URL(string: strURL1), placeholderImage: UIImage(named: "profile"))

            
            cell.lblName.text = String(format:"%@ %@",arrUserLists[indexPath.row]["user_first_name"].stringValue ,arrUserLists[indexPath.row]["user_last_name"].stringValue)
            
            let flagMentor = String(format:"%d",(arrUserLists[indexPath.row]["Mentor_flag"].intValue))
            let flagMentees = String(format:"%d",(arrUserLists[indexPath.row]["Mentee_flag"].intValue))
            
            if flagMentor == "1" {
                cell.btnLikeDislike.isHidden = false
                cell.btnLikeDislike.setImage(UIImage(named: "heartgreen"), for: .normal)
            }
            else if flagMentees == "1" {
                cell.btnLikeDislike.isHidden = false
                cell.btnLikeDislike.setImage(UIImage(named: "heartGreenBordered"), for: .normal)
            }
            else {
                cell.btnLikeDislike.isHidden = true
            }
            
            cell.lblDesignation.text = String(format:"%@",arrUserLists[indexPath.row]["user_occupation"].stringValue)
        }
        
        if arrSelection.contains(tempId) {
            cell.btnSelection.setImage(UIImage(named: "checked"), for: .normal)
        }
        else {
            cell.btnSelection.setImage(UIImage(named: "checkBox"), for: .normal)
        }
        
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.height/2
        cell.imgUser.clipsToBounds = true
        
        cell.btnSelection.tag = indexPath.row
        cell.btnSelection.addTarget(self, action: #selector(self.selectUnselect(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
            threedotsinfo.isHidden = true
        
    }
    
    @objc func selectUnselect(_ sender: UIButton) {
        if isSearchActive {
            let tempId = String(format:"%@",arrUserSearch[sender.tag]["user_id"].stringValue)
            if arrSelection.contains(tempId) {
                for index in 0..<arrSelection.count {
                    if arrSelection[index] as! String == tempId {
                        arrSelection.removeObject(at: index)
                        break
                    }
                    else  {
                        
                    }
                }
            }
            else {
                arrSelection.add(tempId)
            }
        }
        else {
            let tempId = String(format:"%@",arrUserLists[sender.tag]["user_id"].stringValue)
            if arrSelection.contains(tempId) {
                for index in 0..<arrSelection.count {
                    if arrSelection[index] as! String == tempId {
                        arrSelection.removeObject(at: index)
                        break
                    }
                    else  {
                        
                    }
                }
            }
            else {
                arrSelection.add(tempId)
            }
        }
        tableUsers.reloadData()
    }

    @IBAction func actionCreateGroup(_ sender: Any)  {
        if byGroupLists == true {
            addMemberOnGroup()
        }
        else {
            if isCheckCreateGroupOrNot == false{
             isCheckCreateGroupOrNot = true
            onCreating()
            }
        }
    }
    
    func addMemberOnGroup() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addMemberOnGroup)"
        print("\(apiURL)")
         UtilityMethods.showIndicator()
        let userID = UserDefault.standard.getCurrentUserId()!
        print(userID)
        
        let strIdUsed = arrSelection.componentsJoined(by: ",")
        
        let newTodo: [String: Any] = ["user_id":strIdUsed,"admin_id":userID,"group_id":idGroup]
        print(newTodo)
        
        Alamofire.request(apiURL, method: .get, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            if((response.result.value) != nil) {
                 UtilityMethods.hideIndicator()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" || swiftyJsonVar["status"].string == "Success" {
                    let alert  = UIAlertController(title: "Success" , message: "User added on your group successfully.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.arrSelection.removeAllObjects()
                        self.allUsersLists()
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                  UtilityMethods.hideIndicator()
            }
        }
    }
    
    func onCreating() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.createGroup)"
        print("\(apiURL)")
         UtilityMethods.showIndicator()
        let userID = UserDefault.standard.getCurrentUserId()!
        let strUserId = arrSelection.componentsJoined(by: ",")
        
        let image = imgGroupicon.image
        let imgData = image!.jpegData(compressionQuality: 0.4)!
        
        let newTodo: [String: Any] = ["user_id":"\(userID)","group_title":strtitle,"group_desc":strDescription,"group_users":strUserId]
        print(newTodo)
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(imgData, withName: "group_icon",fileName: "profile.jpg", mimeType: "image/jpg")
            
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:apiURL)
        {
            (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                   
                    print(response.result.value)
                    
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "Success" {
                         UtilityMethods.hideIndicator()
                        let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            self.isCheckCreateGroupOrNot = false
                            NotificationCenter.default.post(name: NSNotification.Name("changeGroupStatus"), object: nil)
//                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else {
                        self.isCheckCreateGroupOrNot = false
                        UtilityMethods.hideIndicator()
                        if swiftyJsonVar["message"].stringValue == "Mentor-Mi, You have already created one group under free version." {
                            let alert = UIAlertController(title: "Mentor-Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                                
                                let subscribe = self.storyboard?.instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
                                self.present(subscribe, animated: true, completion: nil)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            self.isCheckCreateGroupOrNot = false
                            UtilityMethods.hideIndicator()
                            let alert = UIAlertController(title: "Mentor-Mi", message: swiftyJsonVar["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            case .failure(let encodingError):
                UtilityMethods.hideIndicator()
                self.isCheckCreateGroupOrNot = false
                print(encodingError)
            }
        }
        
    }
    
    //    MARK:- SearchBar Delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if arrUserSearch.count > 0 {
            isSearchActive = true
        }
        else {
            isSearchActive = false
        }
        search.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            
        }
        else {
            search.showsCancelButton = true
            isSearchActive = true
            arrUserSearch.removeAll()
            for index in 0..<arrUserLists.count {
                let dict = arrUserLists[index]
                
                let name = dict["user_first_name"].stringValue
                let r = NSString(string: name).range(of: searchText, options: String.CompareOptions.caseInsensitive)

               // let r: NSRange = (name.string)?.range(of: searchText, options: .caseInsensitive)
                if r.location != NSNotFound {
                    arrUserSearch.append(dict)
                }
            }
            if arrUserSearch.count > 0 {
                
            }
            else {
                if searchText == "" {
                    
                }
            }
        }
        tableUsers.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        search.text = nil
        search.showsCancelButton = false
        search.resignFirstResponder()
        tableUsers.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search.showsCancelButton = false
        search.resignFirstResponder()
    }
    
    @IBAction func actioNback(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryBoard.instantiateViewController(withIdentifier: "GroupsListsVC") as! GroupsListsVC
        let navController = UINavigationController(rootViewController: mainController)
        let rootController = DDMenuController(rootViewController:navController)
        let leftmenuController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        let leftnavController = UINavigationController(rootViewController: leftmenuController)
        leftnavController.isNavigationBarHidden = false
        rootController?.leftViewController = leftnavController;
        appDelegate.menuController = rootController
        appDelegate.menuController?.leftViewController = leftnavController;
        appDelegate.window?.rootViewController = appDelegate.menuController
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    //    Mark :- apiGetProfile
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {

            }
        }
    }
    
    //    Mark :- apiTimeLine
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {

                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = swiftyJsonVar["data"].arrayValue
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                

            }
        }
    }
    
    /*
     ["status": "abc", "user_id": 621]
     {
       "status" : "Failed",
       "data" : [

       ],
       "message" : "Failed to Add !!"
     }
     */
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefault.standard.getStatus()
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusAbtMe]
            

            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {

                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        
                        let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].string, preferredStyle: .alert)

                             let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                                 self.getProfileData()
                             })
                             alert.addAction(ok)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
                        
                       
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
