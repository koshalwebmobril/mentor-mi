//
//  GroupChatsVC.swift
//  Mentor Mi
//
//  Created by WebMobril on 18/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage


class GroupChatsVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    
    
    var dictChatgroup = NSDictionary()
    var arrChat = [JSON]()
    var strGroupId = String()
    var isOn = true
    var arrTimeLine = [JSON]()
    
    @IBOutlet weak var lblgroupLogo: UILabel!
    @IBOutlet weak var imgGroupLogo: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tableGroup: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var viewForTextFiled: UIView!
    
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var textFieldBGViewBottomConstraint: NSLayoutConstraint!
    
    var isCheckMsgSend = false
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
        
        uploadStatus()
        statusTxt.text = ""
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !tableGroup.isDecelerating {
            view.endEditing(true)
        }
    }
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
            }
        }
    }
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = swiftyJsonVar["data"].arrayValue
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
            }
        }
    }
    
    func uploadStatus() {
        
        if statusTxt.text == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefault.standard.getStatus()
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"status":statusTxt.text!]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        let alert = UIAlertController(title: "Success", message: swiftyJsonVar["message"].string, preferredStyle: .alert)

                             let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                                 self.getProfileData()
                             })
                             alert.addAction(ok)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
                       
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    @IBOutlet weak var threedotsinfo: UIView!
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
        
    }
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(dictChatgroup)
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        
        //        self.navigationController?.navigationItem.rightBarButtonItems  = rightBarButtons
        self.navigationItem.rightBarButtonItems = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black

//        navigationController?.navigationBar.isHidden =
        
        strGroupId = String(format: "%@", dictChatgroup.object(forKey: "group_id") as! CVarArg)
//        strGroupId = dictChatgroup.object(forKey: "group_id") as! String
        print(strGroupId)
        tableGroup.estimatedRowHeight = 125
        tableGroup.rowHeight = UITableView.automaticDimension
        
        lblgroupLogo.text = String(format:"%@",dictChatgroup.object(forKey: "group_name") as! CVarArg)
        let iconURL = String(format:"%@", dictChatgroup.object(forKey: "group_icon") as! CVarArg)
        imgGroupLogo.sd_setImage(with: URL(string: iconURL), placeholderImage: UIImage(named: "imgUnSelected"))
        
        imgGroupLogo.layer.cornerRadius = imgGroupLogo.frame.height/2
        imgGroupLogo.clipsToBounds = true
        imgGroupLogo.layer.borderWidth = 3.0
        imgGroupLogo.layer.borderColor = UIColor.clear.cgColor
        
        let tempAdminId = String(format: "%@", dictChatgroup.object(forKey: "admin_id") as! CVarArg)
        let userID = String(format: "%d",UserDefault.standard.getCurrentUserId()!)
        if userID == tempAdminId {
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(GroupChatsVC.Tap1))
            imgGroupLogo.isUserInteractionEnabled = true
            imgGroupLogo.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(GroupChatsVC.Tap1))
            lblgroupLogo.isUserInteractionEnabled = true
            lblgroupLogo.addGestureRecognizer(tap2)
        }
        
        btnSend.layer.cornerRadius = 5.0
        btnSend.clipsToBounds = true
        
        viewForTextFiled.layer.cornerRadius = 2.5
        viewForTextFiled.layer.borderColor = UIColor.darkGray.cgColor
        viewForTextFiled.layer.borderWidth = 2.0
       // txtMessage.placeholder = "  Write Message..."
       txtMessage.attributedPlaceholder = NSAttributedString(string: "  Write Message...",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
//        tableGroup.delegate = self
//        tableGroup.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    @objc func notification() {
        
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }

    @objc func threedots() {
       
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threedotsinfo.isHidden = true
            
            isOn = true
        }
    }

    @objc func Tap1() {
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isOn = true
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        threedotsinfo.isHidden = true
        navigationController?.isNavigationBarHidden = true
        groupChat()
         tabBarController?.tabBar.isHidden = false
    }
    @IBAction func actioNback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func groupChat() {
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.chatList)"
        print("\(apiURL)")
        
        let newTodo: [String: Any] = ["group_id": strGroupId]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    
                    self.arrChat = swiftyJsonVar["data"].arrayValue
                    self.tableGroup.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute: {
                        if self.arrChat.count > 0{
                        let indexPath = IndexPath(row: self.arrChat.count-1, section: 0)
                        self.tableGroup.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                        }
                    })
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {

            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableGroup.dequeueReusableCell(withIdentifier: "cell") as! ChatCell
        
        let iconURL = arrChat[indexPath.row]["profile_pic"].stringValue
        cell.imgChatUser.sd_setImage(with: URL(string: iconURL), placeholderImage: UIImage(named: "imgUnSelected"))
        cell.imgChatUser.layer.cornerRadius = cell.imgChatUser.frame.height/2
        cell.imgChatUser.clipsToBounds = true
        cell.imgChatUser.layer.masksToBounds = true
        
        
        cell.lblDate.text = String(format: "%@", arrChat[indexPath.row]["created_date"].stringValue)
        cell.lblName.text = String(format: "%@ %@", arrChat[indexPath.row]["first_name"].stringValue , arrChat[indexPath.row]["last_name"].stringValue)
        
        cell.lblMasg.text = String(format: "%@", arrChat[indexPath.row]["chat_msg"].stringValue)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UITableView.automaticDimension > 125 {
            return UITableView.automaticDimension
        }
        else {
            return 125
        }
    }
    func handleKeyBoard(){
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    // MARK: - KeyBoard Notification Method
    @objc func keyboardWillHide(notification: NSNotification)
    {      //  self.keyboardIsShowing = false
        textFieldBGViewBottomConstraint.constant=0
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        var keyboardFrame: CGRect = CGRect.null
        if let info = notification.userInfo {
            keyboardFrame = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    textFieldBGViewBottomConstraint.constant = keyboardFrame.size.height
                case 1334:
                    print("iPhone 6/6S/7/8")
                    textFieldBGViewBottomConstraint.constant = keyboardFrame.size.height
                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    textFieldBGViewBottomConstraint.constant = keyboardFrame.size.height
                case 2436:
                    print("iPhone X")
                    textFieldBGViewBottomConstraint.constant = keyboardFrame.size.height - 40
                default:
                    textFieldBGViewBottomConstraint.constant = keyboardFrame.size.height
                }
            }
            self.view.layoutIfNeeded()
           // self.tableViewScrollToBottom(animated: true)
        }
    }
    
    @IBAction func actionSend(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if txtMessage.text == "" {
            let alert  = UIAlertController(title: "Mentor-Mi" , message: "Please enter message", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else  {
            if isCheckMsgSend == false{
               isCheckMsgSend = true
             UtilityMethods.showIndicator()
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.sendMsg)"
            print("\(apiURL)")
            let userID = UserDefault.standard.getCurrentUserId()!
            
            let newTodo: [String: Any] = ["from_id":userID,"group_id":strGroupId,"chat_msg":"\(txtMessage.text ?? "")"]
            print(newTodo)
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    UtilityMethods.hideIndicator()
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        self.isCheckMsgSend = false
                        //self.txtMessage.text = "     Write Message..."
                        self.txtMessage.text = ""
                        self.txtMessage.attributedPlaceholder = NSAttributedString(string: "  Write Message...",
                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
                        self.groupChat()
                        
                    }
                    else {
                        self.isCheckMsgSend = false
                        let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    self.isCheckMsgSend = false
                }
            }
            }
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
