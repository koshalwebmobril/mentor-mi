//
//  LeftMenuVC.swift
//  Mentor_Mi
//
//  Created by Manish on 18/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class LeftMenuVC: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var itemArray = ["Profile","Soapbox","Tutorial","Classes","Groups","Logout"]
    @IBOutlet weak var slideTableView : UITableView!
    
    var logoutSelect = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.navigationController?.isNavigationBarHidden = true
    }
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */



extension LeftMenuVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableFooterView = UIView()
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell0") as! LeftMenuCell
            
            return slideCell
        }
        else
            if indexPath.row == 1 {
                let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell1") as! LeftMenuCell
                
                return slideCell
            }
            else if indexPath.row == 2 {
                let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell2") as! LeftMenuCell
                
                return slideCell
            }
            else if indexPath.row == 3 {
                let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell3") as! LeftMenuCell
                
                return slideCell
            }
            else if indexPath.row == 4 {
                let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell4") as! LeftMenuCell
                
                return slideCell
            }
            else if indexPath.row == 5 {
                let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell5") as! LeftMenuCell
                
                return slideCell
            }
            else {
                let slideCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell6") as! LeftMenuCell
                
                return slideCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
            
        }
        else {
            return 55
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: break
            //appDelegate.goToHome()
        case 1:
            appDelegate.goToProfile()
        case 2 :
            appDelegate.goToSoapBox()
        case 3 :
            appDelegate.goToTutorials()
        case 4 :
            appDelegate.goToClasses()
        case 5 :
            appDelegate.goToGroup()
            
        //
        default:
            
            let actionSheetController = UIAlertController(title: "Mentor-Mi", message: "Sure! Want to logout?", preferredStyle: UIAlertController.Style.actionSheet)
            
            
            let logoutAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (action) -> Void in
                
                if Type.login == "1" {
                    UserDefault.standard.removeUserId()
                }
                if Type.login == "2" {
                    GIDSignIn.sharedInstance().signOut()
                    UserDefault.standard.removeUserId()
                }
                if Type.login == "3" {
                    UserDefault.standard.removeUserId()
                }
                if Type.login == "4" {
                    LoginManager().logOut()
                    UserDefault.standard.removeUserId()
                }
                UserDefault.standard.removeUserId()
                UserDefaults.standard.removeObject(forKey: "Apple")
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                let navigationController = UINavigationController(rootViewController: controller!)
                navigationController.isNavigationBarHidden = true
                self.appDelegate.window?.rootViewController = navigationController
                self.appDelegate.window?.makeKeyAndVisible()
                
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) { (action) -> Void in
            }
            actionSheetController.addAction(logoutAction)
            actionSheetController.addAction(cancelAction)
            present(actionSheetController, animated: true, completion: nil)
            
            
            if let popoverController = actionSheetController.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: 750, width: 20, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
        }
        
    }
}
