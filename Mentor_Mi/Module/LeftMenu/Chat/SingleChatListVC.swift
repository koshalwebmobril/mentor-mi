//
//  SingleChatListVC.swift
//  Mentor_Mi
//
//  Created by Rajeev Sharma on 18/01/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Firebase

class SingleChatListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var messageLabel: UILabel!
    var recentChats:[RecentChats] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.separatorStyle = .none
        
        self.navigationController?.isNavigationBarHidden=true
        tabBarController?.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        tableView.register(UINib.init(nibName: "chatListTableViewCell", bundle: nil), forCellReuseIdentifier: "chatListCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true)
            FireBaseManager.defaultManager.recentChatsManagerDelegate = self
            //FireBaseManager.defaultManager.recentChats = []
            FireBaseManager.defaultManager.fetchRecentChats()
            self.observeRecentUpdates()
        }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func observeRecentUpdates() {
        let studentId = UserDefault.standard.getCurrentUserId() ?? 0

        Database.database().reference().child("Recents").queryOrdered(byChild: "receiver_id").queryEqual(toValue: "\(studentId)").observe(.childChanged, with: { (snapshot) -> Void in
            
            DispatchQueue.main.async {
                let msgData = snapshot.value as! Dictionary<String, AnyObject>
                let key = snapshot.key
                let reqIndex = self.recentChats.firstIndex { (message) -> Bool in
                    return message.node == key
                }
                guard let index = reqIndex else {return}
                self.recentChats[index].lastMsg = msgData["last_message"] as? String ?? ""
                let indexPath = IndexPath(row: reqIndex!, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        })
        
        Database.database().reference().child("Recents").queryOrdered(byChild: "sender_id").queryEqual(toValue: "\(studentId)").observe(.childChanged, with: { (snapshot) -> Void in
            
            DispatchQueue.main.async {
                let msgData = snapshot.value as! Dictionary<String, AnyObject>
                let key = snapshot.key
                let reqIndex = self.recentChats.firstIndex { (message) -> Bool in
                    return message.node == key
                }
                guard let index = reqIndex else {return}
                
                self.recentChats[index].lastMsg = msgData["last_message"] as? String ?? ""
                self.recentChats[index].activeNow = msgData["active_now"] as? String ?? ""
                self.recentChats[index].senderName = msgData["sender_name"] as? String ?? ""
                self.recentChats[index].senderImg = msgData["sender_image"] as? String ?? ""
                self.recentChats[index].receiverName = msgData["receiver_name"] as? String ?? ""
                self.recentChats[index].receiverImage = msgData["receiver_image"] as? String ?? ""
                
                let indexPath = IndexPath(row: reqIndex!, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        })
        
        Database.database().reference().child("Recents").queryOrdered(byChild: "receiver_id").queryEqual(toValue: "\(studentId)").observe(.childChanged, with: { (snapshot) -> Void in
            
            DispatchQueue.main.async {
                let msgData = snapshot.value as! Dictionary<String, AnyObject>
                let key = snapshot.key
                let reqIndex = self.recentChats.firstIndex { (message) -> Bool in
                    return message.node == key
                }
                guard let index = reqIndex else {return}
                
                self.recentChats[index].lastMsg = msgData["last_message"] as? String ?? ""
                self.recentChats[index].activeNow = msgData["active_now"] as? String ?? ""
                self.recentChats[index].senderName = msgData["sender_name"] as? String ?? ""
                self.recentChats[index].senderImg = msgData["sender_image"] as? String ?? ""
                self.recentChats[index].receiverName = msgData["receiver_name"] as? String ?? ""
                self.recentChats[index].receiverImage = msgData["receiver_image"] as? String ?? ""
                
                let indexPath = IndexPath(row: reqIndex!, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        })
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        self.messageLabel.isHidden=true
        
        if let customCell = tableView.dequeueReusableCell(withIdentifier: "chatListCell") as? chatListTableViewCell{
            customCell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let studentId = UserDefault.standard.getCurrentUserId() ?? 0
            
            let recentChat = recentChats[indexPath.row]
                      
            if recentChat.senderId != "\(studentId)"{
                customCell.userNameLabel.text = recentChat.senderName
                if let url = URL(string: recentChat.senderImg){
                    customCell.userImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "Avatar"))
                }else{
                    customCell.userImageView.image = UIImage(named: "Avatar")
                }
            }else{
                customCell.userNameLabel.text = recentChat.receiverName
                if let url = URL(string: recentChat.receiverImage){
                    customCell.userImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "Avatar"))
                }else{
                    customCell.userImageView.image = UIImage(named: "Avatar")
                }
            }
            
            customCell.unreadCountView.backgroundColor = recentChat.activeNow == "true" ? .red : .red
            customCell.subTitleMessageLabel.text = recentChat.lastMsg
            
            cell = customCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentChatView = UIStoryboard.singleChat
        let studentId = UserDefault.standard.getCurrentUserId() ?? 0

        let recent = recentChats[indexPath.row]
        if recentChats[indexPath.row].senderId != "\(studentId)"{
            let friend = ChatFriend(id: recent.senderId , name: recent.senderName, image: recent.senderImg)
            currentChatView.friend = friend
            currentChatView.toId = recent.senderId
        }else{
            let friend = ChatFriend(id: recent.receiverId, name: recent.receiverName, image: recent.receiverImage)
            currentChatView.friend = friend
            currentChatView.toId = recent.receiverId
        }
        
        self.navigationController?.pushViewController(currentChatView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }

}
extension SingleChatListVC : FirebaseRecentChatsManagerDelegate {
    
    func fireBaseManagerDidCompleteFetchingRecentChats() {
        recentChats = FireBaseManager.defaultManager.recentChats
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
