//
//  SIngleChatCell.swift
//  
//
//  Created by Manish on 18/10/19.
//

import UIKit

class SIngleChatCell: UITableViewCell {
    
    @IBOutlet weak var chatMessages: UILabel!
    @IBOutlet weak var bgChatCell: UIView!
    @IBOutlet weak var lbldateChat: UILabel!
    @IBOutlet weak var imgBubble: UIImageView!
    @IBOutlet weak var lblReceive: UILabel!
    @IBOutlet weak var bgView1: UIView!
    @IBOutlet weak var bgView2: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
