//
//  chatListTableViewCell.swift
//  Bizzalley
//
//  Created by Anuj Jha on 08/03/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit

class chatListTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var subTitleMessageLabel: UILabel!
    @IBOutlet weak var unreadCountView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellView.layer.cornerRadius = 5
        cellView.layer.borderColor = UIColor(displayP3Red: 208/255, green: 211/255, blue: 212/255, alpha: 0.8).cgColor
        
        unreadCountView.layer.cornerRadius = unreadCountView.frame.size.width/2
        unreadCountView.clipsToBounds = true
        unreadCountView.layer.masksToBounds = true
        //unreadCountView.isHidden = true
        
        cellView.clipsToBounds = true
        cellView.layer.masksToBounds = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
