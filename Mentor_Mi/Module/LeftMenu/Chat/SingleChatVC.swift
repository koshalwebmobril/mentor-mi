//
//  SingleChatVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MessageKit

class SingleChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate,  UIGestureRecognizerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as!AppDelegate
    var appConstants: AppConstants = AppConstants()
    var isOn = false
    var toId = String()
    var arrMsgs = [JSON]()
    var arrMsgId = NSMutableArray()
    var friend: ChatFriend?
    var messages:[Message] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var statusVC: UIView!    
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var threedotsinfo: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        FireBaseManager.defaultManager.messageManagerDelegate = self
        FireBaseManager.defaultManager.messages = []
        FireBaseManager.defaultManager.fetchMessages(node: getMsgNode())
        setUI()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        let imageView=UIImageView(image:UIImage(named: "bg_chat"))
        tableView.backgroundView=imageView
        
        nameLabel.text = friend?.name
        
        commentTextView.delegate = self
        
        //setupLongPressGesture()
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.tableView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                
                let cell: SIngleChatCell = self.tableView.cellForRow(at: IndexPath.init(row: indexPath.row, section: 0)) as! SIngleChatCell
                let tempId = (arrMsgs[indexPath.row]["id"]).stringValue
                
                if arrMsgId.contains(tempId) {
                    arrMsgId.remove(tempId)
                }
                else {
                    arrMsgId.add(tempId)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       // setNavigationUI()
        self.navigationController?.isNavigationBarHidden = true
        tabBarController?.navigationController?.isNavigationBarHidden = true
        //getAllChats(myId: UserDefault.standard.getCurrentUserId()!, toId: toId)
    }
    
    func setNavigationUI() {
        isOn = false
        threedotsinfo.isHidden = true
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        tabBarController?.tabBar.isHidden = false
        tabBarController?.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.title = "Messages"
        tabBarController?.tabBar.isHidden = false
    }
    
    @objc func notification() {
        self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    func getAllChats(myId: Int, toId: String) {
        let parameters = ["from_id": myId, "to_id" : toId] as [String:Any]
        UtilityMethods.showIndicator()
        APIManager.sharedInstance.particularUserChat(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleChatResponse(response: response)
        }
    }
    
    func handleChatResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.arrMsgs = tempResponse["data"].arrayValue
            self.tableView.reloadData()
        }
    }
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
//        statusView.isHidden = false
//        statusView.layer.borderWidth = 2
//        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    
    
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    
    @IBAction func actionDelete(_ sender: Any) {
//        let strMsgId = arrMsgId.componentsJoined(by: ",")
//        let parameters = ["msg_id": strMsgId]
//        UtilityMethods.showIndicator()
//        APIManager.sharedInstance.particularUserChat(parameters: parameters) { (success, response) in
//            UtilityMethods.hideIndicator()
//            guard let response = response else{ return }
//            self.handleResponse(response: response)
//        }
    }
    
    func handleResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
           getAllChats(myId: UserDefault.standard.getCurrentUserId()!, toId: toId)
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var returnCell:UITableViewCell = UITableViewCell()
        
        if indexPath.section == 0 {
            let userId = String(format: "%d", UserDefault.standard.getCurrentUserId()!)
            
//            if (arrMsgs[indexPath.row]["to_id"]).stringValue == userId {
            if messages[indexPath.row].userId != userId {
                let cell:SIngleChatCell = self.tableView.dequeueReusableCell(withIdentifier: "cell2") as UITableViewCell? as! SIngleChatCell
                
                cell.lblReceive.text = messages[indexPath.row].message//(arrMsgs[indexPath.row]["chat_msg"]).stringValue
                cell.bgView2.layer.cornerRadius = 2.5
                cell.bgView2.clipsToBounds = true
                //cell.bgView2.backgroundColor = UIColor(red: 69/255, green: 214/255, blue: 93/255, alpha: 1.0)
                cell.bgView2.roundReceiverCorners(radius: 10)
//                let msgId = (arrMsgs[indexPath.row]["id"]).stringValue
//                if arrMsgId.contains(msgId) {
//                    cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
//                }
//                else {
//                    cell.backgroundColor = UIColor.white
//                }
                
                returnCell = cell
            }
            else {
                
                let cell:SIngleChatCell = self.tableView.dequeueReusableCell(withIdentifier: "cell1") as UITableViewCell? as! SIngleChatCell
                cell.chatMessages.text = messages[indexPath.row].message//(arrMsgs[indexPath.row]["chat_msg"]).stringValue
                cell.bgView1.layer.cornerRadius = 2.5
                cell.bgView1.clipsToBounds = true
              //  cell.bgView1.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 235/255, alpha: 1.0)
                cell.bgView1.roundSenderCorners(radius: 10)
//                let msgId = (arrMsgs[indexPath.row]["id"]).stringValue
//                if arrMsgId.contains(msgId) {
//                    cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
//                }
//                else {
//                    cell.backgroundColor = UIColor.white
//                }
                
                returnCell = cell
            }
        }
        return returnCell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UITableView.automaticDimension > 50 {
            print(UITableView.automaticDimension)
            return UITableView.automaticDimension
        }
        else {
            
            print(UITableView.automaticDimension)
            return UITableView.automaticDimension
            
        }
    }
    
    //MARK:- Create Node.
    func getMsgNode() -> String{
        
        var nodeStr = ""
        
        let num1 = Int(self.toId)
        let num2 = UserDefault.standard.getCurrentUserId() ?? 0
        
        if(num1!>num2){
            nodeStr = "\(num1!)_\(num2)"
        }else{
            nodeStr = "\(num2)_\(num1!)"
        }
        return nodeStr
    }
    
    @IBAction func actionSend(_ sender: Any) {
        if checkValidation() {
            guard let friend = self.friend else {return}
            self.view.endEditing(true)
            
            
            
            let messageDict = ["message":commentTextView.text!, "messageStatus":"sent", "user":UserDefault.standard.getCurrentUserName() ?? "", "userId":"\(UserDefault.standard.getCurrentUserId() ?? 0)"]
            
            let recentDict = [
                "active_now":"true",
                "sender_id":"\(UserDefault.standard.getCurrentUserId() ?? 0)",
                "sender_image":UserDefault.standard.getCurrentUserImage() ?? "",
                "sender_name":UserDefault.standard.getCurrentUserName() ?? "",
                "last_message":commentTextView.text!,
                "receiver_id":friend.id ,
                "receiver_image":friend.image ,
                "receiver_name":friend.name ,]
            
            FireBaseManager.defaultManager.sendNewMsg(atNode: getMsgNode(), detailDict: messageDict, recentDict: recentDict)
            
            
            commentTextView.text=""
            placeholderLabel.isHidden = false
            
            //messageSend(by: UserDefault.standard.getCurrentUserId()!,to: toId, chat_msg: commentTextView.text!)
        }
    }
    
    func messageSend(by: Int,to: String, chat_msg: String) {
        let parameters = ["from_id": by, "to_id" : to, "chat_msg" :chat_msg] as [String:Any]
        UtilityMethods.showIndicator()
        APIManager.sharedInstance.chatStart(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleChatSendResponse(response: response)
        }
    }
    
    func handleChatSendResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.commentTextView.text = ""
            self.placeholderLabel.isHidden = false
            getAllChats(myId: UserDefault.standard.getCurrentUserId()!, toId: toId)
        }
    }
    
    func checkValidation() -> Bool {
        if commentTextView.text == "" {
            self.commentTextView.becomeFirstResponder()
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorNoTextMsgs, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])

            return false
        }
        return true
    }
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholderLabel.isHidden = true
        print("print1")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            
            placeholderLabel.isHidden = false
        }
        print("print2")
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewShouldReturn(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func scrollToBottom(){
        if arrMsgs.count > 0
        {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.arrMsgs.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SingleChatVC : FirebaseMessageManagerDelegate {
    func fireBaseManagerDidCompleteFetchingMessages() {
        messages = FireBaseManager.defaultManager.messages
        tableView.reloadData()
    }
    
}
extension UIView {
    func roundReceiverCorners(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
    }
    
    func roundSenderCorners(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
    }
}
