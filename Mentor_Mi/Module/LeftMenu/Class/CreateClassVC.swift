//
//  CreateClassVC.swift
//  Mentor_Mi
//
//  Created by Manish on 14/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SkyFloatingLabelTextField
import SVProgressHUD

class CreateClassVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate  {
    
    var categories = [ImageCategory]()
    var collectionClassVideos : UICollectionView?
    let headerReuseId = "TableHeaderViewReuseId"
    var isOn = true
    
    var arrClass = [JSON]()
    var arrTutorial = [JSON]()
    let appConstant:AppConstants = AppConstants()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var arrTimeLine = [JSON]()
    
    @IBOutlet weak var createClassView: UIView!
    @IBOutlet weak var tableClassCreate: UITableView!
    @IBOutlet weak var bgDescriptionView: UIView!
    @IBOutlet weak var txtNameClass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDescription: SkyFloatingLabelTextField!
    @IBOutlet weak var btnClassCreate: UIButton!
    @IBOutlet weak var threedotsinfo: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    var fromWhere:String = ""
    @IBOutlet weak var topNavbarView: UIView!
    
    @IBOutlet weak var createClasslbl: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }

        
        setNavigationUI()
        
        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI() {

        createClassView.isHidden = true
        bgDescriptionView.isHidden = true
        btnClassCreate.layer.cornerRadius = 2.5
        btnClassCreate.clipsToBounds = true
        bgDescriptionView.layer.cornerRadius = 20.0
//        bgDescriptionView.backgroundColor = .white
        bgDescriptionView.layer.borderWidth = 1.0
        bgDescriptionView.layer.borderColor = UIColor.black.cgColor

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        

        self.navigationController?.isNavigationBarHidden = false
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
        
        if fromWhere != "menuBar"{
            
            
            self.navigationController?.isNavigationBarHidden = true
            
            
        }
        
//
//
//
//        }
//        else{
//
//
//            //topNavbarView.isHidden = true
//
//           // topNavbarView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
//
//        }
        
        
        
        getMyClassServiceMethod()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationUI() {
        self.title = "Classes"
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.navigationItem.rightBarButtonItems = rightBarButtons
        navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        tabBarController?.navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = false
//        navigationController?.navigationBar.isHidden = false
        tabBarController?.navigationController?.isNavigationBarHidden = false
        
    }
    
    @objc func notification() {
       self.navigationController?.pushViewController(UIStoryboard.notification, animated: true)
    }
    
    @objc func threedots() {
        if isOn == true {
            threedotsinfo.isHidden = false
            threedotsinfo.layer.borderWidth = 2
            threedotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
        else{
            threedotsinfo.isHidden = true
            isOn = true
        }
    }
    
    
    @IBAction func status(_ sender: Any) {
        threedotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    
    @IBAction func premium(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.premium, animated: true)
    }
    @IBAction func settings(_ sender: Any) {
        threedotsinfo.isHidden = true
        self.navigationController?.pushViewController(UIStoryboard.setting, animated: true)
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        
       
            tableClassCreate.isHidden = true
            bgDescriptionView.isHidden = false
            createClassView.isHidden = false
        
        
//            self.title = "Create Class"
//            isOn = true
//            let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
//            let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
//            let rightBarButtons = [threeDots,notify]
//            self.navigationController?.navigationItem.rightBarButtonItems  = rightBarButtons
//            notify.tintColor = UIColor.black
//            threeDots.tintColor = UIColor.black
//            threedotsinfo.isHidden = true
//            
//            tabBarController?.navigationController?.isNavigationBarHidden = false
//            navigationController?.navigationBar.barTintColor = appConstant.navigationColor
//            navigationController?.isNavigationBarHidden = false
//            tabBarController?.tabBar.isHidden = true
       
    }
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    
    @IBAction func ok(_ sender: Any) {
       
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
             statusView.isHidden = true
            uploadStatus(text: statusTxt.text!, id: UserDefault.standard.getCurrentUserId()!)
        }
//        statusTxt.text = ""
    }
    
    func uploadStatus(text: String, id: Int) {
        let parameters = ["user_id" : id, "status":text] as [String : Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.uploadStatus(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleStatusUploadResponse(response: response)
        }
    }
    
    func handleStatusUploadResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            UserDefault.standard.setStatus(type: statusTxt.text!)
            getProfile(userId: UserDefault.standard.getCurrentUserId()!)
        }
    }
    
    
    func getProfile(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getUserDetails(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleUserDataResponse(response: response)
        }
    }
    
    func handleUserDataResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            myData.myDetails = tempResponse["data"]
            getTimeLine(userId: UserDefault.standard.getCurrentUserId()!)
        }
    }
    
    func getTimeLine(userId: Int) {
        let parameters = ["user_id" : userId]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.getTimeLine(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTimeLineResponse(response: response)
        }
    }
    
    
    func handleTimeLineResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            arrTimeLine = tempResponse["data"].arrayValue
            
        }
    }
    
    @IBAction func actionCreateClss(_ sender: Any) {
        if checkValidation() {
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.createClass)"
            print("\(apiURL)")
            
            let userID = UserDefault.standard.getCurrentUserId()!
            let newTodo: [String: Any] = ["user_id": userID,"class_name":"\(txtNameClass.text ?? "ABCD")","class_accessibility":"1"]
            
            SVProgressHUD.show()
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {
                    SVProgressHUD.dismiss()
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        let responseJson = swiftyJsonVar["data"]
                        print(responseJson)

//                        self.dismiss(animated: false, completion: nil)
                        self.navigationController?.pushViewController(UIStoryboard.uploadVideo, animated: true)
                        
                    }
                    else {
                        let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {

                    SVProgressHUD.dismiss()

                    let alert  = UIAlertController(title: "Mentor-Mi" , message: "Please try again.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    
    func checkValidation() -> Bool {
        if txtNameClass.text == "" {
            
            let alert  = UIAlertController(title: "Mentor-Mi" , message: "Please enter class name.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
        else if txtDescription.text == "" {

            let alert  = UIAlertController(title: "Mentor-Mi" , message: "Please add description.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
        return true
    }
    
    
//  MARK:Tableview Delegates and Datasource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrClass.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        if cell == nil {
            
        }
        
        cell?.txtClassName.text = String(format:"%@", arrClass[indexPath.row]["class_name"].stringValue)
        arrTutorial = arrClass[indexPath.row]["tutorials"].arrayValue
        cell?.collectView.tag = indexPath.row
        cell?.bgCollection.backgroundColor = UIColor.white
//        cell?.bgCollection.layer.cornerRadius = 5.0
        cell?.bgCollection.layer.borderColor = UIColor.lightGray.cgColor
        cell?.bgCollection.clipsToBounds = true
        
        cell?.bgCollection.layer.cornerRadius = 5
        cell?.bgCollection.layer.shadowColor = UIColor.gray.cgColor
        cell?.bgCollection.layer.shadowOpacity = 1
        cell?.bgCollection.layer.shadowOffset = CGSize.zero
        cell?.bgCollection.layer.shadowRadius = 2.5
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTutorial.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InsideCollectionViewCell", for: indexPath) as! InsideCollectionViewCell
        
//        cell.bgView.layer.masksToBounds = true
//        cell.bgView.layer.cornerRadius = 5
//        cell.bgView.layer.borderColor = UIColor.black.cgColor
//        cell.bgView.layer.borderWidth = 2.0
//        cell.bgView.clipsToBounds = true
        
        let tempPostId = String(format:"%@", arrClass[collectionView.tag]["tutorials"][indexPath.row]["postid"].stringValue)
        print(tempPostId)
        let temp = String(format:"%@", arrClass[collectionView.tag]["tutorials"][indexPath.row]["video_thumbnail"].stringValue)
        cell.imgTutorial.sd_setImage(with: URL(string: temp), placeholderImage: UIImage(named: "trending3"))
        
        cell.imgTutorial.layer.borderColor = UIColor.black.cgColor
        cell.imgTutorial.layer.borderWidth = 1.0
        cell.imgTutorial.clipsToBounds = true
        cell.imgTutorial.layer.cornerRadius = 5
        //cell.imgTutorial.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let controller = UIStoryboard.playerThumbnail
        controller.dictTutorial = arrClass[collectionView.tag]["tutorials"][indexPath.row]
        controller.fromWhere = "Class"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateClassVC{
    
    func getMyClassServiceMethod(){
        
        let userId = UserDefault.standard.getCurrentUserId() ?? 0
        let parameters = ["user_id" : "\(userId)"]
               print(parameters)
               
               UtilityMethods.showIndicator()
               
               APIManager.sharedInstance.viewTutorialbyClass(parameters: parameters) { (success, response) in
                   UtilityMethods.hideIndicator()
                   guard let response = response else{ return }
                   self.handleMyClassResponse(response: response)
               }
        
    }
    
    func handleMyClassResponse(response: [String : Any]) {
        var tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.arrClass = tempResponse["data"].arrayValue
            
            self.tableClassCreate.reloadData()
            
        }
    }
    
}
