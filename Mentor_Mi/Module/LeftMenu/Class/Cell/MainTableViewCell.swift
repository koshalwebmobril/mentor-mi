//
//  MainTableViewCell.swift
//  Mentor_Mi
//
//  Created by Manish on 15/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var bgCollection: UIView!
    @IBOutlet weak var txtClassName: UILabel!
    @IBOutlet weak var collectView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension MainTableViewCell{
    
    func setCollectonViewDataourceDelegate
        <D:UICollectionViewDelegate & UICollectionViewDataSource > (_ dataSourceDelegate: D, forRow row:Int)
    {
        collectView.delegate = dataSourceDelegate
        collectView.dataSource = dataSourceDelegate
        
        collectView.reloadData()
    }
}
