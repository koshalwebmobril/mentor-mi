//
//  ForgetVC.swift
//  Mentor_Mi
//
//  Created by Manish on 19/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class ForgetVC: UIViewController {
    @IBOutlet weak var txtMail: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func setUI() {
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.clipsToBounds = true
    }
 
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if checkValidation() {
            self.forget(email: txtMail.text!)
        }
    }
    
    func checkValidation() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if txtMail.text == "" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmail, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            
            return false
        }else if  emailTest.evaluate(with: txtMail.text) == false{
                    UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmailFormat, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
                   
                   return false
               }
        return true
    }
    
    func forget(email: String) {
        let parameters = ["email": email] as [String : Any]
        print(parameters)
        UtilityMethods.showIndicator()
        APIManager.sharedInstance.forget(parameterss: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            
            
           
            guard let response = response else{ return }
            
            
            self.handleForgetResponse(response: response)
        }
    }
    
    func handleForgetResponse(response : [String : Any]) {
        
        if let status = response["status"] as? String{
            
            if status == "Failed"{
                if let message = response["message"] as? String{
                UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: message, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
                }
                
            }else {
                print(response)
                
                let alert = UIAlertController(title: StringConstants.Alert.Titles.Success, message: "Password reset link has been sent to your email.", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: StringConstants.Alert.Actions.Okay, style: .default) { (UIAlertAction) in
                    self.dismiss(animated: false, completion: nil)
                }
                
                alert.addAction(okAction)
                self.present(alert, animated: true, completion:nil )
               
            }
        }
       
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
