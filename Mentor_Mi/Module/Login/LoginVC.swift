//
//  LoginVC.swift
//  Mentor_Mi
//
//  Created by Manish on 17/09/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GoogleSignIn
//   import TwitterKit
import SwiftyJSON
import FBSDKLoginKit
import FBSDKCoreKit
import AuthenticationServices
import Alamofire
import SwiftyJSON
import SVProgressHUD

class LoginVC: UIViewController, GIDSignInDelegate,ASAuthorizationControllerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    @IBOutlet weak var txtEMail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnGoggle: UIButton!
    @IBOutlet weak var eyeButton: UIButton!
    
    @IBOutlet weak var loginProviderStackView: UIView!
    var guserId = String()
    var gEmail = String()
    var gImageUrl1 = String()
    var gdisplayName = String()
    var ggivenName = String()
    var gImageURL = URL(string: "")
    var gFirstName = String()
    var gLastName = String()
    
    var fbId = String()
    var fbEmail = String()
    var fbUsername = String()
    var fbFullname = String()
    var fbFirstName = String()
    var fbLastName = String()
    
    var twtId = String()
    var twtEmail = String()
    var twtDisplayName = String()
    var twtImageUrl = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
         if #available(iOS 13.0, *) {
         let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
         
             statusBar.backgroundColor = .appDefaultColor
         UIApplication.shared.keyWindow?.addSubview(statusBar)
         } else {
             
             if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                 // my stuff
                 statusBar.backgroundColor = .appDefaultColor
             }

         }
        
        GIDSignIn.sharedInstance().clientID = "969391961401-aq6lci2jovd4sh14539gc38odlkht9bh.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
            authorizationButton.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 90, height: 40)
            
            loginProviderStackView.addSubview(authorizationButton)
        } else {
           
        }
       
        
        setUI()

    }
    
    @objc
    func handleAuthorizationAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self as? ASAuthorizationControllerPresentationContextProviding
        authorizationController.performRequests()
    }
    
//    @objc private func handleLogInWithAppleIDButtonPress() {
//        if #available(iOS 13.0, *) {
//            let appleIDProvider = ASAuthorizationAppleIDProvider()
//            let request = appleIDProvider.createRequest()
//            request.requestedScopes = [.fullName, .email ]
//            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//            authorizationController.delegate = self
//            authorizationController.presentationContextProvider = self as? ASAuthorizationControllerPresentationContextProviding
//            authorizationController.performRequests()
//        } else {
//
//            print("login with apple.....")
//
//        }
//
//
//
//    }
    
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        
        if #available(iOS 13.0, *) {
            let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
            let authorizationController = ASAuthorizationController(authorizationRequests: requests)
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self as? ASAuthorizationControllerPresentationContextProviding
                   authorizationController.performRequests()
        } else {
            
             print("login already with apple.....")
        }
        
        
    }
    
    @available(iOS 13.0, *)
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
                        let userLastName = appleIDCredential.fullName?.familyName
                        let userEmail = appleIDCredential.email
            
            loginWithApple(userId: userIdentifier, firstname: userFirstName ?? "", lastname: userLastName ?? "", email: userEmail ?? "")
        
        
        case let passwordCredential as ASPasswordCredential:
        
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
           print("username:\(username) and password:\(password)")
           
            
        default:
            break
        }
    }

    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
//    UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: "Apple authorization failed.", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        
    print("Apple sign in error")
    }
    
    @IBAction func eyeButtonAction(_ sender: Any) {
        
        if txtPassword.isSecureTextEntry == true {
            txtPassword.isSecureTextEntry = false
            eyeButton.isSelected = true
        } else {
            txtPassword.isSecureTextEntry = true
            eyeButton.isSelected = false
        }
    }
    func setUI() {
        btnLogin.layer.cornerRadius = 5.0
        btnLogin.clipsToBounds = true
        btnPhone.layer.cornerRadius = 5.0
        btnPhone.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    
    // MARK: - actionButton
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        self.view.endEditing(true)
        if checkValidation() {
             self.login(email: txtEMail.text!, password: txtPassword.text!)
        }
    }
    
    @IBAction func actionPhone(_ sender: Any) {
        
       
        let alert  = UIAlertController(title: "Mentor-Mi" , message: "we will update soon.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionForgetPassword(_ sender: Any) {
        let VC=self.storyboard?.instantiateViewController(withIdentifier: "ForgetVC") as! ForgetVC
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        
        let VC=self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        VC.modalPresentationStyle = .fullScreen
        self.present(VC, animated: true, completion: nil)
    }
    
    @IBAction func actionFaceBook(_ sender: Any) {
        
        let fbLoginManager : LoginManager = LoginManager()
                fbLoginManager.logIn(permissions: ["email"], from: self, handler: { (result, error) -> Void in
        //        fbLoginManager.logIn(withReadPermissions: ["email"], handler: { (result, error) -> Void in
                    
                    print("\n\n result: \(String(describing: result))")
                    print("\n\n Error: \(String(describing: error))")
                    
                    if (error == nil)
                    {
                        let fbloginresult : LoginManagerLoginResult = result!
                        
                        if(fbloginresult.isCancelled) {
                            //Show Cancel alert
                        } else if(fbloginresult.grantedPermissions.contains("email")) {
                           self.getFBUserData()
                            
                            //fbLoginManager.logOut()
                        }
                    }
                })
        
        
        
        
//        let fbLoginManager : LoginManager = LoginManager()
//        fbLoginManager.loginBehavior = LoginBehavior.browser
//
//        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
//            if (error == nil){
//                let fbloginresult : LoginManagerLoginResult = result!
//                // if user cancel the login
//
//                if (result?.isCancelled)!{
//                    return
//                }
//                if(fbloginresult.grantedPermissions.contains("email")) {
//                    self.getFBUserData()
//                    fbLoginManager.logOut()
//                }
//            }
//            else {
//                print("Error : \(error.debugDescription)")
//            }
//        }
    }
    
    @IBAction func actionTwiiter(_ sender: Any) {
        
        UIAlertController.showAlert(withTitle: "Mentor-Mi", message: "Coming soon..", style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        
//        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
//            if let unwrappedSession = session {
//                self.twtId = unwrappedSession.userID
//                self.twtDisplayName = unwrappedSession.userName
//
//                let twitterClient = TWTRAPIClient(userID: session?.userID)
//                twitterClient.loadUser(withID: (session?.userID)!, completion: { (user, error) in
//                    if let user = user{
//                    self.twtImageUrl = user.profileImageURL
//
//                    }
//                })
//
//                let client = TWTRAPIClient.withCurrentUser()
//                client.requestEmail { email, error in
//                    if (email != nil) {
//                        self.twtEmail = email!
//                        self.twtLogin(id: self.twtId, email : self.twtEmail, name : self.twtDisplayName, image: self.twtImageUrl)
//
//                    } else {
//                        print("error: \(error?.localizedDescription ?? "")");
//                        print(error.debugDescription)
//                    }
//                }
//            } else {
//                NSLog("Login error: %@", error!.localizedDescription);
//            }
//        })
    }
    
    @IBAction func actionGMail(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()

    }
    
    
//  MARK:- checkValidation

    func checkValidation() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)

        if txtEMail.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmail, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if emailTest.evaluate(with: txtEMail.text) == false {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorEmailFormat, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        else if txtPassword.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: StringConstants.Alert.Messages.ErrorPassword, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        return true
    }
    
    
//  MARK:- Login API.
   
    func login(email: String, password: String){
        let parameters = ["username": email,
                          "password": password,
                          "device_type" : "2",
                          "device_tokken" : UserDefault.standard.getCurrentDeviceToken()

            ] as [String:Any]
       if UIScreen.main.bounds.width > 375{
         SVProgressHUD.show(withStatus: "Loading...")
         }
        else{
         UtilityMethods.showIndicator()
         }
        APIManager.sharedInstance.login(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            if UIScreen.main.bounds.width > 375{
                SVProgressHUD.dismiss()
            }
            guard let response = response else{ return }
            self.handleLoginResponse(response: response)
        }
    }
    
    func handleLoginResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
            
            UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)
            Type.login = "1"
            appDelegate.goToHome()
        }
    }
    
    
    // MARK: - GoogleAction
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let err = error {
            print(err)
        }
        else {
            // Perform any operations on signed in user here.
            if user.profile.hasImage {
                let pic = user.profile.imageURL(withDimension: 100)
                print(pic ?? "")
                gImageURL = user.profile.imageURL(withDimension: 100)
            }
            guserId = user.userID
            gdisplayName = user.profile.name
            ggivenName = user.profile.givenName
            gEmail = user.profile.email
            
            googleLogin(userId: guserId, email: gEmail, name: gdisplayName, imageUrl: gImageURL!, displayName: ggivenName)
        }
    }
    
    func googleLogin(userId: String, email: String, name: String, imageUrl: URL, displayName: String) {
        let parameters = ["guserId": userId, "email": email, "displayName" : name, "imageUrl" : imageUrl,"device_tokken": "1234", "device_type":"2"] as [String: Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.googleLogin(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handlegoogleLoginResponse(response: response)
        }
        
    }
    
    func handlegoogleLoginResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            print("Google : \(tempResponse["data"])")

            if tempResponse["data"]["flag"].string == "1" {
               Type.login = "2"
                 UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
               UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)

               appDelegate.goToHome()
            }
            else {
                 UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
                UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)

                                let vc = UIStoryboard.skills
                                                          vc.modalPresentationStyle = .fullScreen
                                                          self.present(vc, animated: true, completion: nil)
                
            }
        }
    }
    
    
    private func signIn(_ signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!) {
        
    }
    
    func twtLogin(id: String, email : String, name : String, image: String) {
        let parameters = ["twitterId": id, "email": email, "displayName" : name, "imageUrl" : image, "device_type":"2"] as [String: Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.twitterLogin(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleTwitterLoginResponse(response: response)
        }
    }
    
    func handleTwitterLoginResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            print("Twitter : \(tempResponse["data"])")
            if tempResponse["data"]["flag"].string == "1" {
                  UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
                UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)
                Type.login = "3"
                appDelegate.goToHome()
                
            }
            else {
                  UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
                UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)
                let vc = UIStoryboard.skills
                                          vc.modalPresentationStyle = .fullScreen
                                          self.present(vc, animated: true, completion: nil)
            }
        }
    }
   
    
    func getFBUserData() {
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result)
                    let swiftyJsonVar = JSON(result!)
                    print(swiftyJsonVar)
                    self.fbId = swiftyJsonVar["id"].stringValue
                    self.fbEmail = swiftyJsonVar["email"].stringValue
                    let firstName = swiftyJsonVar["first_name"].stringValue
                    let lastName = swiftyJsonVar["last_name"].stringValue
                    
                    self.fbFullname = "\(firstName) \(lastName)"
                    self.fbUsername = swiftyJsonVar["first_name"].stringValue
                    
                    self.fbLogin(id: self.fbId, mail: self.fbEmail, name: self.fbUsername, fullName : self.fbFullname)
                }
            })
        }
    }
    
    func fbLogin(id: String, mail: String, name: String, fullName : String) {
        let parameters = ["fb_id": id, "email": mail, "username" : name, "fullname" : fullName, "device_type":"2"] as [String: Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.facebookLogin(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleFacebookLoginResponse(response: response)
        }
    }
    
    func handleFacebookLoginResponse(response : [String : Any]) {
        let tempResponse = JSON(response)
        if tempResponse["status"].stringValue == "Failed" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: tempResponse["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            print("FB : \(tempResponse["data"])")

            if tempResponse["data"]["flag"].string == "1" {
                Type.login = "4"
                 UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
                UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)

                appDelegate.goToHome()
                
            }
            else {
                 UserDefaults.standard.set(tempResponse["data"]["user_id"].stringValue, forKey: "UserID")
                UserDefault.standard.setCurrentUserId(userId: tempResponse["data"]["user_id"].intValue)

                let vc = UIStoryboard.skills
                                          vc.modalPresentationStyle = .fullScreen
                                          self.present(vc, animated: true, completion: nil)
                
            }
        }
    }
    
    
    func loginWithApple(userId:String,firstname:String,lastname:String,email:String){
        /*apple_id, email, first_name, last_name, device_type, device_tokken*/
        let baseurl = "\(GlobalUrls.BASE_URL)\(GlobalUrls.loginApple)"
        
        let token = UserDefault.standard.getCurrentDeviceToken()
            
        let params :[String:Any] = ["apple_id":"\(userId)","email":"\(email)","first_name":firstname,"last_name":lastname,"device_type":"2","device_tokken":"\(token)"]
            print(params)
            
            UtilityMethods.showIndicator()
            Alamofire.request(baseurl, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
               UtilityMethods.hideIndicator()
                
                if  response.result.value != nil{
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["status"].stringValue == "success"{
                        if swiftyVarJson["data"]["flag"].intValue == 1{
                            UserDefaults.standard.set("AppleLogin", forKey: "Apple")
                             UserDefaults.standard.set(swiftyVarJson["data"]["user_id"].stringValue, forKey: "UserID")
                            UserDefault.standard.setCurrentUserId(userId: swiftyVarJson["data"]["user_id"].intValue)

                                                       self.appDelegate.goToHome()
                        }else{
                             UserDefaults.standard.set(swiftyVarJson["data"]["user_id"].stringValue, forKey: "UserID")
                            UserDefault.standard.setCurrentUserId(userId: swiftyVarJson["data"]["user_id"].intValue)
                            
                            
                            let vc = UIStoryboard.skills
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                           
                        }
                                            
                    }else{
                        
                       UtilityMethods.hideIndicator()
                        
                         UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.Error, message: swiftyVarJson["message"].stringValue, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
                       
                        
                    }
                    
                    
                    
                    
                }else{
    
                    
                }
                
            }
        }
    
    

}
