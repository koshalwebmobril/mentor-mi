//
//  PlayerThumbnailVC.swift
//  Mentor_Mi
//
//  Created by Manish on 11/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import AVKit
import AVFoundation
import FacebookShare
import SafariServices
import WebKit

//import TwitterKit




class PlayerThumbnailVC: UIViewController, SharingDelegate  {
    
    var fromWhere = String()
    var dictTutorial = JSON()
    @IBOutlet weak var bgImgThumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var bgViewComment: UIView!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var bgViewdetails: UIView!
    @IBOutlet weak var lblSpeakMind: UILabel!
    @IBOutlet weak var countCommentLabel: UILabel!
    
    @IBOutlet weak var lineView: UILabel!
    
    @IBOutlet weak var shareView: UIView!
    
    
    
    var strCommentCount:String? = ""
    
    var playerViewController=AVPlayerViewController()
    var playerView = AVPlayer()
    
    var strURL = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictTutorial)
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        
        setUI()
        getData()
        
        
        if fromWhere == "MentorFeed" {
            
            strCommentCount=dictTutorial["post_comments"].stringValue
        }
           else if fromWhere == "otherTutorial"{
        strCommentCount=dictTutorial["post_comments"].stringValue
        
    }
          
        
        else{
            strCommentCount=dictTutorial["total_comments"].stringValue
        }
        
        
        
        
    }
    
    func setUI() {
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(PlayerThumbnailVC.actionProfile))
        bgViewdetails.isUserInteractionEnabled = true
        bgViewdetails.addGestureRecognizer(tap1)
        
        if fromWhere=="myProfile"{
            
            bgViewdetails.isUserInteractionEnabled = false
            
        }
        
        
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(PlayerThumbnailVC.forComments))
        bgViewComment.isUserInteractionEnabled = true
        bgViewComment.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(PlayerThumbnailVC.forComments))
        lblSpeakMind.isUserInteractionEnabled = true
        lblSpeakMind.addGestureRecognizer(tap3)
        
        imgUser.layer.cornerRadius = imgUser.bounds.height/2
        imgUser.clipsToBounds = true
        imgUser.layer.borderWidth = 1.5
        imgUser.layer.borderColor = UIColor.white.cgColor
        
        bgViewComment.layer.cornerRadius = 5.0
        bgViewComment.clipsToBounds = true
        bgViewComment.layer.borderWidth = 2.5
        bgViewComment.layer.borderColor = UIColor.white.cgColor

    }
    
    func  getData() {
        if fromWhere == "soapBox" {
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        if fromWhere == "Class" {
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        if fromWhere == "MentorFeed" {
            print("MentorFeed : \(dictTutorial)")
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        if fromWhere == "Profile" {
            print(dictTutorial)
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        if fromWhere == "myProfile" {
            print(dictTutorial)
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        
        if fromWhere == "myTutorial" {
            print(dictTutorial)
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        
        if fromWhere == "otherTutorial" {
            print(dictTutorial)
            let strUrl = dictTutorial["video_thumbnail"].stringValue
            bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
            
            lblTitle.text = dictTutorial["post_title"].stringValue
            lblTime.text = dictTutorial["posted"].stringValue
            lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
            let strURLUser = dictTutorial["profile_pic"].stringValue
            imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
            strURL = dictTutorial["post_vedio"].stringValue
        }
        
        if fromWhere == "Group" {
            if strURL == "" {
                
            }
            else {
                print(dictTutorial)
                let strUrl = dictTutorial["thumbnail"].stringValue
                bgImgThumbnail.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "trending3"))
                
                lblTitle.text = dictTutorial["post_title"].stringValue
                lblTime.text = dictTutorial["posted"].stringValue
                lblTag.text = "\(dictTutorial["post_tag"].stringValue)"
                let strURLUser = dictTutorial["profile_pic"].stringValue
                imgUser.sd_setImage(with: URL(string: strURLUser), placeholderImage: UIImage(named: "trending3"))
                strURL = dictTutorial["video"].stringValue
            }
        }
        openPlayer()
    }
    
    func openPlayer() {
        if strURL == "" {
            
        }
        else {
            let top: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
            let video = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoVC") as! PlayVideoVC
            video.strUrl = strURL
//            video.strUrl = dictTutorial["post_vedio"].stringValue
            
//            self.navigationController?.pushViewController(video, animated: true)
            top?.present(video, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
                
        tabBarController?.navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = true
        navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCommentCount(_:)), name: NSNotification.Name("countComment"), object: nil)
        
       
            countCommentLabel.text=strCommentCount
        
    }
    
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
   
        shareView.isHidden = true
    
    }
    
    
    @IBAction func facebookShare(_ sender: Any) {
        
                
//       guard let url = URL(string: "https://developers.facebook.com") else { return }
//       let content = ShareLinkContent()
//       content.contentURL = url
//        content.quote = "This share from mentor_mi"
//        let dialog : ShareDialog = ShareDialog()
//        dialog.fromViewController = self
//        dialog.mode = .automatic
//        dialog.shareContent = content
//        dialog.delegate = self
//        if dialog.canShow{
//            dialog.show()
//        }else{
//            print("Can't open")
//        }
        
       let shareString = "http://www.facebook.com/sharer.php?u=\(strURL)"


        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: escapedShareString)

        print(url!)


        let VC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        VC.strUrl = escapedShareString
        self.navigationController?.pushViewController(VC, animated: true)
//        self.present(VC, animated: true, completion: nil)
        
//        UIApplication.shared.open(url!)
        
       


       
    }
    
    
    
   
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        if sharer.shareContent.pageID != nil {
            print("Share: Success")
        }
    }
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("Share: Fail")
    }
    func sharerDidCancel(_ sharer: Sharing) {
        print("Share: Cancel")
    }
    
    @IBAction func twitterShare(_ sender: Any) {
        
        let tweetUrl = strURL

        let shareString = "https://twitter.com/intent/tweet?url=\(tweetUrl)"
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        VC.strUrl = escapedShareString
        self.navigationController?.pushViewController(VC, animated: true)
//        self.present(VC, animated: true, completion: nil)
        
        
//        let url = URL(string: escapedShareString)
        
        

//        UIApplication.shared.open(url!)
        

    }
    
    
    
    
    @IBAction func linkedInShare(_ sender: Any) {
        
        let linkedInUrl = strURL
        
        

        let escapedString = linkedInUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(escapedString!)
        
        let shareString = "https://www.linkedin.com/shareArticle?mini=true&text=\(escapedString!)"
        
        guard let url = URL(string: shareString) else{return}
      
        print(url)
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        VC.strUrl = shareString
        self.navigationController?.pushViewController(VC, animated: true)
        
//        self.present(VC, animated: true, completion: nil)
        
//        UIApplication.shared.open(url)
        
       /*
        LISDKSessionManager.createSession(withAuth:
        [LISDK_BASIC_PROFILE_PERMISSION,LISDK_W_SHARE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: {(sucess) in
                let session = LISDKSessionManager.sharedInstance().session
                print("Session ",session!)
//                let url = "https://api.linkedin.com/v1/people/~"
            
                if LISDKSessionManager.hasValidSession(){
                    let url: String = "https://api.linkedin.com/v2/ugcPosts"

//                    let payloadStr: String = "{\"comment\":\(self.strURL),\"visibility\":{\"code\":\"anyone\"}}"
                    let payloadStr: String = "{\"comment\":\"\(self.strURL)\",\"visibility\":{ \"code\":\"anyone\" }}"
                    let payloadData = payloadStr.data(using: String.Encoding.utf8)

                    LISDKAPIHelper.sharedInstance().postRequest(url, body: payloadData, success: { (response) in
                        print(response!.data)
                    },
                        error: { (error) in
                        print(error!)

                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)

                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        {(error) in
                print("Error \(String(describing: error))")
            }
        */
    }
    
    
    
    
    @objc func updateCommentCount(_ notification: NSNotification){
        
        
    let value = notification.userInfo?["count"]
    
        if let value=value{
            
            strCommentCount="\(value)"
        }
        
        
    }
    
    @objc func actionProfile(_ sender: UITapGestureRecognizer) {
        let controller = UIStoryboard.otherProfile
        controller.fromWhere = fromWhere
        controller.userId = dictTutorial["user_id"].stringValue
        print("UserId: \(dictTutorial["user_id"].stringValue) , fromWhere: \(fromWhere)")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionPlayTutorial(_ sender: Any) {
        let top: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        let video = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoVC") as! PlayVideoVC
        video.strUrl = strURL
        top?.present(video, animated: true)
    }
    
    @IBAction func actionComments(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
        //controller.strPostId = dictTutorial["id"].stringValue
        if fromWhere == "soapBox" {

                    controller.strPostId = String(format: "%@", dictTutorial["id"].stringValue)
                }

                if fromWhere == "MentorFeed" {

                    controller.strPostId = String(format: "%@", dictTutorial["post_id"].stringValue)

                }

                if fromWhere == "Search" {

                    controller.strPostId = String(format: "%@", dictTutorial["post_id"].stringValue)

                }

                if fromWhere == "Tutorial" {

                    controller.strPostId = String(format: "%@", dictTutorial["post_id"].stringValue)

                }

                if fromWhere == "myTutorial" {

                    controller.strPostId = String(format: "%@", dictTutorial["id"].stringValue)

                }

                if fromWhere == "Class" {

                    controller.strPostId = String(format: "%@", dictTutorial["postid"].stringValue)
                }

                if fromWhere == "Group" {

                    controller.strPostId = String(format: "%@", dictTutorial["postid"].stringValue)
                }
        if fromWhere == "otherTutorial" {

            controller.strPostId = String(format: "%@", dictTutorial["post_id"].stringValue)
        }




                if fromWhere == "Profile" {

                    controller.strPostId = String(format: "%@", dictTutorial["id"].stringValue)
                }

                if fromWhere == "myProfile" {

                    controller.strPostId = String(format: "%@", dictTutorial["id"].stringValue)
                }

        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @objc func forComments(_ sender: UITapGestureRecognizer) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlayerVC") as! PlayerVC
        controller.dictTutorial = dictTutorial
        controller.fromWhere = fromWhere
        

        
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func actionShare(_ sender: Any) {
        
//        let items = [URL(string: strURL)!]
        let items = [ strURL]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
//        self.view.window?.rootViewController = UINavigationController.present(ac)
        
//        shareView.isHidden = false
    
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
