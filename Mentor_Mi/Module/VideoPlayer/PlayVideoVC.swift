//
//  PlayVideoVC.swift
//  Mentor_Mi
//
//  Created by Manish on 12/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayVideoVC: AVPlayerViewController {
    
    var playerViewController=AVPlayerViewController()
    var playerView = AVPlayer()
    var strUrl = String()


    override func viewDidLoad() {
        super.viewDidLoad()

    if #available(iOS 13.0, *) {
    let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
    
        statusBar.backgroundColor = .appDefaultColor
    UIApplication.shared.keyWindow?.addSubview(statusBar)
    } else {
        
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            // my stuff
            statusBar.backgroundColor = .appDefaultColor
        }

    }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         guard let url=URL(string: strUrl) else { return  }
               let player = AVPlayer(url: url)
               let playerLayer = AVPlayerLayer(player: player)
               playerLayer.frame = self.view.frame
               self.player = player
               self.player?.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
       // let fileURL = NSURL(fileURLWithPath: "\(strUrl)")
       // let player = AVPlayer(url: fileURL as URL)
       
        
    }
    

   

}
