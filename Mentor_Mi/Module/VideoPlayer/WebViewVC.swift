//
//  WebViewVC.swift
//  Mentor_Mi
//
//  Created by Sandeep Kumar on 04/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import WebKit
import FacebookShare
import SVProgressHUD

class WebViewVC: UIViewController, WKUIDelegate,WKNavigationDelegate {
   
    

    @IBOutlet weak var webView: WKWebView!
    var strUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

             
        
        let url = URL(string: strUrl)
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url!))
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show(withStatus: "Loading...")
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        SVProgressHUD.dismiss()
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
//        self.dismiss(animated: true, completion: nil)
    }
    
  

}
