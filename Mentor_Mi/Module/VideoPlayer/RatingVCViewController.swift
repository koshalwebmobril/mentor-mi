//
//  RatingVCViewController.swift
//  Mentor Mi
//
//  Created by WebMobril on 17/12/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
//import FloatRatingView




class RatingVCViewController: UIViewController ,FloatRatingViewDelegate{

    @IBOutlet weak var btnStar1: UIButton!
    @IBOutlet weak var btnStar2: UIButton!
    @IBOutlet weak var btnStar3: UIButton!
    @IBOutlet weak var btnStar4: UIButton!
    @IBOutlet weak var btnStar5: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    var strIdPost = String()
    var rate = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ratingView.delegate = self
        
        rate = "0"
        
        btnStar1.tag = 1
        btnStar2.tag = 2
        btnStar3.tag = 3
        btnStar4.tag = 4
        btnStar5.tag = 5
        
        navigationController?.navigationBar.isHidden = true
        
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.clipsToBounds = true

        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        // Do any additional setup after loading the view.
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        rate = "\(rating)"
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        print(rating)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        rate = "0"
    }
    
    //    Mark :- actionReview
    
    @IBAction func actionStars(_ sender: UIButton) {
        if sender.tag == 1 {
            rate = "1"
            sender.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar2.setImage(UIImage(named: "starBlank"), for: .normal)
            btnStar3.setImage(UIImage(named: "starBlank"), for: .normal)
            btnStar4.setImage(UIImage(named: "starBlank"), for: .normal)
            btnStar5.setImage(UIImage(named: "starBlank"), for: .normal)
        }
        if sender.tag == 2 {
            rate = "2"
            btnStar2.setImage(UIImage(named: "starGary"), for: .normal)
            sender.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar3.setImage(UIImage(named: "starBlank"), for: .normal)
            btnStar4.setImage(UIImage(named: "starBlank"), for: .normal)
            btnStar5.setImage(UIImage(named: "starBlank"), for: .normal)
            
        }
        if sender.tag == 3 {
            rate = "3"
            btnStar1.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar2.setImage(UIImage(named: "starGary"), for: .normal)
            sender.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar4.setImage(UIImage(named: "starBlank"), for: .normal)
            btnStar5.setImage(UIImage(named: "starBlank"), for: .normal)
            
        }
        if sender.tag == 4 {
            rate = "4"
            btnStar1.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar2.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar3.setImage(UIImage(named: "starGary"), for: .normal)
            sender.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar5.setImage(UIImage(named: "starBlank"), for: .normal)
            
        }
        if sender.tag == 5 {
            rate = "5"
            btnStar1.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar2.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar3.setImage(UIImage(named: "starGary"), for: .normal)
            btnStar4.setImage(UIImage(named: "starGary"), for: .normal)
            sender.setImage(UIImage(named: "starGary"), for: .normal)
            
        }
    }
    
    //    Mark :- actionBack
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    //    Mark :- actionSubmit
    
    @IBAction func actionSubmit(_ sender: Any) {
        
        let rateDone = true
        let rates = true
        UserDefaults.standard.set(rates, forKey: "rate")
        UserDefaults.standard.set(rateDone, forKey: "ratedone")
        
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.rate)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID,"post_id":"\(strIdPost)","rating":"\(rate)"]
        print(rate)
        print("rate")
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    
                    let alert  = UIAlertController(title: "Success" , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                SVProgressHUD.dismiss()
            }
        }
    }
   
}
