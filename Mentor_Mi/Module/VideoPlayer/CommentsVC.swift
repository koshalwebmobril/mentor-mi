//
//  CommentsVC.swift
//  Mentor_Mi
//
//  Created by Manish on 18/11/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire



class CommentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableComment: UITableView!
    var arrComments = NSArray()
    var strPostId = String()
    
    @IBOutlet weak var lblMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableComment.delegate = self
        tableComment.dataSource = self
        tableComment.estimatedRowHeight = 80
        tableComment.rowHeight = UITableView.automaticDimension
        
        getAllComment()
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
    }
    
    func getAllComment() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewComments)"
        print("\(apiURL)")
        
        let newTodo: [String: Any] = ["postid": strPostId]
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrComments = swiftyJsonVar["data"].arrayObject! as NSArray
//                    let temp = Int(self.arrComments.count)
                    
                    if self.arrComments.count > 0 {
                        self.tableComment.reloadData()
                    }
                    else {
                        
                        self.lblMessage.isHidden = false
                        self.tableComment.isHidden = true
                        
                    }
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
                SVProgressHUD.dismiss()
            }
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.lblMessage.isHidden = true
        
        let cell = tableComment.dequeueReusableCell(withIdentifier: "cell1") as! CommentTableViewCell
        
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.height/2
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        
        
        let strURL = String(format:"%@",((arrComments.object(at: indexPath.row) ) as AnyObject).object(forKey: "posted_user_pic") as! String)
        cell.userImageView.sd_setImage(with: URL(string: strURL), placeholderImage: UIImage(named: "trending3"))
        
        
        cell.nameLabel.text = String(format:"%@",((arrComments.object(at: indexPath.row) ) as AnyObject).object(forKey: "posted_user_name") as! String)
        
        cell.dateLabel.text = String(format:"%@",((arrComments.object(at: indexPath.row) ) as AnyObject).object(forKey: "comment_date") as! String)
        
        
        
        cell.lblComment.text = String(format:"%@",((arrComments.object(at: indexPath.row) ) as AnyObject).object(forKey: "comments") as! String)
        return cell
        
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return UITableView.automaticDimension
        
        return 80
    }
    
    
}
