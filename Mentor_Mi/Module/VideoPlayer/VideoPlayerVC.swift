//
//  Video Player.swift
//  Mentor Mi
//
//  Created by WebMobril on 12/11/18.
//  Copyright © 2018 WebMobril. All rights reserved.
//

import UIKit

import AVKit
import AVFoundation
import Alamofire
import SwiftyJSON
import SDWebImage
import FacebookShare
import SVProgressHUD

class Video_Player: UIViewController , UITableViewDelegate, UITableViewDataSource  {
    
    var isOn = true
    let appConstant:AppConstants = AppConstants()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var playerViewController=AVPlayerViewController()
    var playerView = AVPlayer()
    var strVideoURL = String()
    
    @IBOutlet weak var tble: UITableView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var btnShowComments: UIButton!
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var iconRating1: UIImageView!
    @IBOutlet weak var iconRating2: UIImageView!
    @IBOutlet weak var iconRating3: UIImageView!
    @IBOutlet weak var iconRating4: UIImageView!
    @IBOutlet weak var iconRating5: UIImageView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var tableCommentLists: UITableView!
    @IBOutlet weak var imgThumbnail: UIImageView!
    var arrTimeLine = [JSON]()
    @IBOutlet weak var threeDotsinfo: UIView!
    
    @IBAction func status(_ sender: Any) {
        threeDotsinfo.isHidden = true
        statusView.isHidden = false
        statusView.layer.borderWidth = 2
        statusView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        isOn =  true
    }
    @IBAction func premium(_ sender: Any) {
        threeDotsinfo.isHidden = true
        let prmum = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
        self.navigationController?.pushViewController(prmum, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        threeDotsinfo.isHidden = true
        let stng = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(stng, animated: true)
        
    }
    
    @IBOutlet weak var statusView: UIView!
    
    @IBAction func cancel(_ sender: Any) {
        statusView.isHidden = true
    }
    @IBAction func ok(_ sender: Any) {
        if statusTxt.text == ""{
        let alert  = UIAlertController(title: "Mentor Mi" , message: "Please enter status.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        else{
        statusView.isHidden = true
       
        uploadStatus()
        statusTxt.text = ""
        }
    }
    
    func getProfileData() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.getProfile)"
        print("\(apiURL)")
        
        let userID = UserDefaults.standard.object(forKey: "user_id") as! String
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    myData.myDetails = swiftyJsonVar["data"]
                    
                    
                    self.getTimeLineAPI()
                    
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
            }
        }
    }
    
    func getTimeLineAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.timelineDatewiseTutorial)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["user_id": userID]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrTimeLine = swiftyJsonVar["data"].arrayValue
                    print(self.arrTimeLine)
                    
                }
                else {
                    
                }
                
            }
            else {
                
            }
        }
    }
    
    func uploadStatus() {
        
        if statusTxt.text! == "" {
            
        }
        else  {
            
            let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addStatus)"
            print("\(apiURL)")
            
            let statusAbtMe =  UserDefaults.standard.object(forKey: "status") as! String
            print(statusAbtMe)
            print("sttttaaattttuuuussss")
            let userID = UserDefaults.standard.object(forKey: "user_id") as! String
            let newTodo: [String: Any] = ["user_id": userID,"status":statusAbtMe]
            
            Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
                if((response.result.value) != nil) {

                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    if swiftyJsonVar["status"].string == "success" {
                        
                        self.getProfileData()
                        
                    }
                    else {
                        
                    }
                }
                else {
                    
                }
            }
        }
    }
    
    @IBOutlet weak var statusTxt: UITextField!
    
    
    var dictTutorial = NSDictionary()
    var fromWhere = ""
    
    var arrComments = NSArray()
    var strURL = String()
    var strThumbnailURL = String()
    var isCommentShow = false
    var strPostId = String()
    var by = String()
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTimeTag: UILabel!
    @IBOutlet weak var btnSendShare: UIButton!
    
    @IBOutlet weak var lblPostTag: UILabel!
    var strRating = String()
    var idForRating = ""
    
    //    @IBAction func actionBack(_ sender: Any) {
    //        self.dismiss(animated: false, completion: nil)
    //    }
    //    let isRate = UserDefaults.standard.object(forKey: "rate") as! Bool
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictTutorial)
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
            statusBar.backgroundColor = .appDefaultColor
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = .appDefaultColor
            }

        }
        
        threeDotsinfo.isHidden = true
        
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        notify.tintColor = UIColor.black
        threeDots.tintColor = UIColor.black
        
        navigationController?.view.addSubview(threeDotsinfo)
        navigationController?.view.addSubview(statusView)
        
        
        if fromWhere == "soapBox" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "video_thumbnail") as! String)
            strURL = String(format: "%@", dictTutorial.object(forKey: "post_vedio") as! String)
            strPostId = String(format: "%@", dictTutorial.object(forKey: "id") as! String)
            
            lblTitle.text = String(format: "%@", dictTutorial.object(forKey: "post_title") as! String)
            lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "schedule_date") as! String)
            lblPostTag.text = String(format:
                "%@", dictTutorial.object(forKey: "post_tag") as! String)
            idForRating = String(format: "%@", dictTutorial.object(forKey: "id") as! String)
        }
        
        if fromWhere == "MentorsFeed" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "video_thumbnail") as! String)
            strURL = String(format: "%@", dictTutorial.object(forKey: "post_vedio") as! String)
            strPostId = String(format: "%@", dictTutorial.object(forKey: "post_id") as! String)
            
            lblTitle.text = String(format: "%@", dictTutorial.object(forKey: "post_title") as! String)
            lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "posted") as! String)
            lblPostTag.text = String(format:
                "%@", dictTutorial.object(forKey: "post_tag") as! String)
            idForRating = String(format: "%@", dictTutorial.object(forKey: "post_id") as! String)
            //            strRating = (dictTutorial.object(forKey: "rating") as! String)
            
        }
        
        if fromWhere == "Search" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "video_thumbnail") as! String)
            strURL = String(format: "%@", dictTutorial.object(forKey: "post_video") as! String)
            strPostId = String(format: "%@", dictTutorial.object(forKey: "post_id") as! String)
            
            idForRating = String(format: "%@", dictTutorial.object(forKey: "post_id") as! String)
            //            strRating = (dictTutorial.object(forKey: "rating") as! String)
            
        }
        
        if fromWhere == "Tutorial" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "video_thumbnail") as! String)
            strURL = String(format: "%@", dictTutorial.object(forKey: "post_vedio") as! String)   //Mentor
            strPostId = String(format: "%@", dictTutorial.object(forKey: "post_id") as! String)
            
            lblTitle.text = String(format: "%@", dictTutorial.object(forKey: "post_title") as! String)
            lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "post_date") as! String)
            lblPostTag.text = String(format:
                "%@", dictTutorial.object(forKey: "post_tag") as! String)
            idForRating = String(format: "%@", dictTutorial.object(forKey: "post_id") as! String)
            //            strRating = (dictTutorial.object(forKey: "rating") as! String)
            
        }
        
        if fromWhere == "TutorialMe" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "video_thumbnail") as! String)
            strURL = String(format: "%@", dictTutorial.object(forKey: "post_vedio") as! String)   //Mentor
            strPostId = String(format: "%@", dictTutorial.object(forKey: "id") as! String)
            
            lblTitle.text = String(format: "%@", dictTutorial.object(forKey: "post_title") as! String)
            //lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "post_date") as! String)
            // lblPostTag.text = String(format: "%@", dictTutorial.object(forKey: "post_tag") as! String)
            idForRating = String(format: "%@", dictTutorial.object(forKey: "id") as! String)
            //            strRating = (dictTutorial.object(forKey: "rating") as! String)
            
        }
        
        if fromWhere == "Class" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "video_thumbnail") as! String)
            strURL = String(format: "%@", dictTutorial.object(forKey: "post_vedio") as! String)
            strPostId = String(format: "%@", dictTutorial.object(forKey: "postid") as! String)
            
            lblTitle.text = String(format: "%@", dictTutorial.object(forKey: "post_title") as! String)
            //   lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "post_date") as! String)
            lblPostTag.text = String(format:
                "%@", dictTutorial.object(forKey: "post_tag") as! String)
            
            idForRating = String(format: "%@", dictTutorial.object(forKey: "postid") as! String)
        }
        
        if fromWhere == "Group" {
            strThumbnailURL = String(format: "%@", dictTutorial.object(forKey: "thumbnail") as! CVarArg)
            strURL = String(format: "%@", dictTutorial.object(forKey: "video") as! CVarArg)
            strPostId = String(format: "%@", dictTutorial.object(forKey: "postid") as! CVarArg)
            
            lblTitle.text = String(format: "%@", dictTutorial.object(forKey: "post_title") as! CVarArg)
            // lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "post_date") as! String)
            // lblPostTag.text = String(format:
            // "%@", dictTutorial.object(forKey: "post_tag") as! String)
            idForRating = String(format: "%@", dictTutorial.object(forKey: "postid") as! CVarArg)
        }
        
        print("Post Id for Rating: \(idForRating)")
        
        
        print(strPostId)
        
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.Rating))
        lblRating.isUserInteractionEnabled = true
        lblRating.addGestureRecognizer(tap1)
                
        if fromWhere == "Profile" {
            strThumbnailURL = dictTutorial.object(forKey: "video_thumbnail") as! String
            strURL = dictTutorial.object(forKey: "post_vedio") as! String
            strPostId = dictTutorial.object(forKey: "id") as! String
            
            lblTitle.text = (dictTutorial.object(forKey: "post_title") as! String)
            lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "post_date") as! String)
            lblPostTag.text = String(format:
                "%@", dictTutorial.object(forKey: "post_tag") as! String)
        }
        
        if fromWhere == "myProfile" {
            strThumbnailURL = dictTutorial.object(forKey: "thumbnail") as! String
            strURL = dictTutorial.object(forKey: "post_vedio") as! String
            strPostId = dictTutorial.object(forKey: "post_id") as! String
            
            lblTitle.text = (dictTutorial.object(forKey: "post_title") as! String)
            lblTimeTag.text = String(format: "%@",dictTutorial.object(forKey: "post_date") as! String)
            lblPostTag.text = String(format:
                "%@", dictTutorial.object(forKey: "post_tag") as! String)
        }
        
        if strRating == "1.00" {
            iconRating1.image = UIImage(named: "starFull")
            iconRating2.image = UIImage(named: "starBlank")
            iconRating3.image = UIImage(named: "starBlank")
            iconRating4.image = UIImage(named: "starBlank")
            iconRating5.image = UIImage(named: "starBlank")
        }
        else if strRating == "2.00" {
            iconRating1.image = UIImage(named: "starFull")
            iconRating2.image = UIImage(named: "starFull")
            iconRating3.image = UIImage(named: "starBlank")
            iconRating4.image = UIImage(named: "starBlank")
            iconRating5.image = UIImage(named: "starBlank")
        }
        else if strRating == "3.00" {
            iconRating1.image = UIImage(named: "starFull")
            iconRating2.image = UIImage(named: "starFull")
            iconRating3.image = UIImage(named: "starFull")
            iconRating4.image = UIImage(named: "starBlank")
            iconRating5.image = UIImage(named: "starBlank")
        }
        else if strRating == "4.00" {
            iconRating1.image = UIImage(named: "starFull")
            iconRating2.image = UIImage(named: "starFull")
            iconRating3.image = UIImage(named: "starFull")
            iconRating4.image = UIImage(named: "starFull")
            iconRating5.image = UIImage(named: "starBlank")
        }
        else if strRating == "5.00" {
            iconRating1.image = UIImage(named: "starFull")
            iconRating2.image = UIImage(named: "starFull")
            iconRating3.image = UIImage(named: "starFull")
            iconRating4.image = UIImage(named: "starFull")
            iconRating5.image = UIImage(named: "starFull")
        }
        else {
            iconRating1.image = UIImage(named: "starBlank")
            iconRating2.image = UIImage(named: "starBlank")
            iconRating3.image = UIImage(named: "starBlank")
            iconRating4.image = UIImage(named: "starBlank")
            iconRating5.image = UIImage(named: "starBlank")
        }
        
        tableCommentLists.estimatedRowHeight = 35
        tableCommentLists.rowHeight = UITableView.automaticDimension
        txtComment.layer.cornerRadius = 2.5
        txtComment.clipsToBounds = true
        
        tableCommentLists.layer.borderColor = UIColor.lightGray.cgColor
        tableCommentLists.layer.borderWidth = 1.5
        tableCommentLists.layer.cornerRadius = 5.0
        
        imgThumbnail.sd_setImage(with: URL(string: strThumbnailURL), placeholderImage: UIImage(named: "trending3"))
        imgThumbnail.layer.cornerRadius = 5.0
        imgThumbnail.clipsToBounds = true
        
//        getCommentsAPI()
        self.getCommentsAPI(postId: strPostId)

        
        tableCommentLists.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.playing))
        imgThumbnail.isUserInteractionEnabled = true
        imgThumbnail.addGestureRecognizer(tap)
        
    }
    
    @objc func notification() {
        let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notify, animated: true)
        
    }
    
    
    
    @objc func threedots() {
        
        if isOn == true {
            
            threeDotsinfo.isHidden = false
            threeDotsinfo.layer.borderWidth = 2
            threeDotsinfo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            isOn = false
        }
            
        else{
            threeDotsinfo.isHidden = true
            isOn = true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getRating()
        isOn = true
        
        navigationController?.view.addSubview(threeDotsinfo)
        navigationController?.view.addSubview(statusView)
        self.tabBarController?.title = "Comments"
        
        navigationController?.navigationBar.barTintColor = appConstant.navigationColor
        let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
        let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
        let rightBarButtons = [threeDots,notify]
        self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
        self.tabBarController?.navigationController?.isNavigationBarHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        if fromWhere == "Class" || fromWhere == "Group" {
            
            let notify = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notification))
            let threeDots = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(threedots))
            let rightBarButtons = [threeDots,notify]
            self.tabBarController?.navigationItem.rightBarButtonItems  = rightBarButtons
            self.tabBarController?.navigationController?.isNavigationBarHidden = false
            self.navigationController?.isNavigationBarHidden = false
        }
        
    }
    
    
    
    
    @objc func Rating(_ sender: UITapGestureRecognizer) {
        
        let ratedone = UserDefaults.standard.object(forKey: "ratedone") as! Bool
            if ratedone == true {
                let alert  = UIAlertController(title: "Mentor Mi", message: "you have already rate.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let rate = self.storyboard?.instantiateViewController(withIdentifier: "RatingVCViewController") as! RatingVCViewController
                rate.strIdPost = strPostId
                self.present(rate, animated: true, completion: nil)
          }
    }
    
    @objc func playing() {
        
     self.navigationController?.popViewController(animated: false)
        
    }
    
    func getCommentsAPI(postId: String) {
        let parameters = ["postid" : postId] as [String: Any]
        print(parameters)
        
        UtilityMethods.showIndicator()
        
        APIManager.sharedInstance.viewComment(parameters: parameters) { (success, response) in
            UtilityMethods.hideIndicator()
            guard let response = response else{ return }
            self.handleCommentListsResponse(response: response)
        }
    }
    
    func handleCommentListsResponse(response : [String : Any]) {
        let swiftyJsonVar = JSON(response)
        if swiftyJsonVar["status"].string == "success" {
            let responseJson = swiftyJsonVar["data"]
            print(responseJson)
            self.arrComments = swiftyJsonVar["data"].arrayObject! as NSArray
            let temp = Int(self.arrComments.count)
            
            self.btnShowComments.setTitle("\(temp) Comments", for: .normal)
            if self.arrComments.count > 0 {
                self.tableCommentLists.reloadData()
            }
            else {
                
            }
        }
        else {
            let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        
        
    }
    
    
    
    func getCommentsAPI() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.viewComments)"
        print("\(apiURL)")
        
        let newTodo: [String: Any] = ["postid": strPostId]
        
        Alamofire.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.httpBody).responseJSON { response in
            if((response.result.value) != nil) {

                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    self.arrComments = swiftyJsonVar["data"].arrayObject! as NSArray
                    let temp = Int(self.arrComments.count)
                    
                    self.btnShowComments.setTitle("\(temp) Comments", for: .normal)
                    if self.arrComments.count > 0 {
                        self.tableCommentLists.reloadData()
                    }
                    else {
                        
                    }
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                

            }
        }
    }
    
    
    @IBAction func actionShowComment(_ sender: Any) {
        if isCommentShow == false {
            isCommentShow = true
            if arrComments.count > 0 {
                tableCommentLists.isHidden = false
            }
        }
        else  {
            isCommentShow = false
            tableCommentLists.isHidden = true
        }
    }
    
    @IBAction func actionAddComment(_ sender: Any) {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.addComment)"
        print("\(apiURL)")
        
        let userID = UserDefault.standard.getCurrentUserId()!
        let newTodo: [String: Any] = ["post_id": strPostId,"user_id":userID,"comments":"\(txtComment.text ?? "")"]
        print(newTodo)
        
        
        
        Alamofire.request(apiURL, method: .get, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            if((response.result.value) != nil) {

                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    let responseJson = swiftyJsonVar["data"]
                    print(responseJson)
                    
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.txtComment.text = ""
//                        self.getCommentsAPI()
                        self.getCommentsAPI(postId: self.strPostId)
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    let alert  = UIAlertController(title: swiftyJsonVar["status"].string , message: swiftyJsonVar["message"].string, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableCommentLists.dequeueReusableCell(withIdentifier: "cell") as! CommentTableViewCell
        cell.lblComments.text = String(format:"%@",((arrComments.object(at: indexPath.row) ) as AnyObject).object(forKey: "comments") as! String)
        return cell
        
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func actionSendShare(_ sender: Any) {
//        let alert  = UIAlertController(title: "Share" , message: "Want to share others links", preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "facebook", style: .default, handler: { action in
////            self.faceBookShare()
//        }))
//        alert.addAction(UIAlertAction(title: "twitter", style: .default, handler: { action in
//
//        }))
//        alert.addAction(UIAlertAction(title: "linkedIn", style: .default, handler: { action in
//
//        }))
//        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil ))
//        self.present(alert, animated: true, completion: nil)
        
        let alert  = UIAlertController(title: "Mentor-Mi" , message: "we are working on it.", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
        //            self.faceBookShare()
                }))
        
        
    }
    
//    func faceBookShare() {
//        let vedio = Video(url: URL(string: strURL)!)
//        let content:VideoShareContent = VideoShareContent(video: vedio)
//
//        let shareDialog = ShareDialog(content: content)
//        shareDialog.mode = .native
//        shareDialog.failsOnInvalidData = true
//        shareDialog.completion = { result in
//            // Handle share results
//        }
//        do
//        {
//            try shareDialog.show()
//        }
//        catch
//        {
//            print("Exception")
//
//        }
//    }
//
    func getRating() {
        let apiURL: String = "\(GlobalUrls.BASE_URL)\(GlobalUrls.ratingVideo)"
        print("\(apiURL)")
        
        let newTodo: [String: Any] = ["post_id": idForRating]
        print(newTodo)
        
        
        
        SVProgressHUD.show(withStatus: "Loading...")
        Alamofire.request(apiURL, method: .get, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            if((response.result.value) != nil) {
                SVProgressHUD.dismiss()
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                if swiftyJsonVar["status"].string == "success" {
                    
                    let tempRate = swiftyJsonVar["data"]["rating"].stringValue
                    
                    if tempRate == "1.00" || tempRate == "1" {
                        self.iconRating1.image = UIImage(named: "starFull")
                        self.iconRating2.image = UIImage(named: "starBlank")
                        self.iconRating3.image = UIImage(named: "starBlank")
                        self.iconRating4.image = UIImage(named: "starBlank")
                        self.iconRating5.image = UIImage(named: "starBlank")
                    }
                    else if tempRate == "2.00"  || tempRate == "2" {
                        self.iconRating1.image = UIImage(named: "starFull")
                        self.iconRating2.image = UIImage(named: "starFull")
                        self.iconRating3.image = UIImage(named: "starBlank")
                        self.iconRating4.image = UIImage(named: "starBlank")
                        self.iconRating5.image = UIImage(named: "starBlank")
                    }
                    else if tempRate == "3.00"  || tempRate == "3" {
                        self.iconRating1.image = UIImage(named: "starFull")
                        self.iconRating2.image = UIImage(named: "starFull")
                        self.iconRating3.image = UIImage(named: "starFull")
                        self.iconRating4.image = UIImage(named: "starBlank")
                        self.iconRating5.image = UIImage(named: "starBlank")
                    }
                    else if tempRate == "4.00"  || tempRate == "4" {
                        self.iconRating1.image = UIImage(named: "starFull")
                        self.iconRating2.image = UIImage(named: "starFull")
                        self.iconRating3.image = UIImage(named: "starFull")
                        self.iconRating4.image = UIImage(named: "starFull")
                        self.iconRating5.image = UIImage(named: "starBlank")
                    }
                    else if tempRate == "5.00"  || tempRate == "5" {
                        self.iconRating1.image = UIImage(named: "starFull")
                        self.iconRating2.image = UIImage(named: "starFull")
                        self.iconRating3.image = UIImage(named: "starFull")
                        self.iconRating4.image = UIImage(named: "starFull")
                        self.iconRating5.image = UIImage(named: "starFull")
                    }
                    else {
                        self.iconRating1.image = UIImage(named: "starBlank")
                        self.iconRating2.image = UIImage(named: "starBlank")
                        self.iconRating3.image = UIImage(named: "starBlank")
                        self.iconRating4.image = UIImage(named: "starBlank")
                        self.iconRating5.image = UIImage(named: "starBlank")
                    }
                }
                else {
                    
                }
            }
            else {
                
                SVProgressHUD.dismiss()
            }
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
